using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class StunStatus : StatusEffectBase
{
    private StunStateParticlesInstance m_particleEffect = null;

    public override string GetName()
    {
        return "Stun";
    }

    public override bool CanPerformActions()
    {
        return false;
    }

    public override void OnApply()
    {
        m_particleEffect = StunStateParticlesPool.Get();
        m_particleEffect.Bind(m_character);
    }

    public override void OnRemove()
    {
        m_particleEffect.Release();
        m_particleEffect = null;
    }
}
