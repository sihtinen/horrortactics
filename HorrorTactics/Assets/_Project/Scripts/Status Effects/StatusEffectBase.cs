using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class StatusEffectBase
{
    public StatusEffectBase() { }

    public int Duration = 0;

    protected CharacterBase m_character = null;

    public void Apply(CharacterBase character)
    {
        m_character = character;

        OnApply();
    }

    public virtual void OnTurnStart()
    {
        if (Duration <= 0)
        {
            m_character.RemoveStatusEffect(this);
            return;
        }

        Duration--;
    }

    public virtual string GetName()
    {
        return "StatusEffect_NoName";
    }

    public virtual bool CanPerformActions()
    {
        return true;
    }

    public virtual void OnApply()
    {

    }

    public virtual void OnRemove()
    {
        
    }

    public virtual void ReApplyStatusEffect(StatusEffectBase newStatusEffect)
    {
        this.Duration = newStatusEffect.Duration;
    }
}

[System.Serializable]
public class StatusInflictParameters<T> where T : StatusEffectBase, new()
{
    [SerializeField] private bool m_isEnabled = false;

    [Range(0, 1)]
    [SerializeField] private float m_inflictChance = 0;

    [Min(1)] public int Duration = 1;

    public T StatusEffect;

    public bool WillInflict()
    {
        if (m_isEnabled == false) 
            return false;

        return Random.Range(0.0f, 1.0f) <= m_inflictChance;
    }

    public StatusEffectBase GetStatusEffectInstance()
    {
        StatusEffectBase _newInstance = new T();
        _newInstance.Duration = this.Duration;

        return _newInstance;
    }
}
