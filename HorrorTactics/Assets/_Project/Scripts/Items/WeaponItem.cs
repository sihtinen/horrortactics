using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[Serializable]
public abstract class WeaponItem : ItemBase
{
    [Header("Weapon Base Settings")]
    [Min(1)] public int ClipCapacity = 8;
    public EquipWeightType EquipWeight = EquipWeightType.Light;

    [Header("Weapon Base Actions")]
    [SerializeField] protected PlayerAction_EquipWeapon m_action_equip = null;
    [SerializeField] protected PlayerAction_ReloadWeapon m_action_reload = null;
    [SerializeField] protected PlayerAction_RangedAbility m_action_shoot = null;
    [SerializeField] protected PlayerAction_RangedAbility m_action_shootSpecial = null;

    [Header("Weapon Base Ammunition Settings")]
    public ItemBase AmmunitionAsset = null;

    [Header("Additional UI Settings")]
    public Sprite SingleAmmunitionIcon = null;

    protected override void generateMenuButtonOptions(ControllableCharacter character)
    {
        var _equipmentInventory = character.InventoryComponent.InventoryData;

        if (character.InventoryComponent.IsItemEquipped(this))
        {
            m_cachedMenuButtonOptions.Add(new InventoryPopupMenuButtonOption()
            {
                ButtonText = $"Unequip ({m_action_equip.APCosts[0]} AP)",
                OnClickedCallback = this.processEquipAction
            });
        }
        else
        {
            m_cachedMenuButtonOptions.Add(new InventoryPopupMenuButtonOption()
            {
                ButtonText = $"Equip ({m_action_equip.APCosts[0]} AP)",
                OnClickedCallback = this.processEquipAction
            });
        }

        if (CanBeReloaded(character))
        {
            m_cachedMenuButtonOptions.Add(new InventoryPopupMenuButtonOption()
            {
                ButtonText = $"Reload ({m_action_reload.APCosts[0]} AP)",
                OnClickedCallback = this.processReloadAction
            });
        }
    }

    protected override void useImplementation(ControllableCharacter character)
    {
        var _itemWrapper = character.InventoryComponent.InventoryData.GetItemWrapper(this);
        if (_itemWrapper.Amount == 0)
            return;

        // Consume one ammunition
        _itemWrapper.Amount -= 1;
        character.InventoryComponent.InventoryData.OnInventoryUpdated?.Invoke();

        // Play fire animation
        character.OnWeaponFired?.Invoke();
    }

    public bool CanBeReloaded(ControllableCharacter character)
    {
        var _inventory = character.InventoryComponent.InventoryData;
        var _weaponItemWrapper = _inventory.GetItemWrapper(this);

        if (_weaponItemWrapper == null)
            return false;

        if (_weaponItemWrapper.Amount >= this.ClipCapacity)
            return false;

        var _ammoItemWrapper = _inventory.GetItemWrapper(AmmunitionAsset);

        if (_ammoItemWrapper == null)
            return false;

        return true;
    }

    public int GetRemainingAmmoCount(ControllableCharacter character)
    {
        var _inventory = character.InventoryComponent.InventoryData;
        var _weaponItemWrapper = _inventory.GetItemWrapper(this);

        if (_weaponItemWrapper == null)
            return 0;

        return _weaponItemWrapper.Amount;
    }

    private void processEquipAction(ControllableCharacter character)
    {
        character.SetSelectedAction(null);

        m_action_equip.OnBeginValidateAction(character, this);
        m_action_equip.ProcessAction();
        m_action_equip.OnEndProcessAction();
    }

    private void processReloadAction(ControllableCharacter character)
    {
        character.SetSelectedAction(null);

        m_action_reload.OnBeginValidateAction(character, this);
        m_action_reload.PerformReloadAction();
        m_action_reload.OnEndProcessAction();
    }
}
