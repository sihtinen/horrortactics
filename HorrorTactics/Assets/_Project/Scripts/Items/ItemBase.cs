using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Sirenix.OdinInspector;


[Serializable]
public abstract class ItemBase : ScriptableObject
{
    [Header("ReadOnly Parameters")]
    [ReadOnly] public string GUID = Convert.ToBase64String(Guid.NewGuid().ToByteArray());

    [Header("Base Settings")]
    public Sprite UIIcon = null;
    public ItemCategory Category = ItemCategory.Survival_Equippable;

    protected static List<InventoryPopupMenuButtonOption> m_cachedMenuButtonOptions = new List<InventoryPopupMenuButtonOption>();
    protected static List<PlayerActionSelectionOption> m_cachedActions = new List<PlayerActionSelectionOption>();

    public string DisplayName => getDisplayName();
    public void Use(ControllableCharacter character) => useImplementation(character);

    public List<InventoryPopupMenuButtonOption> GetMenuButtonOptions(ControllableCharacter character)
    {
        m_cachedMenuButtonOptions.Clear();
        generateMenuButtonOptions(character);
        return m_cachedMenuButtonOptions;
    }

    public abstract List<PlayerActionSelectionOption> GetAvailableActions(ControllableCharacter character);
    protected abstract void useImplementation(ControllableCharacter character);
    protected abstract string getDisplayName();
    protected abstract void generateMenuButtonOptions(ControllableCharacter character);
}
