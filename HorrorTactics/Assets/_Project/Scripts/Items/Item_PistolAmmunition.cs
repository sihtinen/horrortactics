using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[Serializable]
[CreateAssetMenu(menuName = "Items/Pistol Ammunition", fileName = "Item_PistolAmmunition")]
public class Item_PistolAmmunition : ItemBase
{
    protected override string getDisplayName()
    {
        return "Pistol Ammunition";
    }

    protected override void useImplementation(ControllableCharacter character)
    {

    }

    protected override void generateMenuButtonOptions(ControllableCharacter character)
    {

    }

    public override List<PlayerActionSelectionOption> GetAvailableActions(ControllableCharacter character)
    {
        m_cachedActions.Clear();
        return m_cachedActions;
    }
}
