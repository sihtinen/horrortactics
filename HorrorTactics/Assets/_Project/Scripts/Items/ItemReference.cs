using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[System.Serializable]
public class ItemReference
{
    public ItemReference(ItemBase item)
    {
        m_item = item;
        ItemGUID = m_item.GUID;
    }

    public string ItemGUID = string.Empty;

    private ItemBase m_item = null;

    public ItemBase ItemAsset
    {
        get 
        {
            return m_item;
        }
    }
}
