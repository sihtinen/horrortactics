using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using TMPro;

[Serializable]
[CreateAssetMenu(menuName = "Items/Pistol Weapon", fileName = "Item_PistolWeapon")]
public class Item_PistolWeapon : WeaponItem
{
    protected override void generateMenuButtonOptions(ControllableCharacter character)
    {
        base.generateMenuButtonOptions(character);
    }

    protected override string getDisplayName()
    {
        return "Pistol";
    }

    protected override void useImplementation(ControllableCharacter character)
    {
        base.useImplementation(character);
    }

    public override List<PlayerActionSelectionOption> GetAvailableActions(ControllableCharacter character)
    {
        m_cachedActions.Clear();

        var _equipmentInventory = character.InventoryComponent.InventoryData;

        if (character.InventoryComponent.IsItemEquipped(this))
        {
            m_cachedActions.Add(new PlayerActionSelectionOption()
            {
                Action = m_action_reload,
                ActionParameters = this
            });

            m_cachedActions.Add(new PlayerActionSelectionOption()
            {
                Action = m_action_shoot,
                ActionParameters = this
            });

            m_cachedActions.Add(new PlayerActionSelectionOption()
            {
                Action = m_action_shootSpecial,
                ActionParameters = this
            });
        }

        return m_cachedActions;
    }
}
