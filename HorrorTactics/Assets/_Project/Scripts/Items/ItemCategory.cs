public enum ItemCategory
{
    Survival_Equippable = 0,
    Survival_Consumable = 1,
    KeyItem = 2,
    Document = 3
}
