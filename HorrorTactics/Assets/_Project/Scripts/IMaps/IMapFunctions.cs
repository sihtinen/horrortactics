using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Sami.IMaps
{
    public enum IMapGateFilter
    {
        Greater = 0,
        GreaterOrEqual = 1,
        Less = 2,
        LessOrEqual = 3
    }

    public static class IMapFunctions
    {
        public static void SetToZero(this IMap map)
        {
            for (int i = 0; i < map.NodeList.Length; i++)
            {
                var _node = map.NodeList[i];
                _node.NodeValue = 0;
                map.NodeList[i] = _node;
            }

            map.OnUpdated?.Invoke();
        }

        public static void Add(this IMap mapA, IMap mapB)
        {
            for (int i = 0; i < mapA.NodeList.Length; i++)
            {
                var _nodeA = mapA.NodeList[i];
                _nodeA.NodeValue += mapB.NodeList[i].NodeValue;
                mapA.NodeList[i] = _nodeA;
            }

            mapA.OnUpdated?.Invoke();
        }

        public static void Subtract(this IMap map, float amount)
        {
            for (int i = 0; i < map.NodeList.Length; i++)
            {
                var _nodeA = map.NodeList[i];
                _nodeA.NodeValue -= amount;
                map.NodeList[i] = _nodeA;
            }

            map.OnUpdated?.Invoke();
        }

        public static void Subtract(this IMap mapA, IMap mapB)
        {
            for (int i = 0; i < mapA.NodeList.Length; i++)
            {
                var _nodeA = mapA.NodeList[i];
                _nodeA.NodeValue -= mapB.NodeList[i].NodeValue;
                mapA.NodeList[i] = _nodeA;
            }

            mapA.OnUpdated?.Invoke();
        }

        public static void Multiply(this IMap mapA, IMap mapB)
        {
            for (int i = 0; i < mapA.NodeList.Length; i++)
            {
                var _nodeA = mapA.NodeList[i];
                _nodeA.NodeValue *= mapB.NodeList[i].NodeValue;
                mapA.NodeList[i] = _nodeA;
            }

            mapA.OnUpdated?.Invoke();
        }

        public static void Invert(this IMap map)
        {
            IMapNode? _maxValueNode = map.GetNodeWithHighestValue();
            if (_maxValueNode.HasValue == false)
                return;

            float _maxValue = _maxValueNode.Value.NodeValue;

            for (int i = 0; i < map.NodeList.Length; i++)
            {
                var _node = map.NodeList[i];
                _node.NodeValue = _maxValue - _node.NodeValue;
                map.NodeList[i] = _node;
            }

            map.OnUpdated?.Invoke();
        }

        public static void Normalize(this IMap map)
        {
            IMapNode? _maxValueNode = map.GetNodeWithHighestValue();
            if (_maxValueNode.HasValue == false)
                return;

            float _maxValue = _maxValueNode.Value.NodeValue;

            for (int i = 0; i < map.NodeList.Length; i++)
            {
                var _node = map.NodeList[i];
                _node.NodeValue = _node.NodeValue / _maxValue;
                map.NodeList[i] = _node;
            }

            map.OnUpdated?.Invoke();
        }

        public static void GateFilter(this IMap map, IMapGateFilter filterType, float threshold, float correctionValue)
        {
            switch (filterType)
            {
                case IMapGateFilter.Greater:

                    for (int i = 0; i < map.NodeList.Length; i++)
                    {
                        var _node = map.NodeList[i];

                        if (_node.NodeValue > threshold)
                            _node.NodeValue = correctionValue;

                        map.NodeList[i] = _node;
                    }

                    break;

                case IMapGateFilter.GreaterOrEqual:

                    for (int i = 0; i < map.NodeList.Length; i++)
                    {
                        var _node = map.NodeList[i];

                        if (_node.NodeValue >= threshold)
                            _node.NodeValue = correctionValue;

                        map.NodeList[i] = _node;
                    }

                    break;

                case IMapGateFilter.Less:

                    for (int i = 0; i < map.NodeList.Length; i++)
                    {
                        var _node = map.NodeList[i];

                        if (_node.NodeValue < threshold)
                            _node.NodeValue = correctionValue;

                        map.NodeList[i] = _node;
                    }

                    break;

                case IMapGateFilter.LessOrEqual:

                    for (int i = 0; i < map.NodeList.Length; i++)
                    {
                        var _node = map.NodeList[i];

                        if (_node.NodeValue <= threshold)
                            _node.NodeValue = correctionValue;

                        map.NodeList[i] = _node;
                    }

                    break;
            }

            map.OnUpdated?.Invoke();
        }
    }
}