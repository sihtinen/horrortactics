using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Unity.Collections;

using ApplicationCore;

namespace Sami.IMaps
{
    public class IMapGenerator : SingletonBehaviour<IMapGenerator>
    {
        public IMap BaseMap { get; private set; } = null;
        public NativeArray<Vector3Int> JobNeigbourOffsets { get; private set; }

        [Header("Debug Settings")]
        [SerializeField] private bool m_drawDebug = false;

        [Header("Object References")]
        [SerializeField] private IMapPool m_mapPool = null;

        protected override void Awake()
        {
            base.Awake();

            JobNeigbourOffsets = new NativeArray<Vector3Int>(new Vector3Int[]
            {
                new Vector3Int(1, 0, 0),
                new Vector3Int(-1, 0, 0),
                new Vector3Int(0, 0, 1),
                new Vector3Int(0, 0, -1),

                new Vector3Int(1, 1, 0),
                new Vector3Int(-1, 1, 0),
                new Vector3Int(0, 1, 1),
                new Vector3Int(0, 1, -1),

                new Vector3Int(1, -1, 0),
                new Vector3Int(-1, -1, 0),
                new Vector3Int(0, -1, 1),
                new Vector3Int(0, -1, -1)

            }, Allocator.Persistent);
        }

        private void generateBaseMap()
        {
            //for (int i = 0; i < tileWorldInstance.AllTileVolumes.Count; i++)
            //{
            //    var _tileVolume = tileWorldInstance.AllTileVolumes[i];

            //    if (_tileVolume.Data.IsWalkable)
            //        _walkableVolumes.Add(_tileVolume);
            //}

            //BaseMap = new IMap();
            //BaseMap.NodeList = new NativeArray<IMapNode>(_walkableVolumes.Count, Allocator.Persistent);
            //BaseMap.NodePositionToListIndexMap = new NativeHashMap<Vector3Int, int>(_walkableVolumes.Count, Allocator.Persistent);
            //BaseMap.IsInitialized = true;

            //for (int i = 0; i < _walkableVolumes.Count; i++)
            //{
            //    TileVolume _volume = _walkableVolumes[i];

            //    BaseMap.NodeList[i] = new IMapNode(_volume.Data.IntPosition);
            //    BaseMap.NodePositionToListIndexMap.Add(_volume.Data.IntPosition, i);
            //}

            m_mapPool.Initialize();
        }

        private void OnDestroy()
        {
            if (BaseMap != null)
                BaseMap.ClearMemory();

            if (JobNeigbourOffsets.IsCreated)
                JobNeigbourOffsets.Dispose();
        }

        private void OnDrawGizmosSelected()
        {
            if (m_drawDebug == false)
                return;

            if (BaseMap == null)
                return;

            Gizmos.color = Color.yellow;

            for (int i = 0; i < BaseMap.NodeList.Length; i++)
            {
                var _node = BaseMap.NodeList[i];
                Gizmos.DrawCube(_node.Position - new Vector3(0, 0.5f, 0), Vector3.one);
            }
        }
    }
}
