using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Unity.Collections;

using ApplicationCore;

namespace Sami.IMaps
{
    public class IMapPool : SingletonBehaviour<IMapPool>
    {
        private ClassObjectPool<IMap> m_mapPool = null;

        public void Initialize()
        {
            m_mapPool = new ClassObjectPool<IMap>();
            m_mapPool.Initialize();
        }

        private void OnDestroy()
        {
            if (m_mapPool != null)
                ClassObjectPool<IMap>.Reset();
        }

        public IMap GetCopyOfBaseMap()
        {
            var _newMap = ClassObjectPool<IMap>.Get();

            if (_newMap.IsInitialized == false)
            {
                var _baseMap = IMapGenerator.Instance.BaseMap;

                _newMap.NodeList = new NativeArray<IMapNode>(_baseMap.NodeList.Length, Allocator.Persistent);
                _newMap.NodePositionToListIndexMap = new NativeHashMap<Vector3Int, int>(_baseMap.NodeList.Length, Allocator.Persistent);
                _newMap.IsInitialized = true;

                for (int i = 0; i < _baseMap.NodeList.Length; i++)
                {
                    var _node = _baseMap.NodeList[i];

                    _newMap.NodeList[i] = _node;
                    _newMap.NodePositionToListIndexMap.Add(_node.Position, i);
                }
            }

            return _newMap;
        }
    }
}