using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;

namespace Sami.IMaps
{
    [BurstCompile]
    public struct GenerateInfluenceJob : IJob
    {
        public Vector3Int GeneratePosition;
        public int GenerateRadius;
        public float GenerateAmount;
        public SampledJobAnimationCurve SampledCurve;

        public NativeArray<IMapNode> NodeList;

        [ReadOnly]
        public NativeHashMap<Vector3Int, int> NodePositionToListIndexMap;

        [ReadOnly]
        public NativeArray<Vector3Int> NeighbourOffsets;

        public NativeList<int> OpenList;
        public NativeHashMap<int, int> ReachableNodeCosts;

        public void Execute()
        {
            if (NodePositionToListIndexMap.ContainsKey(GeneratePosition) == false)
                return;

            int _startNodelistIndex = NodePositionToListIndexMap[GeneratePosition];

            var _startNode = NodeList[_startNodelistIndex];
            _startNode.NodeValue += GenerateAmount;
            NodeList[_startNodelistIndex] = _startNode;

            int _neighbourOptionsCount = NeighbourOffsets.Length;
            OpenList.Add(_startNodelistIndex);
            ReachableNodeCosts.Add(_startNodelistIndex, 0);

            while (OpenList.Length > 0)
            {
                int _currentNodeIndex = OpenList[0];
                removeIndexFromOpenList(_currentNodeIndex);

                int _distance = ReachableNodeCosts[_currentNodeIndex] + 1;

                if (_distance > GenerateRadius)
                    continue;

                IMapNode _currentNode = NodeList[_currentNodeIndex];

                for (int ii = 0; ii < _neighbourOptionsCount; ii++)
                {
                    Vector3Int _neighbourPosition = _currentNode.Position + NeighbourOffsets[ii];

                    if (NodePositionToListIndexMap.ContainsKey(_neighbourPosition) == false)
                        continue;

                    int _neighbourNodeIndex = NodePositionToListIndexMap[_neighbourPosition];

                    if (ReachableNodeCosts.ContainsKey(_neighbourNodeIndex))
                    {
                        if (_distance < ReachableNodeCosts[_neighbourNodeIndex])
                            ReachableNodeCosts[_neighbourNodeIndex] = _distance;
                    }
                    else
                    {
                        OpenList.Add(_neighbourNodeIndex);
                        ReachableNodeCosts.Add(_neighbourNodeIndex, _distance);
                    }
                }
            }

            for (int i = 0; i < NodeList.Length; i++)
            {
                if (ReachableNodeCosts.TryGetValue(i, out int _moveDistance))
                {
                    var _node = NodeList[i];
                    float _distanceNormalized = (float)_moveDistance / GenerateRadius;
                    _node.NodeValue += SampledCurve.EvaluateLerp(_distanceNormalized) * GenerateAmount;
                    NodeList[i] = _node;
                }
            }
        }

        private void removeIndexFromOpenList(int index)
        {
            for (int i = 0; i < OpenList.Length; i++)
            {
                if (OpenList[i] == index)
                {
                    OpenList.RemoveAt(i);
                    break;
                }
            }
        }
    }
}
