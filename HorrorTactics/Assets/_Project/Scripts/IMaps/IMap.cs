using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Unity.Collections;
using Unity.Jobs;
using System;

namespace Sami.IMaps
{
    [System.Serializable]
    public class IMapGenerateInfluenceParameters
    {
        public float Amount = 0;
        public int Radius = 0;
        public AnimationCurve InfluenceCurveOverDistance = new AnimationCurve();
    }

    public class IMap : Poolable
    {
        public bool IsInitialized = false;
        public NativeArray<IMapNode> NodeList;
        public NativeHashMap<Vector3Int, int> NodePositionToListIndexMap;

        public Action OnUpdated = null;

        public override void ClearMemory()
        {
            IsInitialized = false;

            if (NodeList.IsCreated)
                NodeList.Dispose();

            if (NodePositionToListIndexMap.IsCreated)
                NodePositionToListIndexMap.Dispose();
        }

        public void GenerateInfluence(Vector3Int position, IMapGenerateInfluenceParameters parameters)
        {
            NativeList<int> _openList = new NativeList<int>(Allocator.TempJob);
            NativeHashMap<int, int> _reachableNodeCosts = new NativeHashMap<int, int>(NodeList.Length, Allocator.TempJob);
            SampledJobAnimationCurve _sampledCurve = new SampledJobAnimationCurve(parameters.InfluenceCurveOverDistance, parameters.Radius, Allocator.TempJob);

            GenerateInfluenceJob _job = new GenerateInfluenceJob()
            {
                GeneratePosition = position,
                GenerateAmount = parameters.Amount,
                GenerateRadius = parameters.Radius,
                SampledCurve = _sampledCurve,

                NodeList = NodeList,
                NodePositionToListIndexMap = NodePositionToListIndexMap,

                NeighbourOffsets = IMapGenerator.Instance.JobNeigbourOffsets,
                OpenList = _openList,
                ReachableNodeCosts = _reachableNodeCosts
            };

            var _jobHandle = _job.Schedule();
            _jobHandle.Complete();

            _openList.Dispose();
            _reachableNodeCosts.Dispose();
            _sampledCurve.Dispose();

            OnUpdated?.Invoke();
        }

        public IMapNode? GetNodeWithHighestValue()
        {
            IMapNode? _result = null;

            for (int i = 0; i < NodeList.Length; i++)
            {
                if (_result.HasValue == false)
                {
                    _result = NodeList[i];
                    continue;
                }

                if (NodeList[i].NodeValue > _result.Value.NodeValue)
                    _result = NodeList[i];
            }

            return _result;
        }

        public IMapNode? GetNodeWithLowestValue()
        {
            IMapNode? _result = null;

            for (int i = 0; i < NodeList.Length; i++)
            {
                if (_result.HasValue == false)
                {
                    _result = NodeList[i];
                    continue;
                }

                if (NodeList[i].NodeValue < _result.Value.NodeValue)
                    _result = NodeList[i];
            }

            return _result;
        }

        public void GenerateAllListIndicesAboveThreshold(float threshold, ref List<int> cachedIndiceList)
        {
            cachedIndiceList.Clear();

            for (int i = 0; i < NodeList.Length; i++)
            {
                if (NodeList[i].NodeValue >= threshold)
                    cachedIndiceList.Add(i);
            }
        }
    }

    public struct IMapNode
    {
        public IMapNode(Vector3Int position)
        {
            Position = position;
            NodeValue = 0;
        }

        public Vector3Int Position;
        public float NodeValue;

        public void DrawGizmos(Color color)
        {
            Gizmos.color = color;
            Gizmos.DrawCube(Position - new Vector3(0, 0.5f, 0), Vector3.one * 1.02f);
        }
    }
}