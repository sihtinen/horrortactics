using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Sami.IMaps;
using UnityEngine.Profiling;

public class FactionProximityIMapManager : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private IMapGenerateInfluenceParameters m_proximityGenerationSettings = new IMapGenerateInfluenceParameters();

    [Header("Debug Settings")]
    [SerializeField] private bool m_drawDebug = false;
    [SerializeField] private bool m_enableProfiling = false;
    [SerializeField] private List<Color> m_debugColors = new List<Color>();

    [Header("Object References")]
    [SerializeField] private List<FactionInstance> m_factions = new List<FactionInstance>();

    private Dictionary<FactionInstance, IMap> m_factionMaps = new Dictionary<FactionInstance, IMap>();

    private void Start()
    {
        for (int i = 0; i < m_factions.Count; i++)
        {
            var _faction = m_factions[i];
            IMap _newMap = IMapPool.Instance.GetCopyOfBaseMap();
            m_factionMaps.Add(_faction, _newMap);
        }

        updateMap();
    }

    private void updateMap()
    {
        if (m_enableProfiling)
            Profiler.BeginSample("Faction Proximity IMap Update");

        for (int i = 0; i < m_factions.Count; i++)
        {
            var _faction = m_factions[i];
            var _factionMap = m_factionMaps[_faction];

            _factionMap.SetToZero();

            for (int ii = 0; ii < _faction.ActiveCharacters.Count; ii++)
            {
                var _character = _faction.ActiveCharacters[ii];

                _factionMap.GenerateInfluence(
                    position: _character.MoveComponent.IntPosition,
                    parameters: m_proximityGenerationSettings);
            }
        }

        if (m_enableProfiling)
            Profiler.EndSample();
    }

    private void OnDrawGizmos()
    {
        if (m_drawDebug == false)
            return;

        for (int i = 0; i < m_factions.Count; i++)
        {
            var _faction = m_factions[i];

            if (m_factionMaps.TryGetValue(_faction, out var _map))
            {
                if (m_debugColors.Count > i)
                    Gizmos.color = m_debugColors[i];

                Color _color = Gizmos.color;

                for (int ii = 0; ii < _map.NodeList.Length; ii++)
                {
                    var _node = _map.NodeList[ii];

                    if (_node.NodeValue > 0)
                    {
                        Gizmos.color = _color * _node.NodeValue;
                        Gizmos.DrawCube(_node.Position - new Vector3(0, 0.5f, 0), Vector3.one * 1.02f);
                    }
                }
            }
        }
    }
}
