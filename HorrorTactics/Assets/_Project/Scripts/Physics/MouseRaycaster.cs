using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

using ApplicationCore;

public class MouseRaycaster : SingletonBehaviour<MouseRaycaster>
{
    public event Action<Vector3> OnHovered_NavMesh = null;
    public event Action<Vector3> OnClicked_NavMesh = null;

    public event Action<ControllableCharacter> OnHovered_ControllableCharacter = null;
    public event Action<ControllableCharacter> OnClicked_ControllableCharacter = null;

    public event Action<AICharacter> OnHovered_AICharacter = null;
    public event Action<AICharacter> OnClicked_AICharacter = null;

    public Vector3? HitNavMeshPoint { get; private set; } = null;
    public AICharacter HitAICharacter { get; private set; } = null;
    public ControllableCharacter HitControllableCharacter { get; private set; } = null;

    [Header("Settings")]
    [SerializeField] private LayerMask m_hitLayers;

    private RaycastHit[] m_hits = new RaycastHit[32];

    private void Start()
    {
        MEC.TimingUtility.SubscribeToUpdate(mecUpdate, this);
    }

    private void mecUpdate()
    {
        if (ApplicationCoreManager.IsGamePausedOrLoading) return;
        if (ScreenStateManager.Instance.CurrentScreenState != ScreenStateType.Gameplay) return;

        Vector3? _navMeshPoint = null;
        ControllableCharacter _controllableCharacter = null;
        AICharacter _aiCharacter = null;

        Ray _ray = MainCameraComponent.Camera.ScreenPointToRay(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);
        int _hitCount = Physics.RaycastNonAlloc(_ray, m_hits, 1000, m_hitLayers, QueryTriggerInteraction.Ignore);

        if (_hitCount > 0)
        {
            RaycastHit _closest = m_hits.GetClosest(_hitCount);
            Transform _target = _closest.transform;

            if (_target != null)
            {
                bool _navMeshPointFound = NavMesh.SamplePosition(_closest.point, out var _navMeshHit, 0.5f, NavMesh.AllAreas);
                if (_navMeshPointFound)
                    _navMeshPoint = _navMeshHit.position;

                IMouseRaycasterTarget _raycasterTarget = _target.GetComponent<IMouseRaycasterTarget>();
                if (_raycasterTarget != null)
                    _target = _raycasterTarget.RootTransform;

                _controllableCharacter = _target.GetComponent<ControllableCharacter>();
                _aiCharacter = _target.GetComponent<AICharacter>();
            }
        }

        processNavMeshPoint(_navMeshPoint);

        if (_controllableCharacter != HitControllableCharacter)
        {
            HitControllableCharacter = _controllableCharacter;
            OnHovered_ControllableCharacter?.Invoke(HitControllableCharacter);
        }

        if (_aiCharacter != HitAICharacter)
        {
            HitAICharacter = _aiCharacter;
            OnHovered_AICharacter?.Invoke(HitAICharacter);
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (HitNavMeshPoint.HasValue)
                OnClicked_NavMesh?.Invoke(HitNavMeshPoint.Value);

            if (HitControllableCharacter)
                OnClicked_ControllableCharacter?.Invoke(HitControllableCharacter);

            if (HitAICharacter)
                OnClicked_AICharacter?.Invoke(HitAICharacter);
        }
    }

    private void processNavMeshPoint(Vector3? _navMeshPoint)
    {
        if (_navMeshPoint.HasValue != HitNavMeshPoint.HasValue)
        {
            HitNavMeshPoint = _navMeshPoint;

            if (HitNavMeshPoint.HasValue)
                OnHovered_NavMesh?.Invoke(HitNavMeshPoint.Value);
        }
        else if (_navMeshPoint.HasValue && HitNavMeshPoint.HasValue)
        {
            if (_navMeshPoint.Value != HitNavMeshPoint.Value)
            {
                HitNavMeshPoint = _navMeshPoint;
                OnHovered_NavMesh?.Invoke(HitNavMeshPoint.Value);
            }
        }
    }
}
