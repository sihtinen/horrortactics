using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public interface IMouseRaycasterTarget
{
    public Transform RootTransform { get; }
}
