using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class MouseRaycasterRootProxy : MonoBehaviour, IMouseRaycasterTarget
{
    [SerializeField] private Transform m_rootTransform = null;

    public Transform RootTransform => m_rootTransform;
}
