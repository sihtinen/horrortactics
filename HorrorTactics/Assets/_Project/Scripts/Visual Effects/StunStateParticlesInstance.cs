using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public class StunStateParticlesInstance : MonoBehaviour
{
    [Header("Object References")]
    [SerializeField] private ParticleSystem m_particleSystem = null;

    public void Bind(CharacterBase character)
    {
        transform.position = character.MoveComponent.IntPosition;
        gameObject.SetActiveOptimized(true);
        m_particleSystem.Play();
    }

    public void Release()
    {
        m_particleSystem.Stop();

        StunStateParticlesPool.ReturnObject(this);
    }
}