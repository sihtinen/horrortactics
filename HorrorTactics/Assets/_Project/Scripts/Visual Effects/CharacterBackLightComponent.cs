using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class CharacterBackLightComponent : MonoBehaviour
{
    [SerializeField] private float m_characterVerticalOffset = 0;
    [SerializeField, Min(0)] private float m_pushBackOffset = 3.0f;

    private Transform m_targetCharacter = null;
    private Transform m_cameraTransform = null;

    private void Awake()
    {
        m_targetCharacter = transform.parent;
    }

    private void Start()
    {
        transform.SetParent(null);
        m_cameraTransform = MainCameraComponent.Camera.transform;

        MEC.TimingUtility.SubscribeToUpdate(this.onUpdate, this);
    }

    private void onUpdate()
    {
        if (m_targetCharacter == null ||
            m_cameraTransform == null)
            return;

        Vector3 _pivotPos = m_targetCharacter.position + new Vector3(0, m_characterVerticalOffset, 0);
        Vector3 _fromCameraDir = (_pivotPos - m_cameraTransform.position).normalized;
        Vector3 _moveOffset = _fromCameraDir * m_pushBackOffset;

        transform.position = _pivotPos + _moveOffset;
        transform.forward = -_fromCameraDir;
    }
}
