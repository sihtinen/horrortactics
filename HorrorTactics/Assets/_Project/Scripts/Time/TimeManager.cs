using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

using MEC;

public class TimeManager : SingletonBehaviour<TimeManager>
{
    public static float DeltaTime_GameTime
    {
        get
        {
            if (Instance != null)
                return Timing.DeltaTime * Instance.GameTimeSpeed;

            return Timing.DeltaTime;
        }
    }

    public float GameTimeSpeed { get; private set; } = 1.0f;
}
