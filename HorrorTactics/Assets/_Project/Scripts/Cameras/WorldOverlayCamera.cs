using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public class WorldOverlayCamera : SingletonBehaviour<WorldOverlayCamera>
{
    private Camera m_camera = null;
    private List<MonoBehaviour> m_registeredUsers = new List<MonoBehaviour>();

    protected override void Awake()
    {
        base.Awake();

        m_camera = GetComponent<Camera>();
    }

    private void Start()
    {
        MEC.TimingUtility.SubscribeToUpdate(mecLateUpdate, this, MEC.Segment.LateUpdate);
    }

    private void mecLateUpdate()
    {
        m_camera.fieldOfView = MainCameraComponent.Camera.fieldOfView;

        bool _enabled = m_registeredUsers.Count > 0;

        if (m_camera.enabled != _enabled)
            m_camera.enabled = _enabled;
    }

    public static void RegisterUser(MonoBehaviour user)
    {
        if (Instance == null)
            return;

        if (Instance.m_registeredUsers.Contains(user) == false)
            Instance.m_registeredUsers.Add(user);
    }

    public static void UnregisterUser(MonoBehaviour user)
    {
        if (Instance == null)
            return;

        if (Instance.m_registeredUsers.Contains(user))
            Instance.m_registeredUsers.Remove(user);
    }
}
