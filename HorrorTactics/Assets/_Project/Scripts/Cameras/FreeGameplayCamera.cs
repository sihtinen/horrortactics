using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Cinemachine;

using Sirenix.OdinInspector;

using ApplicationCore;


public class FreeGameplayCamera : SingletonBehaviour<FreeGameplayCamera>
{
    public enum CameraMode
    {
        PlayerControl = 0,
        FocusOnCharacter = 1
    }

    [Header("Object References")]
    [SerializeField, InlineEditor] private FreeCameraSettings m_cameraSettings = null;
    [SerializeField] private CinemachineVirtualCamera[] m_cams = null;
    [SerializeField] private FactionInstance m_playerFaction = null;

    private Transform m_cameraPivot = null;
    private CinemachineMixingCamera m_mixingCamera = null;
    private CinemachineOrbitalTransposer[] m_orbitalTransposers = null;
    private CinemachineComposer[] m_composers = null;

    private bool m_mousePressedPreviousFrame = false;

    private Vector2 m_previousMousePosition = Vector2.zero;

    private Vector3 m_movementTarget = Vector3.zero;
    private Vector3 m_currentMovementPosition = Vector3.zero;
    private Vector3 m_currentMovementVelocity = Vector3.zero;

    private float m_rotationTarget = 0.0f;
    private float m_currentRotationValue = 0.0f;
    private float m_currentRotationVelocity = 0.0f;
    private float m_zoomTarget = 0.5f;
    private float m_currentZoomValue = 0.5f;
    private float m_currentZoomVelocity = 0.0f;

    private CameraMode m_currentCameraMode = CameraMode.PlayerControl;

    protected override void Awake()
    {
        base.Awake();

        m_cameraPivot = new GameObject("Free Camera Pivot").transform;
        m_cameraPivot.SetParent(null);
        m_cameraPivot.position = Vector3.zero;

        m_mixingCamera = GetComponent<CinemachineMixingCamera>();
        m_orbitalTransposers = new CinemachineOrbitalTransposer[m_cams.Length];
        m_composers = new CinemachineComposer[m_cams.Length];

        for (int i = 0; i < m_cams.Length; i++)
        {
            m_cams[i].Follow = m_cameraPivot;
            m_cams[i].LookAt = m_cameraPivot;

            m_orbitalTransposers[i] = m_cams[i].GetCinemachineComponent<CinemachineOrbitalTransposer>();
            m_composers[i] = m_cams[i].GetCinemachineComponent<CinemachineComposer>();
        }
    }

    private void Start()
    {
        m_movementTarget = m_playerFaction.ActiveCharacters[0].transform.position;
        m_currentCameraMode = CameraMode.PlayerControl;

        MEC.TimingUtility.SubscribeToUpdate(mecLateUpdate, this, MEC.Segment.LateUpdate);
    }

    public void SetTargetPosition(Vector3 pos)
    {
        m_movementTarget = pos;
    }

    private void mecLateUpdate()
    {
        if (ScreenStateManager.Instance.CurrentScreenState != ScreenStateType.Gameplay)
            return;

        calculateCameraValues(MEC.Timing.DeltaTime);

        applyWorldHeight(Time.unscaledDeltaTime);
        applyCameraMovement(Time.unscaledDeltaTime);
        applyCameraRotation();
        applyCameraZoom();
    }

    private void calculateCameraValues(float deltaTime)
    {
        if (Input.GetMouseButton(0) || Input.GetMouseButton(1) || Input.GetMouseButton(2))
        {
            if (m_mousePressedPreviousFrame == false)
            {
                m_previousMousePosition = Input.mousePosition;
                m_mousePressedPreviousFrame = true;
            }
            else
            {
                Vector2 _currentMousePos = Input.mousePosition;
                Vector2 _mouseDiff = _currentMousePos - m_previousMousePosition;
                m_previousMousePosition = _currentMousePos;

                if (m_currentCameraMode == CameraMode.PlayerControl)
                {
                    float _movementSpeed = Mathf.Lerp(
                        m_cameraSettings.MovementSpeed_ZoomOut, 
                        m_cameraSettings.MovementSpeed_ZoomIn, 
                        m_currentZoomValue);

                    if (Input.GetMouseButton(2))
                        m_movementTarget +=
                            MainCameraComponent.HorizontalForward * _mouseDiff.y * -_movementSpeed +
                            MainCameraComponent.HorizontalRight * _mouseDiff.x * -_movementSpeed;
                }

                if (Input.GetMouseButton(1))
                    m_rotationTarget += _mouseDiff.x * m_cameraSettings.RotationSpeed;
            }
        }
        else
        {
            m_mousePressedPreviousFrame = false;
        }

        switch (m_currentCameraMode)
        {
            case CameraMode.PlayerControl:

                float _keyMoveDelta = deltaTime * m_cameraSettings.KeyMovementSpeed;

                if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
                    m_movementTarget += MainCameraComponent.HorizontalRight * _keyMoveDelta;
                else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
                    m_movementTarget -= MainCameraComponent.HorizontalRight * _keyMoveDelta;

                if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
                    m_movementTarget += MainCameraComponent.HorizontalForward * _keyMoveDelta;
                else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
                    m_movementTarget -= MainCameraComponent.HorizontalForward * _keyMoveDelta;

                if (Input.GetKey(KeyCode.E))
                    m_rotationTarget -= m_cameraSettings.KeyRotationSpeed * deltaTime;
                else if (Input.GetKey(KeyCode.Q))
                    m_rotationTarget += m_cameraSettings.KeyRotationSpeed * deltaTime;

                break;

            case CameraMode.FocusOnCharacter:


                break;
        }
    }

    private const float m_coordinateMin = 0.4f;
    private const float m_coordinateMax = 0.6f;

    private static readonly Vector3[] m_heightRaycastCoordinates = new Vector3[5]
    {
        new Vector3(m_coordinateMin, m_coordinateMin, 0),
        new Vector3(m_coordinateMax, m_coordinateMin, 0),
        new Vector3(0.5f, 0.5f, 0),
        new Vector3(m_coordinateMin, m_coordinateMax, 0),
        new Vector3(m_coordinateMax, m_coordinateMax, 0),
    };

    private void applyWorldHeight(float unscaledDeltaTime)
    {
        float _highestWorldHeight = 0;

        for (int i = 0; i < 5; i++)
        {
            var _ray = MainCameraComponent.Camera.ViewportPointToRay(m_heightRaycastCoordinates[i], Camera.MonoOrStereoscopicEye.Mono);
            bool _hit = Physics.Raycast(_ray, out var _hitInfo, 1000f, m_cameraSettings.WorldHeightCollisionLayers);

            if (_hit && _hitInfo.point.y > _highestWorldHeight)
                _highestWorldHeight = _hitInfo.point.y;
        }

        m_movementTarget.y = Mathf.Lerp(m_movementTarget.y, _highestWorldHeight, m_cameraSettings.HeightCorrectionSpeed * unscaledDeltaTime);
    }

    private void applyCameraMovement(float unscaledDeltaTime)
    {
        m_currentMovementPosition = Vector3.SmoothDamp(
            m_currentMovementPosition,
            m_movementTarget,
            ref m_currentMovementVelocity,
            m_cameraSettings.MovementSmoothing * unscaledDeltaTime);

        m_cameraPivot.position = m_currentMovementPosition;
    }

    private void applyCameraRotation()
    {
        m_currentRotationValue = Mathf.SmoothDamp(
            m_currentRotationValue,
            m_rotationTarget,
            ref m_currentRotationVelocity,
            m_cameraSettings.RotationSmoothing * Time.unscaledDeltaTime);

        for (int i = 0; i < m_orbitalTransposers.Length; i++)
        {
            m_orbitalTransposers[i].m_XAxis.Value = m_currentRotationValue;
        }
    }

    private void applyCameraZoom()
    {
        if (Mathf.Abs(Input.mouseScrollDelta.y) > 0.1f)
        {
            m_zoomTarget += Input.mouseScrollDelta.y / m_cameraSettings.ZoomScrollWheelSteps;
            m_zoomTarget = Mathf.Clamp01(m_zoomTarget);
        }

        m_currentZoomValue = Mathf.SmoothDamp(
            m_currentZoomValue,
            m_zoomTarget,
            ref m_currentZoomVelocity,
            m_cameraSettings.ZoomSmoothing * Time.unscaledDeltaTime);

        m_mixingCamera.SetWeight(0, m_currentZoomValue);
        m_mixingCamera.SetWeight(1, 1.0f - m_currentZoomValue);
    }
}
