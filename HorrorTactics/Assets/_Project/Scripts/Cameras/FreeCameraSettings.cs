using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Cameras/Free Camera Settings", fileName = "Free Camera Settings")]
public class FreeCameraSettings : ScriptableObject
{
    [Header("Settings")]
    public float RotationSpeed = 0.2f;
    public float RotationSmoothing = 0.2f;
    [Space]
    public float MovementSpeed_ZoomIn = 0.12f;
    public float MovementSpeed_ZoomOut = 0.2f;
    public float MovementSmoothing = 0.2f;
    [Space]
    public int ZoomScrollWheelSteps = 6;
    public float ZoomSmoothing = 0.2f;
    [Space]
    public float KeyMovementSpeed = 2.2f;
    public float KeyRotationSpeed = 2.2f;

    [Header("Height Correction")]
    public LayerMask WorldHeightCollisionLayers;
    public float HeightCorrectionSpeed = 3f;
}