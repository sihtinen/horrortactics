using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public class MainCameraComponent : MonoBehaviour
{
    public static Vector3 HorizontalForward = Vector3.zero;
    public static Vector3 HorizontalRight = Vector3.zero;

    public static Camera Camera = null;

    private void Awake()
    {
        Camera = GetComponent<Camera>();
    }

    private void Start()
    {
        MEC.TimingUtility.SubscribeToUpdate(mecUpdate, this);
    }

    private void mecUpdate()
    {
        HorizontalForward = transform.forward;
        HorizontalForward.y = 0;
        HorizontalForward.Normalize();

        HorizontalRight = transform.right;
        HorizontalRight.y = 0;
        HorizontalRight.Normalize();
    }
}
