using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public class MouseTargetVisualizer : SingletonBehaviour<MouseTargetVisualizer>
{
    public enum VisualizationType
    {
        Movement,
        RangedAbility
    }

    private VisualizationType m_currentVisualizationType = VisualizationType.Movement;

    protected override void Awake()
    {
        base.Awake();

        SetActive(false);
    }

    public static void SetVisualizationType(VisualizationType newType)
    {
        if (Instance == null)
            return;

        Instance.m_currentVisualizationType = newType;
    }

    public static void SetPosition(Vector3 position)
    {
        if (Instance == null)
            return;

        Instance.transform.position = position;
    }

    public static void SetActive(bool isActive)
    {
        if (Instance == null)
            return;

        if (Instance.gameObject.activeSelf != isActive)
            Instance.gameObject.SetActive(isActive);

        if (isActive)
            WorldOverlayCamera.RegisterUser(Instance);
        else
            WorldOverlayCamera.UnregisterUser(Instance);
    }
}
