using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using TMPro;

using Febucci.UI;

using Pixelplacement;

using ApplicationCore;

using MEC;

public class UI_DialogueCanvas : SingletonBehaviour<UI_DialogueCanvas>
{
    [Header("Object References")]
    [SerializeField] private RectTransform m_dialogueContentRoot = null;
    [Space]
    [SerializeField] private TMP_Text m_speakerText = null;
    [SerializeField] private TMP_Text m_dialogueContentText = null;
    [Space]
    [SerializeField] private TextAnimator m_textAnimator = null;
    [SerializeField] private TextAnimatorPlayer m_textAnimatorPlayer = null;
    [Space]
    [SerializeField] private DialogueAsset m_debugDialogue = null;

    private bool m_isPlaying = false;
    private bool m_typeWriterPlaying = false;

    private Canvas m_canvas = null;
    private DialogueAsset m_currentDialogueAsset = null;

    protected override void Awake()
    {
        base.Awake();

        m_canvas = GetComponent<Canvas>();
        m_canvas.enabled = false;

        m_textAnimatorPlayer.onTextShowed.AddListener(this.onTextShowed);
    }

    private void Start()
    {
        TimingUtility.SubscribeToUpdate(mecUpdate, this);
    }

    private void onTextShowed()
    {
        m_typeWriterPlaying = false;
    }

    private void mecUpdate()
    {
        if (Input.GetKeyDown(KeyCode.L))
            StartDialogue(m_debugDialogue);
    }

    public void StartDialogue(DialogueAsset dialogueAsset)
    {
        if (m_isPlaying)
            return;

        ScreenStateManager.Instance.SetScreenState(ScreenStateType.Dialogue);

        m_isPlaying = true;
        m_canvas.enabled = true;
        m_currentDialogueAsset = dialogueAsset;

        Tween.LocalScale(
            target: m_dialogueContentRoot,
            startValue: new Vector3(0.8f, 0.0f, 1.0f),
            endValue: Vector3.one,
            duration: 0.3f,
            delay: 0.0f,
            easeCurve: Tween.EaseOutStrong,
            obeyTimescale: false,
            completeCallback: this.onTweenCompleted);
    }

    private void onTweenCompleted()
    {
        Timing.RunCoroutine(coroutine_updateDialogue());
    }

    private IEnumerator<float> coroutine_updateDialogue()
    {
        int _playbackPosition = 0;

        while (m_isPlaying)
        {
            string _speakerText = m_currentDialogueAsset.GetSpeakerAtPlaybackPosition(_playbackPosition);
            m_speakerText.SetText(_speakerText);

            string _contentText = m_currentDialogueAsset.GetLineAtPlaybackPosition(_playbackPosition);

            m_typeWriterPlaying = true;

            m_textAnimator.SetText(_contentText, true);
            m_textAnimatorPlayer.StartShowingText();

            while (m_typeWriterPlaying)
            {
                if (Input.GetMouseButtonDown(0))
                    m_textAnimatorPlayer.SkipTypewriter();

                yield return Timing.WaitForOneFrame;
            }

            while (Input.GetMouseButtonDown(0) == false)
                yield return Timing.WaitForOneFrame;

            yield return Timing.WaitForOneFrame;

            _playbackPosition++;

            if (m_currentDialogueAsset.IsLastLine(_playbackPosition))
                m_isPlaying = false;
        }

        closeAnimation();
    }

    private void closeAnimation()
    {
        m_speakerText.SetText(string.Empty);
        m_dialogueContentText.SetText(string.Empty);

        Tween.LocalScale(
            m_dialogueContentRoot,
            Vector3.one,
            new Vector3(0.8f, 0.0f, 1.0f),
            0.1f,
            0.0f,
            easeCurve: Tween.EaseOutStrong,
            obeyTimescale: false,
            completeCallback: this.close);
    }

    private void close()
    {
        m_canvas.enabled = false;
        m_currentDialogueAsset = null;

        ScreenStateManager.Instance.SetScreenState(ScreenStateType.Gameplay);
    }
}
