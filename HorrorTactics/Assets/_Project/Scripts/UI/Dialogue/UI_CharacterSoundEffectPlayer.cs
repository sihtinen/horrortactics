using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public class UI_CharacterSoundEffectPlayer : SingletonBehaviour<UI_CharacterSoundEffectPlayer>
{
    [Header("Settings")]
    [SerializeField, Range(0.0f, 1.0f)] private float m_percentageTimePlayed = 0.5f;
    [SerializeField] private List<CharacterSoundEffectType> m_characterSoundEffects = new List<CharacterSoundEffectType>();

    public void PlaySoundEffect(char character)
    {
        if (Random.Range(0.0f, 1.0f) > m_percentageTimePlayed)
            return;

        character = char.ToLower(character);

        for (int i = 0; i < m_characterSoundEffects.Count; i++)
        {
            m_characterSoundEffects[i].PlayIfMatchingCharacter(character);
        }
    }

    [System.Serializable]
    private class CharacterSoundEffectType
    {
        [SerializeField] private Sami.Audio.FMODEventAsset m_audioEvent = null;
        [SerializeField] private List<char> m_characters = new List<char>();

        public void PlayIfMatchingCharacter(char character)
        {
            if (m_characters.Contains(character) == false)
                return;

            m_audioEvent.PlayAtPosition(Vector3.zero);
        }
    }
}