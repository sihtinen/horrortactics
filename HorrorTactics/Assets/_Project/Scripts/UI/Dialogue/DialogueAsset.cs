using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Sirenix.OdinInspector;

[System.Serializable]
[CreateAssetMenu(menuName = "Dialogue/New Dialogue Asset", fileName = "New Dialogue Asset")]
public class DialogueAsset : ScriptableObject
{
    [System.Serializable]
    public class DialogueLine
    {
        public string LineContent = string.Empty;
    }

    [System.Serializable]
    public class DialogueLineSequence
    {
        public string Speaker = "Speaker";
        public List<DialogueLine> Lines = new List<DialogueLine>();
    }

    [Header("Dialogue Content")]
    [SerializeField] private List<DialogueLineSequence> m_lineSequences = new List<DialogueLineSequence>();

    public bool IsLastLine(int playbackPosition)
    {
        for (int i = 0; i < m_lineSequences.Count; i++)
        {
            for (int ii = 0; ii < m_lineSequences[i].Lines.Count; ii++)
                playbackPosition--;
        }

        return playbackPosition >= 0;
    }

    public string GetLineAtPlaybackPosition(int playbackPosition)
    {
        for (int i = 0; i < m_lineSequences.Count; i++)
        {
            for (int ii = 0; ii < m_lineSequences[i].Lines.Count; ii++)
            {
                if (playbackPosition == 0) 
                    return m_lineSequences[i].Lines[ii].LineContent;

                playbackPosition--;
            }
        }

        return string.Empty;
    }

    public string GetSpeakerAtPlaybackPosition(int playbackPosition)
    {
        for (int i = 0; i < m_lineSequences.Count; i++)
        {
            for (int ii = 0; ii < m_lineSequences[i].Lines.Count; ii++)
            {
                if (playbackPosition == 0)
                    return m_lineSequences[i].Speaker;

                playbackPosition--;
            }
        }

        return string.Empty;
    }
}