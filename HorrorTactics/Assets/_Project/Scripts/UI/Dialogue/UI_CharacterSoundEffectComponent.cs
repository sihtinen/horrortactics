using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Febucci.UI;

public class UI_CharacterSoundEffectComponent : MonoBehaviour
{
    private TextAnimatorPlayer m_animatorPlayer = null;

    private void Awake()
    {
        m_animatorPlayer = GetComponent<TextAnimatorPlayer>();
        m_animatorPlayer.onCharacterVisible.AddListener(this.onCharacterVisible);
    }

    private void onCharacterVisible(char character)
    {
        UI_CharacterSoundEffectPlayer.Instance.PlaySoundEffect(character);
    }
}
