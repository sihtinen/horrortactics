using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public class UI_DamagePopupSystem : MonoBehaviour
{
    public static Camera MainCamera = null;
 
    [Header("Object References")]
    [SerializeField] private UI_DamagePopupInstance m_instancePrefab = null;

    private static UI_DamagePopupSystem m_instance = null;
    private List<UI_DamagePopupInstance> m_pool = new List<UI_DamagePopupInstance>();

    private void Awake()
    {
        m_instance = this;

        for (int i = 0; i < 32; i++)
        {
            GameObject _g = Instantiate(m_instancePrefab.gameObject, transform);
            _g.SetActive(false);
            m_pool.Add(_g.GetComponent<UI_DamagePopupInstance>());
        }
    }

    private void Start()
    {
        MainCamera = FindObjectOfType<Camera>();
    }

    public static void PlayAtPosition(int damage, Transform target)
    {
        UI_DamagePopupInstance _popupInstance = m_instance.m_pool[0];
        m_instance.m_pool.Remove(_popupInstance);

        _popupInstance.PlayAtPosition(damage.ToString(), target);
    }

    public static void ReturnToPool(UI_DamagePopupInstance popupInstance)
    {
        m_instance.m_pool.Add(popupInstance);
    }
}
