using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using TMPro;

using MEC;

public class UI_DamagePopupInstance : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField, Min(0.001f)] private float m_lifeTime = 2.0f;
    [SerializeField] private float m_verticalPositionOffset = 2.5f;

    [Header("Object References")]
    [SerializeField] private TMP_Text m_text = null;
    [SerializeField] private RectTransform m_rootRectTransform = null;

    public void PlayAtPosition(string textContent, Transform target)
    {
        m_text.SetText(textContent);
        gameObject.SetActive(true);

        Timing.RunCoroutine(coroutine_lifetime(target));
    }

    private IEnumerator<float> coroutine_lifetime(Transform target)
    {
        float _timer = 0f;

        while (_timer < m_lifeTime)
        {
            float _normalized = _timer / m_lifeTime;
            _timer += Time.unscaledDeltaTime;

            m_rootRectTransform.position = 
                UI_DamagePopupSystem.MainCamera.WorldToScreenPoint(target.position + Vector3.up * m_verticalPositionOffset);

            yield return Timing.WaitForOneFrame;
        }

        UI_DamagePopupSystem.ReturnToPool(this);
        gameObject.SetActive(false);
    }
}
