using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using ApplicationCore;

using TMPro;

public class UI_AIDebugElement : MonoBehaviour
{
    private class MultiTextElementWrapper
    {
        public GameObject Parent = null;
        public TMP_Text TextLeft = null;
        public TMP_Text TextRight = null;
    }

    [Header("Object References")]
    [SerializeField] private TMP_Text m_nameText = null;
    [SerializeField] private Slider m_healthSlider = null;
    [SerializeField] private TMP_Text m_awarenessText = null;
    [SerializeField] private GameObject m_statusEffectGroup = null;

    private RectTransform m_rectTransform = null;
    private AICharacter m_characterBinding = null;

    private List<MultiTextElementWrapper> m_statusEffectElements = null;

    private const float VERTICAL_POS_OFFSET = -0.05f;

    private void Awake()
    {
        m_rectTransform = GetComponent<RectTransform>();
        m_statusEffectElements = new List<MultiTextElementWrapper>(3);

        for (int i = 1; i < m_statusEffectGroup.transform.childCount; i++)
        {
            GameObject _statusTextParent = m_statusEffectGroup.transform.GetChild(i).gameObject;
            TMP_Text _textLeft = _statusTextParent.transform.GetChild(0).GetComponent<TMP_Text>();
            TMP_Text _textRight = _statusTextParent.transform.GetChild(1).GetComponent<TMP_Text>();

            m_statusEffectElements.Add(new MultiTextElementWrapper()
            { 
                Parent = _statusTextParent,
                TextLeft = _textLeft,
                TextRight = _textRight
            });
        }
    }

    private void Start()
    {
        MEC.TimingUtility.SubscribeToUpdate(mecUpdate, this);
    }

    public void BindToCharacter(AICharacter aiCharacter)
    {
        m_characterBinding = aiCharacter;

        m_nameText.SetText(m_characterBinding.gameObject.name);

        onHealthUpdated();
        onAggressionUpdated();
        onStatusEffectsUpdated();

        m_characterBinding.Health.OnHealthUpdated += this.onHealthUpdated;
        m_characterBinding.OnStatusEffectsUpdated += this.onStatusEffectsUpdated;
        m_characterBinding.OnObjectDestroyed += this.ReleaseBinding;

        gameObject.SetActive(true);
    }

    private void mecUpdate()
    {
        Vector3 _screenPos = 
            MainCameraComponent.Camera.WorldToScreenPoint(m_characterBinding.transform.position);

        _screenPos.y += Screen.height * VERTICAL_POS_OFFSET;

        m_rectTransform.position = _screenPos;
    }

    public void ReleaseBinding()
    {
        m_characterBinding.Health.OnHealthUpdated -= this.onHealthUpdated;
        m_characterBinding.OnStatusEffectsUpdated -= this.onStatusEffectsUpdated;
        m_characterBinding.OnObjectDestroyed -= this.ReleaseBinding;

        m_characterBinding = null;

        AIDebugElementPool.ReturnObject(this);
    }

    private void onHealthUpdated()
    {
        int _currentHealth = m_characterBinding.Health.CurrentHealth;
        int _maxHealth = m_characterBinding.Health.Parameters.MaxHealth;

        float _normalized = (float)_currentHealth / (float)_maxHealth;

        m_healthSlider.SetValueWithoutNotify(_normalized);
    }

    private void onAggressionUpdated()
    {
        m_awarenessText.SetText(m_characterBinding.ThreatComponent.AggressionState.ToString());
    }

    private void onStatusEffectsUpdated()
    {
        if (m_characterBinding.StatusEffectList.Count == 0)
        {
            m_statusEffectGroup.SetActiveOptimized(false);
            return;
        }

        for (int i = 0; i < 3; i++)
            m_statusEffectElements[i].Parent.SetActiveOptimized(false);

        for (int i = 0; i < m_characterBinding.StatusEffectList.Count; i++)
        {
            StatusEffectBase _statusEffect = m_characterBinding.StatusEffectList[i];
            MultiTextElementWrapper _textWrapper = m_statusEffectElements[i];

            _textWrapper.TextLeft.SetText(_statusEffect.GetName());
            _textWrapper.TextRight.SetText(_statusEffect.Duration.ToStringMinimalAlloc());
            _textWrapper.Parent.SetActiveOptimized(true);
        }

        m_statusEffectGroup.SetActiveOptimized(true);
    }
}
