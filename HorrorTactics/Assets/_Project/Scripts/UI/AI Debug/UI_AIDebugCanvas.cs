using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public class UI_AIDebugCanvas : MonoBehaviour
{
    [Header("Object References")]
    [SerializeField] private FactionInstance m_enemyFaction = null;

    private Canvas m_canvas = null;

    private Dictionary<AICharacter, UI_AIDebugElement> m_activeDebugElements = 
        new Dictionary<AICharacter, UI_AIDebugElement>();

    private void Awake()
    {
        m_canvas = GetComponent<Canvas>();
        m_canvas.enabled = false;
    }

    private void Start()
    {
        for (int i = 0; i < m_enemyFaction.ActiveCharacters.Count; i++)
            onCharacterRegistered(m_enemyFaction.ActiveCharacters[i]);

        m_enemyFaction.OnCharacterRegistered += this.onCharacterRegistered;
        m_enemyFaction.OnCharacterUnregistered += this.onCharacterUnregistered;

        MEC.TimingUtility.SubscribeToUpdate(mecUpdate, this);
    }

    private void OnDestroy()
    {
        if (Application.isPlaying == false)
            return;

        m_enemyFaction.OnCharacterRegistered -= this.onCharacterRegistered;
        m_enemyFaction.OnCharacterUnregistered -= this.onCharacterUnregistered;
    }

    private void mecUpdate()
    {
        if (Input.GetKeyDown(KeyCode.F4))
        {
            if (m_canvas.enabled)
                m_canvas.enabled = false;
            else
                m_canvas.enabled = true;
        }
    }

    private void onCharacterRegistered(CharacterBase character)
    {
        if (character == null)
            return;

        if (character is AICharacter _aiCharacter)
        {
            UI_AIDebugElement _debugElement = AIDebugElementPool.Get();
            _debugElement.BindToCharacter(_aiCharacter);
            m_activeDebugElements.Add(_aiCharacter, _debugElement);
        }
    }

    private void onCharacterUnregistered(CharacterBase character)
    {
        if (character == null)
            return;

        if (character is AICharacter _aiCharacter)
        {
            if (m_activeDebugElements.TryGetValue(_aiCharacter, out var _debugUI))
            {
                _debugUI.ReleaseBinding();
                m_activeDebugElements.Remove(_aiCharacter);
            }
        }
    }
}
