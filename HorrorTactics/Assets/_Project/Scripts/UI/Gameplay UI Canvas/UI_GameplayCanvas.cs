using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class UI_GameplayCanvas : MonoBehaviour
{
    [Header("Object References")]
    [SerializeField] private FactionInstance m_playerFaction = null;
    [SerializeField] private List<UI_CharacterPortrait> m_characterPortraits = null;

    private Canvas m_canvas = null;

    private void Awake()
    {
        m_canvas = GetComponent<Canvas>();
        ScreenStateManager.OnScreenStateChanged += this.onScreenStateChanged;
    }

    private IEnumerator Start()
    {
        while (m_playerFaction.ActiveCharacters.Count == 0)
            yield return null;

        for (int i = 0; i < m_characterPortraits.Count; i++)
        {
            if (i >= m_playerFaction.ActiveCharacters.Count)
                m_characterPortraits[i].gameObject.SetActive(false);
            else
                m_characterPortraits[i].BindToCharacter(m_playerFaction.ActiveCharacters[i] as ControllableCharacter);
        }
    }

    private void OnDestroy()
    {
        if (Application.isPlaying == false)
            return;

        ScreenStateManager.OnScreenStateChanged -= this.onScreenStateChanged;
    }

    private void onScreenStateChanged(ScreenStateType screenState)
    {
        switch (screenState)
        {
            case ScreenStateType.Gameplay:
                m_canvas.enabled = true;
                break;

            default:
                m_canvas.enabled = false;
                break;
        }
    }

    public void Button_Inventory()
    {
        var _activeCharacter = m_playerFaction.ActiveCharacters[0] as ControllableCharacter;

        UI_InventoryScreen.Instance.BindToInventory(_activeCharacter.InventoryComponent);
        UI_InventoryScreen.Instance.OpenScreen();
    }
}
