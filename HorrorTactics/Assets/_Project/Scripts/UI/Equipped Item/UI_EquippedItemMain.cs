using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using ApplicationCore;

public class UI_EquippedItemMain : MonoBehaviour
{
    [Header("Object References")]
    [SerializeField] private Sprite m_noItemEquippedSprite = null;
    [SerializeField] private Image m_equippedItemIcon = null;

    private ControllableCharacter m_characterBinding = null;
    private WeaponItem m_itemBinding = null;

    private List<UI_EquippedItemAmmunitionInstance> m_ammunitionElements = 
        new List<UI_EquippedItemAmmunitionInstance>();

    private void Awake()
    {
        PlayerActionHandler.OnCharacterSelected += this.onCharacterSelected;
    }

    private void OnDestroy()
    {
        if (Application.isPlaying == false)
            return;

        PlayerActionHandler.OnCharacterSelected -= this.onCharacterSelected;

        clearBindings();
    }

    private void clearBindings()
    {
        if (m_characterBinding != null)
        {
            m_characterBinding.OnWeaponEquipped -= this.updateUI;
            m_characterBinding.OnWeaponFired -= this.updateUI;
            m_characterBinding.OnWeaponReload -= this.updateUI;

            m_characterBinding = null;
        }

        m_itemBinding = null;
    }

    private void onCharacterSelected(ControllableCharacter character)
    {
        clearBindings();
        m_characterBinding = character;
        updateUI();

        if (m_characterBinding)
        {
            m_characterBinding.OnWeaponEquipped += this.updateUI;
            m_characterBinding.OnWeaponFired += this.updateUI;
            m_characterBinding.OnWeaponReload += this.updateUI;
        }
    }

    private void updateUI()
    {
        //if (m_characterBinding)
        //    m_itemBinding = m_characterBinding.EquippedWeapon;
        //else
        //    m_itemBinding = null;

        Sprite _targetSprite = m_noItemEquippedSprite;

        if (m_characterBinding && m_itemBinding)
            _targetSprite = m_itemBinding.UIIcon;

        if (m_equippedItemIcon.overrideSprite != _targetSprite)
            m_equippedItemIcon.overrideSprite = _targetSprite;

        if (m_itemBinding)
        {
            int _currentAmmo = m_itemBinding.GetRemainingAmmoCount(m_characterBinding);

            if (m_ammunitionElements.Count != m_itemBinding.ClipCapacity)
            {
                UI_EquippedItemAmmunitionPool.ResetPool();
                m_ammunitionElements.Clear();

                while (m_ammunitionElements.Count < m_itemBinding.ClipCapacity)
                    m_ammunitionElements.Add(UI_EquippedItemAmmunitionPool.Get());
            }

            for (int i = 0; i < m_ammunitionElements.Count; i++)
            {
                m_ammunitionElements[i].Initialize(m_itemBinding.SingleAmmunitionIcon);
                m_ammunitionElements[i].SetIsAvailable(i < _currentAmmo);
            }
        }
        else
        {
            UI_EquippedItemAmmunitionPool.ResetPool();
            m_ammunitionElements.Clear();
        }
    }
}
