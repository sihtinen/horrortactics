using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class UI_EquippedItemAmmunitionInstance : MonoBehaviour
{
    [Header("Object References")]
    [SerializeField] private Image m_iconImage = null;

    public void Initialize(Sprite sprite)
    {
        if (m_iconImage.overrideSprite != sprite)
            m_iconImage.overrideSprite = sprite;

        if (gameObject.activeSelf == false)
            gameObject.SetActive(true);
    }

    public void SetIsAvailable(bool isAvailable)
    {
        m_iconImage.color = isAvailable ?
            Color.white :
            new Color(0.1f, 0.1f, 0.1f);
    }
}
