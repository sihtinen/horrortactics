using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public class UI_CanvasSortingHelper : MonoBehaviour
{
    private Canvas m_canvas = null;

    private int m_defaultSortingOrder = 0;

    private void Awake()
    {
        m_canvas = GetComponent<Canvas>();
        m_defaultSortingOrder = m_canvas.sortingOrder;
    }

    public void UpdateSorting(int offset)
    {
        m_canvas.sortingOrder = m_defaultSortingOrder + offset;
    }
}
