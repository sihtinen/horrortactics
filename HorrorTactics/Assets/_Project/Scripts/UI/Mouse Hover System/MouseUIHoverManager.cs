using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public class MouseUIHoverManager : SingletonBehaviour<MouseUIHoverManager>
{
    private HashSet<MouseUIHoverInstance> m_registeredHoverComponents = null;

    protected override void Awake()
    {
        base.Awake();

        m_registeredHoverComponents = new HashSet<MouseUIHoverInstance>();
    }

    public static void RegisterHover(MouseUIHoverInstance instance)
    {
        Instance.m_registeredHoverComponents.Add(instance);
    }

    public static void UnregisterHover(MouseUIHoverInstance instance)
    {
        if (Instance == null)
            return;

        if (Instance.m_registeredHoverComponents.Contains(instance))
            Instance.m_registeredHoverComponents.Remove(instance);
    }

    public static bool IsMouseOverUI
    {
        get
        {
            if (Instance == null)
                return false;

            return Instance.m_registeredHoverComponents.Count > 0;
        }
    }
}
