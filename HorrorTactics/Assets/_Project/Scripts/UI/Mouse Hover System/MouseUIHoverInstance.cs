using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;


[RequireComponent(typeof(UI_MouseEventComponent))]
public class MouseUIHoverInstance : MonoBehaviour
{
    private UI_MouseEventComponent m_mouseEventComponent = null;

    private void Awake()
    {
        m_mouseEventComponent = GetComponent<UI_MouseEventComponent>();
        m_mouseEventComponent.OnPointerEnter += this.onPointerEnter;
        m_mouseEventComponent.OnPointerExit += this.onPointerExit;
    }

    private void OnDestroy()
    {
        if (Application.isPlaying == false)
            return;

        MouseUIHoverManager.UnregisterHover(this);
    }

    private void onPointerEnter()
    {
        MouseUIHoverManager.RegisterHover(this);
    }

    private void onPointerExit()
    {
        MouseUIHoverManager.UnregisterHover(this);
    }
}
