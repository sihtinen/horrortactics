using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class UI_ActionBar : MonoBehaviour
{
    [Header("Object References")]
    [SerializeField] private UI_ActionBarActionElement m_actionElementExample = null;

    [Header("Debug")]
    [SerializeField] private bool m_logDebugMessages = false;

    private RectTransform m_rectTransform = null;
    private ControllableCharacter m_currentCharacter = null;

    private UI_ActionBarActionElement m_selectedActionElement = null;
    private List<UI_ActionBarActionElement> m_actionButtonElements = new List<UI_ActionBarActionElement>();

    private bool m_isLayoutDirty = false;

    private void Awake()
    {
        m_rectTransform = GetComponent<RectTransform>();

        m_actionButtonElements.Add(m_actionElementExample);

        for (int i = 0; i < 15; i++)
        {
            GameObject _newObject = Instantiate(m_actionElementExample.gameObject, m_actionElementExample.transform.parent);
            m_actionButtonElements.Add(_newObject.GetComponent<UI_ActionBarActionElement>());
        }

        for (int i = 0; i < m_actionButtonElements.Count; i++)
            m_actionButtonElements[i].Initialize(this);

        PlayerActionHandler.OnCharacterSelected += this.onCharacterSelected;
    }

    private void Start()
    {
        MEC.TimingUtility.SubscribeToUpdate(mecLateUpdate, this, MEC.Segment.LateUpdate);
    }

    private void OnDestroy()
    {
        if (Application.isPlaying == false)
            return;

        PlayerActionHandler.OnCharacterSelected -= this.onCharacterSelected;

        unbindPlayerEvents();
    }

    private void bindPlayerEvents()
    {
        m_currentCharacter.OnWeaponEquipped += this.generateActionElements;
        m_currentCharacter.OnSelectedActionChanged += this.onActionSelected;
        m_currentCharacter.OnDynamicActionsUpdated += this.generateActionElements;
        m_currentCharacter.InventoryComponent.InventoryData.OnInventoryUpdated += this.generateActionElements;
    }

    private void unbindPlayerEvents()
    {
        if (m_currentCharacter != null)
        {
            m_currentCharacter.OnWeaponEquipped -= this.generateActionElements;
            m_currentCharacter.OnSelectedActionChanged -= this.onActionSelected;
            m_currentCharacter.OnDynamicActionsUpdated -= this.generateActionElements;
            m_currentCharacter.InventoryComponent.InventoryData.OnInventoryUpdated -= this.generateActionElements;
        }
    }

    private void mecLateUpdate()
    {
        if (m_isLayoutDirty)
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(m_rectTransform);
            m_isLayoutDirty = false;
        }
    }

    public void SelectAction(UI_ActionBarActionElement actionElement)
    {
        m_currentCharacter.SetSelectedAction(actionElement.PlayerActionBinding, actionElement.PlayerActionParameters);
    }

    public void MarkLayoutDirty()
    {
        m_isLayoutDirty = true;
    }

    private void onCharacterSelected(ControllableCharacter character)
    {
        unbindPlayerEvents();

        m_currentCharacter = character;

        generateActionElements();
        bindPlayerEvents();
    }

    private void generateActionElements()
    {
        if (m_currentCharacter == null)
        {
            if (m_logDebugMessages)
                Debug.LogWarning("UI_ActionBar.generateActionElements(): current character reference is null!");

            return;
        }

        for (int i = 0; i < m_actionButtonElements.Count; i++)
        {
            m_actionButtonElements[i].ReleaseBinding();
        }

        int _actionButtonIndex = 0;
        List<PlayerActionSelectionOption> _allActions = m_currentCharacter.GetAllAvailableActions();

        for (int i = 0; i < _allActions.Count; i++)
        {
            PlayerActionBase _action = _allActions[i].Action;
            object _actionParameters = _allActions[i].ActionParameters;

            bool _canBeSelected = _action.CanPerformAction(m_currentCharacter, _actionParameters);

            m_actionButtonElements[_actionButtonIndex].BindToAction(_action, _actionParameters, _canBeSelected);
            _actionButtonIndex++;
        }
    }

    private void onActionSelected(PlayerActionBase action)
    {
        if (m_selectedActionElement)
            m_selectedActionElement.OnDeselected();

        if (action == null)
            return;

        for (int i = 0; i < m_actionButtonElements.Count; i++)
        {
            if (m_actionButtonElements[i].PlayerActionBinding == action)
            {
                m_selectedActionElement = m_actionButtonElements[i];
                break;
            }
        }

        m_selectedActionElement?.OnSelected();
    }
}
