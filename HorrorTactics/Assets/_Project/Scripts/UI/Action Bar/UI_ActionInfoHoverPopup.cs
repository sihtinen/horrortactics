using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using TMPro;

using ApplicationCore;

public class UI_ActionInfoHoverPopup : SingletonBehaviour<UI_ActionInfoHoverPopup>
{
    [Header("Settings")]
    [SerializeField] private float m_verticalOffset = 0f;

    [Header("Object References")]
    [SerializeField] private TMP_Text m_nameText = null;
    [SerializeField] private TMP_Text m_descriptionText = null;
    [SerializeField] private RectTransform m_apCostGroupParent = null;
    [SerializeField] private RectTransform m_ammoCostGroupParent = null;

    private RectTransform m_rectTransform = null;

    private List<UI_ActionBarAPCostElement> m_apCostElements = new List<UI_ActionBarAPCostElement>();
    private List<UI_ActionBarAmmoCostElement> m_ammoCostElements = new List<UI_ActionBarAmmoCostElement>();
    private List<UI_ActionBarOrElement> m_orElements = new List<UI_ActionBarOrElement>();

    protected override void Awake()
    {
        base.Awake();

        m_rectTransform = GetComponent<RectTransform>();

        SetEnabled(false);
    }

    public void SetEnabled(bool isEnabled)
    {
        gameObject.SetActiveOptimized(isEnabled);
    }

    public void SetPosition(Vector2 position)
    {
        m_rectTransform.position = position;

        Vector2 _anchoredPos = m_rectTransform.anchoredPosition;
        _anchoredPos.y += m_verticalOffset;
        m_rectTransform.anchoredPosition = _anchoredPos;
    }

    public void BindToAction(PlayerActionBase action)
    {
        for (int i = m_apCostElements.Count; i-- > 0;)
            UI_ActionBarAPCostElementPool.ReturnObject(m_apCostElements[i]);

        for (int i = m_ammoCostElements.Count; i --> 0;)
            UI_ActionBarAmmoCostElementPool.ReturnObject(m_ammoCostElements[i]);

        for (int i = m_orElements.Count; i-- > 0;)
            UI_ActionBarOrElementPool.ReturnObject(m_orElements[i]);

        m_apCostElements.Clear();
        m_ammoCostElements.Clear();
        m_orElements.Clear();

        m_nameText.SetText(action.DisplayName);
        m_descriptionText.SetText(action.Description);

        for (int i = 0; i < action.APCosts.Length; i++)
        {
            if (i > 0)
            {
                UI_ActionBarOrElement _orElement = UI_ActionBarOrElementPool.Get();
                m_orElements.Add(_orElement);

                _orElement.transform.SetParent(m_apCostGroupParent);
                _orElement.gameObject.SetActiveOptimized(true);
            }

            for (int ii = 0; ii < action.APCosts[i]; ii++)
            {
                UI_ActionBarAPCostElement _apElement = UI_ActionBarAPCostElementPool.Get();
                m_apCostElements.Add(_apElement);

                _apElement.transform.SetParent(m_apCostGroupParent);
                _apElement.gameObject.SetActiveOptimized(true);
            }
        }

        if (action is PlayerAction_RangedAbility _rangedAction)
        {
            for (int i = 0; i < _rangedAction.AmmoCost; i++)
            {
                UI_ActionBarAmmoCostElement _ammoElement = UI_ActionBarAmmoCostElementPool.Get();
                m_ammoCostElements.Add(_ammoElement);

                _ammoElement.transform.SetParent(m_ammoCostGroupParent);
                _ammoElement.gameObject.SetActiveOptimized(true);
            }
        }
    }
}
