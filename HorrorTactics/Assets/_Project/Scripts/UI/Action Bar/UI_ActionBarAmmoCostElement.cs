using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class UI_ActionBarAmmoCostElement : MonoBehaviour
{
    private void OnEnable()
    {
        if (Application.isPlaying == false) return;

        transform.localScale = Vector3.one;
    }
}
