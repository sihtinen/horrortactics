using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using MEC;

using ApplicationCore;

public class UI_ActionBarActionElement : MonoBehaviour
{
    [System.NonSerialized] public PlayerActionBase PlayerActionBinding = null;
    [System.NonSerialized] public object PlayerActionParameters = null;

    [Header("Object References")]
    [SerializeField] private Image m_iconImage = null;
    [SerializeField] private Image m_selectionHighlight = null;
    [SerializeField] private RectTransform m_iconRect = null;
    [SerializeField] private Button m_button = null;
    [SerializeField] private UI_MouseEventComponent m_eventComponent = null;

    private const float SCALE_SELECTED = 1.0f;
    private const float SCALE_NOT_SELECTED = 0.7f;

    private const int WIDTH_SELECTED = 130;
    private const int WIDTH_NOT_SELECTED = 100;

    private const int HORIZONTAL_OFFSET_SELECTED = 13;
    private const int HORIZONTAL_OFFSET_NOT_SELECTED = 0;

    private const float ANIM_TIME = 0.17f;

    private bool m_isSelected = false;

    private UI_ActionBar m_actionBar = null;
    private RectTransform m_rectTransform = null;

    private CoroutineHandle m_currentScaleAnimationRoutine;

    private float m_currentAnimationPosition = 0.0f;

    public void Initialize(UI_ActionBar actionBar)
    {
        m_actionBar = actionBar;

        m_rectTransform = GetComponent<RectTransform>();

        m_selectionHighlight.enabled = false;

        applyAnimationPosition(0.0f);

        m_eventComponent.OnPointerEnter += this.onHoverBegin;
        m_eventComponent.OnPointerExit += this.onHoverEnd;

        gameObject.SetActive(false);
    }

    public void BindToAction(
        PlayerActionBase action,
        object actionParameters,
        bool canBeSelected)
    {
        PlayerActionBinding = action;
        PlayerActionParameters = actionParameters;

        m_iconImage.sprite = action.Icon;
        m_button.interactable = canBeSelected;

        if (gameObject.activeSelf == false)
            gameObject.SetActive(true);
    }

    public void ReleaseBinding()
    {
        if (this == null) return;

        PlayerActionBinding = null;

        gameObject.SetActiveOptimized(false);

        applyAnimationPosition(0.0f);
    }

    private IEnumerator<float> coroutine_updateScale(float targetAnimationPosition)
    {
        float _startPosition = m_currentAnimationPosition;
        float _timer = 0.0f;

        while (_timer < ANIM_TIME)
        {
            _timer += Time.unscaledDeltaTime;
            float _normalized = _timer / ANIM_TIME;

            float _animationPosition = Mathf.Lerp(_startPosition, targetAnimationPosition, _normalized);
            applyAnimationPosition(_animationPosition);

            yield return Timing.WaitForOneFrame;
        }

        applyAnimationPosition(targetAnimationPosition);
    }

    private void applyAnimationPosition(float targetAnimationPosition)
    {
        m_currentAnimationPosition = targetAnimationPosition;

        float _currentScale = Mathf.Lerp(SCALE_NOT_SELECTED, SCALE_SELECTED, m_currentAnimationPosition);
        m_rectTransform.localScale = new Vector3(_currentScale, _currentScale, 1.0f);

        float _currentWidth = Mathf.Lerp(WIDTH_NOT_SELECTED, WIDTH_SELECTED, m_currentAnimationPosition);
        m_rectTransform.sizeDelta = new Vector2(_currentWidth, m_rectTransform.sizeDelta.y);

        float _horizontalOffset = Mathf.Lerp(HORIZONTAL_OFFSET_NOT_SELECTED, HORIZONTAL_OFFSET_SELECTED, m_currentAnimationPosition);
        m_iconRect.SetLeft(_horizontalOffset);
        m_iconRect.SetRight(_horizontalOffset);

        m_actionBar.MarkLayoutDirty();
    }

    public void OnSelected()
    {
        if (m_isSelected == false)
        {
            m_isSelected = true;

            m_selectionHighlight.enabled = true;

            if (m_currentScaleAnimationRoutine.IsRunning)
                Timing.KillCoroutines(m_currentScaleAnimationRoutine);

            m_currentScaleAnimationRoutine = Timing.RunCoroutine(coroutine_updateScale(1.0f));
        }
    }

    public void OnDeselected()
    {
        if (m_isSelected)
        {
            m_isSelected = false;

            m_selectionHighlight.enabled = false;

            if (m_currentScaleAnimationRoutine != null)
                Timing.KillCoroutines(m_currentScaleAnimationRoutine);

            if (gameObject.activeInHierarchy)
                m_currentScaleAnimationRoutine = Timing.RunCoroutine(coroutine_updateScale(0.0f));
        }
    }

    private void onHoverBegin()
    {
        UI_ActionInfoHoverPopup.Instance.SetPosition(m_rectTransform.position);
        UI_ActionInfoHoverPopup.Instance.BindToAction(PlayerActionBinding);
        UI_ActionInfoHoverPopup.Instance.SetEnabled(true);
    }

    private void onHoverEnd()
    {
        UI_ActionInfoHoverPopup.Instance.SetEnabled(false);
    }

    public void Button_OnClick()
    {
        m_actionBar.SelectAction(this);
    }
}
