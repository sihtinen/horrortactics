using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using TMPro;

using ApplicationCore;

public class UI_InteractableTooltipElement : MonoBehaviour
{
    [Header("Object References")]
    [SerializeField] private List<Image> m_iconImages = new List<Image>();
    [SerializeField] private TMP_Text m_interactableNameText = null;

    private Interactable m_interactableBinding = null;
    private RectTransform m_rectTransform = null;

    private float m_baseAlpha = 0.0f;
    private float m_expandedAlpha = 0.0f;

    private const float ALPHA_SPEED = 4.0f;

    private void Awake()
    {
        m_rectTransform = GetComponent<RectTransform>();
    }

    private void Start()
    {
        MEC.TimingUtility.SubscribeToUpdate(mecUpdate, this);
    }

    public void BindToElement(Interactable interactable)
    {
        m_interactableBinding = interactable;
        m_interactableNameText.SetText(m_interactableBinding.name);

        gameObject.SetActiveOptimized(true);
    }

    private void mecUpdate()
    {
        if (m_interactableBinding == null)
            return;

        Vector3 _interactableWorldPos = m_interactableBinding.transform.position + m_interactableBinding.TooltipPositionOffset;
        Vector3 _viewPortPos = MainCameraComponent.Camera.WorldToViewportPoint(_interactableWorldPos, Camera.MonoOrStereoscopicEye.Mono);

        m_rectTransform.anchorMin = _viewPortPos;
        m_rectTransform.anchorMax = _viewPortPos;
        m_rectTransform.anchoredPosition = Vector2.zero;

        updateBase(Time.deltaTime);
        updateExpanded(Time.deltaTime, _viewPortPos);
    }

    private void updateBase(float deltaTime)
    {
        if (m_interactableBinding.IsVisibleToPlayer == false)
        {
            if (m_baseAlpha > 0)
                m_baseAlpha -= deltaTime * ALPHA_SPEED;
        }
        else
        {
            if (m_baseAlpha < 1)
                m_baseAlpha += deltaTime * ALPHA_SPEED;
        }

        bool _areVisible = m_baseAlpha > 0;

        for (int i = 0; i < m_iconImages.Count; i++)
        {
            Image _image = m_iconImages[i];

            if (_areVisible)
                _image.SetAlpha(m_baseAlpha);

            if (_image.enabled != _areVisible)
                _image.enabled = _areVisible;
        }
    }

    private void updateExpanded(float deltaTime, Vector3 interactableViewPortPos)
    {
        Vector3 _mouseViewPortPos = MainCameraComponent.Camera.ScreenToViewportPoint(Input.mousePosition);
        Vector2 _toMouse = _mouseViewPortPos - interactableViewPortPos;

        float _toMouseDistance = _toMouse.magnitude;

        if (m_interactableBinding.IsVisibleToPlayer == false ||
            _toMouseDistance > 0.1f)
        {
            if (m_expandedAlpha > 0)
                m_expandedAlpha -= deltaTime * ALPHA_SPEED;
        }
        else
        {
            if (m_expandedAlpha < 1)
                m_expandedAlpha += deltaTime * ALPHA_SPEED;
        }

        bool _extrasEnabled = m_expandedAlpha > 0f;

        m_interactableNameText.alpha = m_expandedAlpha;

        if (m_interactableNameText.enabled != _extrasEnabled)
            m_interactableNameText.enabled = _extrasEnabled;
    }
}