using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;
using System;

public class UI_InteractableTooltipCanvas : MonoBehaviour
{
    private Canvas m_canvas = null;

    private void Awake()
    {
        m_canvas = GetComponent<Canvas>();

        InteractableCollection.OnInteractableRegistered += this.onInteractableRegistered;
        ScreenStateManager.OnScreenStateChanged += this.onScreenStateUpdated;
    }

    private void OnDestroy()
    {
        if (Application.isPlaying == false)
            return;

        InteractableCollection.OnInteractableRegistered -= this.onInteractableRegistered;
        ScreenStateManager.OnScreenStateChanged -= this.onScreenStateUpdated;
    }


    private void onScreenStateUpdated(ScreenStateType newScreenState)
    {
        m_canvas.enabled = newScreenState == ScreenStateType.Gameplay;
    }

    private void onInteractableRegistered(Interactable interactable)
    {
        var _toolTip = UI_InteractableTooltipElementPool.Get();

        _toolTip.transform.SetParent(transform);
        _toolTip.BindToElement(interactable);
    }
}
