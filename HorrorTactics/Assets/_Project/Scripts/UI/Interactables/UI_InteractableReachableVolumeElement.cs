using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using ApplicationCore;

public class UI_InteractableReachableVolumeElement : MonoBehaviour
{
    private RectTransform m_rectTransform = null;
    private Image m_image = null;

    private void Awake()
    {
        m_rectTransform = GetComponent<RectTransform>();
        m_image = GetComponent<Image>();
    }

    public void SetAlpha(float alpha)
    {
        m_image.SetAlpha(alpha);
    }

    public void UpdatePosition(Vector3 worldPosition)
    {
        Vector2 _viewportPos = MainCameraComponent.Camera.WorldToViewportPoint(worldPosition);
        m_rectTransform.anchorMin = _viewportPos;
        m_rectTransform.anchorMax = _viewportPos;
        m_rectTransform.anchoredPosition = Vector2.zero;
    }
}
