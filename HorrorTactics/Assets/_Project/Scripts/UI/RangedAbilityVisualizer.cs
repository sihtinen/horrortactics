using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public class RangedAbilityVisualizer : SingletonBehaviour<RangedAbilityVisualizer>
{
    [Header("Object References")]
    [SerializeField] private LineRenderer m_lineRenderer = null;

    private static Vector3[] m_positions = new Vector3[2];

    protected override void Awake()
    {
        base.Awake();

        m_lineRenderer.enabled = false;
    }

    public void SetEnabled(bool isEnabled)
    {
        m_lineRenderer.enabled = isEnabled;

        if (isEnabled)
            WorldOverlayCamera.RegisterUser(this);
        else
            WorldOverlayCamera.UnregisterUser(this);
    }

    public void SetPositions(Vector3 start, Vector3 end)
    {
        m_positions[0] = start;
        m_positions[1] = end;

        m_lineRenderer.SetPositions(m_positions);
    }
}
