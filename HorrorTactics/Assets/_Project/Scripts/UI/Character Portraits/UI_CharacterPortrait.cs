using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using ApplicationCore;

public class UI_CharacterPortrait : MonoBehaviour
{
    [Header("Object References")]
    [SerializeField] private Image m_selectionHighlight = null;
    [SerializeField] private List<Image> m_healthPointElements = null;

    private ControllableCharacter m_characterBinding = null;

    private void Start()
    {
        PlayerActionHandler.OnCharacterSelected += this.onCharacterSelected;
        PlayerActionHandler.OnCharacterDeselected += this.onCharacterDeselected;
        ScreenStateManager.OnScreenStateChanged += this.onScreenStateChanged;
    }

    public void BindToCharacter(ControllableCharacter character)
    {
        m_selectionHighlight.enabled = false;

        m_characterBinding = character;
        m_characterBinding.Health.OnHealthUpdated += this.onHealthUpdated;

        onHealthUpdated();
    }

    private void OnDestroy()
    {
        if (Application.isPlaying == false)
            return;

        PlayerActionHandler.OnCharacterSelected -= this.onCharacterSelected;
        PlayerActionHandler.OnCharacterDeselected -= this.onCharacterDeselected;
        ScreenStateManager.OnScreenStateChanged -= this.onScreenStateChanged;

        if (m_characterBinding)
        {
            m_characterBinding.Health.OnHealthUpdated -= this.onHealthUpdated;
            m_characterBinding = null;
        }
    }

    private void onScreenStateChanged(ScreenStateType stateType)
    {
        gameObject.SetActiveOptimized(stateType == ScreenStateType.Gameplay);
    }

    private void onCharacterSelected(CharacterBase character)
    {
        if (character != m_characterBinding) return;

        m_selectionHighlight.enabled = true;
    }

    private void onCharacterDeselected(CharacterBase character)
    {
        if (character != m_characterBinding)
            return;

        m_selectionHighlight.enabled = false;
    }

    private void onHealthUpdated()
    {
        int _maxHealth = m_characterBinding.Health.Parameters.MaxHealth;

        for (int i = 0; i < 10; i++)
        {
            if (i >= _maxHealth)
            {
                if (m_healthPointElements[i].gameObject.activeSelf)
                    m_healthPointElements[i].gameObject.SetActive(false);
            }
            else
            {
                int _healthPointIndex = _maxHealth - i - 1;

                if (i + 1 <= m_characterBinding.Health.CurrentHealth)
                    m_healthPointElements[_healthPointIndex].color = Color.green;
                else
                    m_healthPointElements[_healthPointIndex].color = Color.red;

                if (m_healthPointElements[_healthPointIndex].gameObject.activeSelf == false)
                    m_healthPointElements[_healthPointIndex].gameObject.SetActive(true);
            }
        }
    }

    public void Button_OnClicked()
    {
        PlayerActionHandler.Instance.SelectCharacter(m_characterBinding);
    }
}
