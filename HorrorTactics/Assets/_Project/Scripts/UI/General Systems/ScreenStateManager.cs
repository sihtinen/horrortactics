using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public enum ScreenStateType
{
    Gameplay,
    Inventory,
    Dialogue
}

public class ScreenStateManager : SingletonBehaviour<ScreenStateManager>
{
    public ScreenStateType CurrentScreenState
    {
        get { return m_currentScreenState; }
    }

    public static Action<ScreenStateType> OnScreenStateChanged = null;

    private ScreenStateType m_currentScreenState = ScreenStateType.Gameplay;

    public void SetScreenState(ScreenStateType newState)
    {
        if (newState == m_currentScreenState) return;

        m_currentScreenState = newState;

        OnScreenStateChanged?.Invoke(m_currentScreenState);
    }
}
