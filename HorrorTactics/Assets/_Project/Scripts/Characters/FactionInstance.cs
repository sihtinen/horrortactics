using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using MEC;

[Serializable]
[CreateAssetMenu(menuName = "Factions/ New Faction Instance", fileName = "New Faction Instance")]
public class FactionInstance : ScriptableObject
{
    public List<CharacterBase> ActiveCharacters { get; private set; } = new List<CharacterBase>();

    public event Action<CharacterBase> OnCharacterRegistered = null;
    public event Action<CharacterBase> OnCharacterUnregistered = null;

    public void ClearRuntimeData()
    {
        ActiveCharacters.Clear();
    }

    public void RegisterCharacter(CharacterBase character)
    {
        if (ActiveCharacters.Contains(character) == false)
        {
            ActiveCharacters.Add(character);
            ActiveCharacters.RemoveAll(item => item == null);

            OnCharacterRegistered?.Invoke(character);
        }
    }

    public void UnregisterCharacter(CharacterBase character)
    {
        if (ActiveCharacters.Contains(character))
        {
            ActiveCharacters.Remove(character);
            ActiveCharacters.RemoveAll(item => item == null);

            OnCharacterUnregistered?.Invoke(character);
        }
    }
}