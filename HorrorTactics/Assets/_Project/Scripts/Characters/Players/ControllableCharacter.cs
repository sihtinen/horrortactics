using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Sirenix.OdinInspector;

public class ControllableCharacter : CharacterBase
{
    public PlayerActionBase SelectedAction { get; private set; } = null;
    public CharacterInventoryComponent InventoryComponent { get; private set; } = null;
    public List<Tuple<PlayerActionBase, object>> DynamicActions { get; private set; } = new List<Tuple<PlayerActionBase, object>>();

    [Header("Controllable Character Object References")]
    [SerializeField, InlineEditor] private PlayerAction_Move m_moveAction = null;
    [SerializeField, InlineEditor] private PlayerAction_MeleeAbility m_kickAction = null;

    public Action OnWeaponEquipped = null;
    public Action OnWeaponReload = null;
    public Action OnWeaponFired = null;
    public Action OnDynamicActionsUpdated = null;
    public Action<PlayerActionBase> OnSelectedActionChanged = null;

    private List<PlayerActionSelectionOption> m_availableActions = new List<PlayerActionSelectionOption>();

    protected override void Awake()
    {
        base.Awake();

        InventoryComponent = GetComponent<CharacterInventoryComponent>();
        Health.OnDeath += this.onDeath;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        if (Application.isPlaying == false)
            return;

        SetSelectedAction(null);
    }

    public void SetSelectedAction(PlayerActionBase newAction, object actionParameters = null)
    {
        if (SelectedAction == newAction) return;

        if (SelectedAction != null)
            SelectedAction.OnEndProcessAction();

        SelectedAction = newAction;

        if (SelectedAction != null)
            SelectedAction.OnBeginValidateAction(this, actionParameters);

        OnSelectedActionChanged?.Invoke(SelectedAction);
    }

    public void RegisterDynamicAction(Tuple<PlayerActionBase, object> actionWrapper)
    {
        if (DynamicActions.Contains(actionWrapper) == false)
        {
            DynamicActions.Add(actionWrapper);
            OnDynamicActionsUpdated?.Invoke();
        }
    }

    public void UnregisterDynamicAction(Tuple<PlayerActionBase, object> actionWrapper)
    {
        if (DynamicActions.Contains(actionWrapper))
        {
            DynamicActions.Remove(actionWrapper);
            OnDynamicActionsUpdated?.Invoke();
        }
    }

    public List<PlayerActionSelectionOption> GetAllAvailableActions()
    {
        m_availableActions.Clear();

        m_availableActions.Add(new PlayerActionSelectionOption()
        { 
            Action = m_moveAction,
            ActionParameters = null
        });

        m_availableActions.Add(new PlayerActionSelectionOption()
        {
            Action = m_kickAction,
            ActionParameters = null
        });

        if (DynamicActions.Count > 0)
        {
            for (int i = 0; i < DynamicActions.Count; i++)
            {
                Tuple<PlayerActionBase, object> _currentActionBinding = DynamicActions[i];

                PlayerActionBase _action = _currentActionBinding.Item1;
                object _actionParameters = _currentActionBinding.Item2;

                m_availableActions.Add(new PlayerActionSelectionOption() 
                {
                    Action = _action,
                    ActionParameters = _actionParameters
                });
            }
        }

        List<ItemBase> _allItems = InventoryComponent.InventoryData.GetAllItems();
        if (_allItems != null && _allItems.Count > 0)
        {
            for (int i = 0; i < _allItems.Count; i++)
            {
                ItemBase _currentItem = _allItems[i];

                List<PlayerActionSelectionOption> _itemActions = _currentItem.GetAvailableActions(this);

                if (_itemActions.Count == 0)
                    continue;

                for (int ii = 0; ii < _itemActions.Count; ii++)
                {
                    if (m_availableActions.Contains(_itemActions[ii]) == false)
                        m_availableActions.Add(_itemActions[ii]);
                }
            }
        }

        return m_availableActions;
    }

    public void ValidateSelectedAction()
    {
        SelectedAction?.ProcessAction();
    }

    private void onDeath()
    {

    }
}
