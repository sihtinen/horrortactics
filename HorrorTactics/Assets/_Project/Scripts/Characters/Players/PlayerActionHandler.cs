using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

using MEC;

public class PlayerActionHandler : SingletonBehaviour<PlayerActionHandler>
{
    public static event Action<ControllableCharacter> OnCharacterSelected = null;
    public static event Action<ControllableCharacter> OnCharacterDeselected = null;

    public ControllableCharacter SelectedCharacter { get; private set; } = null;

    [Header("Object References")]
    [SerializeField] private FactionInstance m_playerFaction = null;

    private void Start()
    {
        MouseRaycaster.Instance.OnClicked_ControllableCharacter += this.SelectCharacter;

        SelectCharacter(m_playerFaction.ActiveCharacters[0] as ControllableCharacter);
        TimingUtility.SubscribeToUpdate(this.mecUpdate, this);
    }

    private void OnDestroy()
    {
        if (Application.isPlaying == false)
            return;

        if (MouseRaycaster.Instance)
            MouseRaycaster.Instance.OnClicked_ControllableCharacter -= SelectCharacter;
    }

    public void SelectCharacter(ControllableCharacter character)
    {
        if (character == null)
        {
            Debug.LogWarning($"PlayerActionHandler.SelectCharacter: tried to select null character");
            return;
        }

        if (m_playerFaction.ActiveCharacters.Contains(character) == false)
        {
            Debug.LogWarning($"PlayerActionHandler.SelectCharacter: tried to select non-player character {character.gameObject.name}");
            return;
        }

        if (SelectedCharacter != null && character == SelectedCharacter)
            return;

        if (SelectedCharacter != null)
        {
            SelectedCharacter.SetSelectedAction(null);
            OnCharacterDeselected?.Invoke(SelectedCharacter);
        }

        SelectedCharacter = character;   
        OnCharacterSelected?.Invoke(SelectedCharacter);
    }

    public IEnumerator<float> SpawnControllableCharacters(SceneTransitionComponent transitionPoint)
    {
        while (m_playerFaction.ActiveCharacters.Count == 0)
            yield return Timing.WaitForOneFrame;

        Vector3Int _targetPos;

        if (transitionPoint)
            _targetPos = transitionPoint.transform.position.ToVector3Int();
        else
            _targetPos = m_playerFaction.ActiveCharacters[0].transform.position.ToVector3Int();

        m_playerFaction.ActiveCharacters[0].SetPosition(_targetPos);

        FreeGameplayCamera.Instance.SetTargetPosition(_targetPos);
    }

    private void mecUpdate()
    {
        if (ScreenStateManager.Instance.CurrentScreenState != ScreenStateType.Gameplay) return;
        if (SelectedCharacter == null) return;

        SelectedCharacter.ValidateSelectedAction();

        if (Input.GetKeyDown(KeyCode.Escape))
            SelectedCharacter.SetSelectedAction(null);
    }
}
