using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Characters/New Movement Parametes", fileName = "New Movement Parameters")]
public class MovementParameters : ScriptableObject
{
    [Header("Movement Parameters")]
    public int MovementDistance_Short = 3;
    public int MovementDistance_Long = 8;
    public int MovementAPCost_Short = 1;
    public int MovementAPCost_Long = 2;

    public int GetMaxMovementDistance(int actionPoints)
    {
        if (actionPoints >= MovementAPCost_Long)
            return MovementDistance_Long;

        return MovementDistance_Short;
    }

    public int GetActionPointsCost(int pathLength)
    {
        if (pathLength <= MovementDistance_Short)
            return MovementAPCost_Short;

        return MovementAPCost_Long;
    }
}
