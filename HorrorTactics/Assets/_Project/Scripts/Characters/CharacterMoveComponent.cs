using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

using MEC;


public class CharacterMoveComponent : MonoBehaviour
{
    public Vector3Int IntPosition { get; private set; } = Vector3Int.zero;
    public bool IsMoving { get; private set; } = false;

    public event Action OnMovementStart = null;
    public event Action OnMovementEnd = null;

    private NavMeshAgent m_navMeshAgent = null;

    private void Awake()
    {
        m_navMeshAgent = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        TimingUtility.SubscribeToUpdate(this.onUpdate, this);
    }

    private void onUpdate()
    {
        bool _isMovingNow = m_navMeshAgent.velocity.sqrMagnitude > 0.2f;

        if (m_navMeshAgent.pathPending == false &&
            m_navMeshAgent.remainingDistance <= m_navMeshAgent.stoppingDistance)
        {
            if (m_navMeshAgent.hasPath == false || m_navMeshAgent.velocity.sqrMagnitude == 0f)
                _isMovingNow = false;
        }

        if (_isMovingNow == IsMoving)
            return;

        IsMoving = _isMovingNow;

        if (IsMoving)
            OnMovementStart?.Invoke();
        else
            OnMovementEnd?.Invoke();
    }

    public void SetMoveDestination(Vector3 position)
    {
        m_navMeshAgent.SetDestination(position);
    }
}
