using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

using Sirenix.OdinInspector;

public class HealthComponent : MonoBehaviour
{
    [Header("Runtime Parameters")]
    [ReadOnly] public int CurrentHealth = 0;

    [Header("Object References")]
    [InlineEditor] public HealthParameters Parameters = null;

    public Action OnHealthUpdated;
    public Action OnDeath;

    private void Awake()
    {
        CurrentHealth = Parameters.MaxHealth;
    }

    public void DealHealthDamage(int damage)
    {
        CurrentHealth -= damage;

        UI_DamagePopupSystem.PlayAtPosition(damage, transform);

        OnHealthUpdated?.Invoke();

        if (CurrentHealth <= 0)
            OnDeath?.Invoke();
    }
}
