using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Characters/New Health Parametes", fileName = "New Health Parameters")]
public class HealthParameters : ScriptableObject
{
    [Min(0)] public int MaxHealth = 6;
}
