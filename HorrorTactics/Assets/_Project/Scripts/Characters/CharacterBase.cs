using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Sirenix.OdinInspector;

public abstract class CharacterBase : MonoBehaviour
{
    [Header("Settings")]
    [InlineEditor]
    public MovementParameters MovementParameters = null;
    public FactionInstance Faction = null;

    public CharacterMoveComponent MoveComponent { get; private set; } = null;
    public HealthComponent Health { get; private set; } = null;
    public Dictionary<Type, int> StatusEffectTypeCollection { get; private set; } = new Dictionary<Type, int>();
    public List<StatusEffectBase> StatusEffectList { get; private set; } = new List<StatusEffectBase>();

    public event Action OnInterruptAction = null;
    public event Action OnStatusEffectsUpdated = null;
    public event Action OnDeath = null;
    public event Action OnObjectDestroyed = null;

    protected virtual void Awake()
    {
        MoveComponent = GetComponent<CharacterMoveComponent>();
        Health = GetComponent<HealthComponent>();

        Faction.RegisterCharacter(this);

        Health.OnDeath += this.onDeath;
    }

    protected virtual void OnDestroy()
    {
        if (Application.isPlaying)
            OnObjectDestroyed?.Invoke();
    }

    public void SetPosition(Vector3Int position)
    {
        transform.position = position;
    }

    public virtual void InterruptAction()
    {
        OnInterruptAction?.Invoke();
    }

    public void AddStatusEffect(StatusEffectBase newStatusEffect)
    {
        Type _statusType = newStatusEffect.GetType();

        if (StatusEffectTypeCollection.TryGetValue(_statusType, out int _listIndex))
        {
            var _activeStatusEffect = StatusEffectList[_listIndex];
            _activeStatusEffect.ReApplyStatusEffect(newStatusEffect);
        }
        else
        {
            StatusEffectList.Add(newStatusEffect);
            StatusEffectTypeCollection.Add(_statusType, StatusEffectList.IndexOf(newStatusEffect));

            newStatusEffect.Apply(this);
        }

        OnStatusEffectsUpdated?.Invoke();
    }

    public void RemoveStatusEffect(StatusEffectBase statusEffect)
    {
        Type _statusType = statusEffect.GetType();

        statusEffect.OnRemove();
        StatusEffectTypeCollection.Remove(_statusType);
        StatusEffectList.Remove(statusEffect);

        OnStatusEffectsUpdated?.Invoke();
    }

    public void RemoveStatusEffect(Type statusEffectType)
    {
        if (StatusEffectTypeCollection.TryGetValue(statusEffectType, out int _listIndex))
        {
            StatusEffectBase _statusEffect = StatusEffectList[_listIndex];

            _statusEffect.OnRemove();
            StatusEffectTypeCollection.Remove(statusEffectType);
            StatusEffectList.Remove(_statusEffect);

            OnStatusEffectsUpdated?.Invoke();
        }
    }

    private void onDeath()
    {
        for (int i = StatusEffectList.Count; i --> 0;)
            RemoveStatusEffect(StatusEffectList[i]);

        Faction.UnregisterCharacter(this);

        OnDeath?.Invoke();
    }
}
