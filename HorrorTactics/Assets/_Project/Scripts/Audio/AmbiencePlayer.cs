using ApplicationCore;
using FMOD.Studio;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using MEC;

public class AmbiencePlayer : SingletonBehaviour<AmbiencePlayer>
{
    [SerializeField] private Sami.Audio.FMODEventAsset m_defaultAmbience = null;

    private EventInstance m_playingEvent;

    private void Start()
    {
        if (Instance == this)
        {
            m_playingEvent = m_defaultAmbience.CreateInstance();
            m_playingEvent.setVolume(0.0f);
            m_playingEvent.start();

            Timing.RunCoroutine(coroutine_fadeIn());
        }
    }

    private void OnDestroy()
    {
        if (Application.isPlaying == false)
            return;

        if (m_playingEvent.isValid())
        {
            m_playingEvent.stop(STOP_MODE.IMMEDIATE);
            m_playingEvent.release();
        }
    }

    private IEnumerator<float> coroutine_fadeIn()
    {
        float _volume = 0.0f;

        while (_volume < 1.0f)
        {
            _volume += Time.deltaTime * 0.3f;
            m_playingEvent.setVolume(_volume);
            yield return Timing.WaitForOneFrame;
        }

        m_playingEvent.setVolume(1.0f);
    }
}
