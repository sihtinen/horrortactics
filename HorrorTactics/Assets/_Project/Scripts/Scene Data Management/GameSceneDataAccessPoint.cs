using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

using Sirenix.OdinInspector;

public class GameSceneDataAccessPoint : SingletonBehaviour<GameSceneDataAccessPoint>
{
    public GameSceneData Data
    {
        get { return m_sceneData; }
    }

    [Header("Object References")]
    [SerializeField, InlineEditor] private GameSceneData m_sceneData = null;

    private List<SceneTransitionComponent> m_registeredTransitionComponents = new List<SceneTransitionComponent>();

    public void RegisterSceneTransitionPoint(SceneTransitionComponent instance)
    {
        m_registeredTransitionComponents.Add(instance);
    }

    public SceneTransitionComponent GetSpawnPoint(SceneTransitionData transitionData)
    {
        if (transitionData == null) return null;

        for (int i = 0; i < m_registeredTransitionComponents.Count; i++)
        {
            if (m_registeredTransitionComponents[i].TransitionData == transitionData)
                return m_registeredTransitionComponents[i];
        }

        return null;
    }
}
