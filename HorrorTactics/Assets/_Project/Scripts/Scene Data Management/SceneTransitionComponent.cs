using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public class SceneTransitionComponent : MonoBehaviour
{
    [Header("Object References")]
    public SceneTransitionData TransitionData = null;
    [SerializeField] private GameSceneData m_linkedSceneData = null;

    [SerializeField] private Sami.Audio.FMODEventAsset m_audioEvent = null;

    private void Start()
    {
        GameSceneDataAccessPoint.Instance.RegisterSceneTransitionPoint(this);
    }

    public void BeginTransition()
    {
        if (m_audioEvent)
            m_audioEvent.PlayAtPosition(Vector3.zero);

        PersistentGameSessionSingleton.Instance.BeginSceneTransition(TransitionData, m_linkedSceneData.SceneAssetReference);
    }
}
