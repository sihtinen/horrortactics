using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AddressableAssets;

using ApplicationCore;

using Sirenix.OdinInspector;

using MEC;

public class PersistentGameSessionSingleton : SingletonBehaviour<PersistentGameSessionSingleton>
{
    [Header("Runtime Parameters")]
    [ReadOnly] public SceneTransitionData CurrentSceneTransitionData = null;

    protected override void Awake()
    {
        base.Awake();

        if (Instance == this)
            ApplicationCoreManager.OnSceneLoadCompleted += this.onSceneLoaded;
    }

    private void OnDestroy()
    {
        if (Application.isPlaying && Instance == this)
            ApplicationCoreManager.OnSceneLoadCompleted += this.onSceneLoaded;
    }

    public void BeginSceneTransition(SceneTransitionData transitionData, AssetReference sceneAssetReference)
    {
        CurrentSceneTransitionData = transitionData;
        ApplicationSceneLoader.LoadScene(sceneAssetReference);
    }

    private void onSceneLoaded()
    {
        Timing.RunCoroutine(coroutine_startScene());
    }

    private IEnumerator<float> coroutine_startScene()
    {
        yield return Timing.WaitForOneFrame;

        SceneTransitionComponent _spawnPoint =
            GameSceneDataAccessPoint.Instance.GetSpawnPoint(CurrentSceneTransitionData);

        Timing.RunCoroutine(PlayerActionHandler.Instance.SpawnControllableCharacters(_spawnPoint));
    }
}