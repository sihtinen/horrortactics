using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AddressableAssets;

[System.Serializable]
[CreateAssetMenu(menuName = "Game Scene Data", fileName = "SceneData_SCENENAME")]
public class GameSceneData : ScriptableObject
{
    [Header("Object References")]
    public AssetReference SceneAssetReference = null;
}