using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Scene Data/New Scene Transition Data", fileName = "New Scene Transition Data")]
public class SceneTransitionData : ScriptableObject
{

}