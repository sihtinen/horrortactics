using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Actions/Reload Weapon", fileName = "PlayerAction_ReloadWeapon")]
public class PlayerAction_ReloadWeapon : PlayerActionBase
{
    private WeaponItem m_weaponAsset = null;

    protected override void onBeginProcessAction(object parameters)
    {
        m_weaponAsset = parameters as WeaponItem;

        if (m_weaponAsset == null)
            Debug.LogError("PlayerAction_ReloadWeapon: input parameters are invalid");
    }

    protected override void onEndProcessAction()
    {
        m_weaponAsset = null;
    }

    public override void ProcessAction()
    {
        if (m_weaponAsset != null)
            PerformReloadAction();

        m_character.SetSelectedAction(null);
    }

    public void PerformReloadAction()
    {
        if (m_weaponAsset == null)
            return;

        var _inventory = m_character.InventoryComponent.InventoryData;
        var _weaponItemWrapper = _inventory.GetItemWrapper(m_weaponAsset);
        var _ammoItemWrapper = _inventory.GetItemWrapper(m_weaponAsset.AmmunitionAsset);

        int _remainingMissingAmmo = m_weaponAsset.ClipCapacity - _weaponItemWrapper.Amount;

        _weaponItemWrapper.Amount += _remainingMissingAmmo;
        _ammoItemWrapper.Amount -= _remainingMissingAmmo;

        if (_ammoItemWrapper.Amount < 0)
        {
            _weaponItemWrapper.Amount += _ammoItemWrapper.Amount;
            _inventory.RemoveItem(_ammoItemWrapper.Item);
        }

        _inventory.OnInventoryUpdated?.Invoke();
        m_character.OnWeaponReload?.Invoke();
    }

    public override bool CanPerformAction(ControllableCharacter character, object parameters)
    {
        //var _inventory = character.InventoryComponent.InventoryData;

        //int _ammoCount = _inventory.GetAmountInInventory(character.EquippedWeapon.AmmunitionAsset);
        //if (_ammoCount == 0)
        //    return false;

        //var _weaponItemWrapper = _inventory.GetItemWrapper(character.EquippedWeapon);

        //if (_weaponItemWrapper.Amount < character.EquippedWeapon.ClipCapacity)
        //    return true;

        return false;
    }
}
