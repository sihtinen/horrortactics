using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Actions/Interact", fileName = "PlayerAction_Interact")]
public class PlayerAction_Interact : PlayerActionBase
{
    private Interactable m_interactable = null;

    public override bool CanPerformAction(ControllableCharacter character, object parameters)
    {
        if (parameters != null)
        {
            switch (parameters)
            {
                case Interactable _interactable:

                    if (_interactable.CanInteractEvaluateDelegate() == false)
                        return false;

                    break;
            }
        }

        return true;
    }

    protected override void onBeginProcessAction(object parameters = null)
    {
        if ((parameters is Interactable) == false)
        {
            Debug.LogError("PlayerAction_Interact.onBeginProcessAction(): input parameters are not of type Interactable");
            return;
        }

        m_interactable = parameters as Interactable;
    }

    public override void ProcessAction()
    {
        m_interactable.OnInteract?.Invoke();

        m_character.SetSelectedAction(null);
    }

    protected override void onEndProcessAction()
    {
        m_interactable = null;
    }
}
