using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Sirenix.OdinInspector;

public struct PlayerActionSelectionOption
{
    public PlayerActionBase Action;
    public object ActionParameters;
}

[System.Serializable]
public abstract class PlayerActionBase : ScriptableObject
{
    [Header("Base Action Settings")]
    public int[] APCosts = null;

    [Header("Base UI Settings")]
    public string DisplayName = "New Action";
    [Title("Description", bold: false)]
    [HideLabel]
    [MultiLineProperty(3)]
    public string Description = "Description";
    public Sprite Icon = null;

    [Header("Audio Events")]
    [SerializeField] private Sami.Audio.FMODEventAsset m_audioBeginAction = null;
    [SerializeField] private Sami.Audio.FMODEventAsset m_audioEndAction = null;

    protected ControllableCharacter m_character = null;

    public void OnBeginValidateAction(ControllableCharacter character, object parameters = null)
    {
        m_character = character;

        onBeginProcessAction(parameters);

        if (m_audioBeginAction)
            m_audioBeginAction.PlayAtPosition(m_character.MoveComponent.IntPosition);
    }

    public void OnEndProcessAction()
    {
        onEndProcessAction();

        if (m_audioEndAction)
            m_audioEndAction.PlayAtPosition(m_character.MoveComponent.IntPosition);

        m_character = null;
    }

    protected abstract void onBeginProcessAction(object parameters = null);
    public abstract void ProcessAction();
    protected abstract void onEndProcessAction();
    public abstract bool CanPerformAction(ControllableCharacter character, object parameters = null);
}
