using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Unity.Mathematics;

[System.Serializable]
[CreateAssetMenu(menuName = "Actions/Ranged Ability", fileName = "New PlayerAction_RangedAbility")]
public class PlayerAction_RangedAbility : PlayerActionBase
{
    [Header("Ranged Ability Settings")]
    [Min(1)] public int RangeMin = 1;
    [Min(1)] public int RangeMax = 12;

    [Space]

    public int HealthEffect = 0;
    public bool MakeTargetHostile = false;

    [Space]

    [Min(0)] public int AmmoCost = 1;

    [Space]

    public StatusInflictParameters<StunStatus> StunParameters = new StatusInflictParameters<StunStatus>();

    [Space]

    public bool CanTargetGround = false;
    public bool CanTargetControllableCharacters = false;
    public bool CanTargetAICharacters = false;

    private WeaponItem m_weaponInUse = null;

    protected override void onBeginProcessAction(object parameters)
    {
        m_weaponInUse = parameters as WeaponItem;

        MouseRaycaster.Instance.OnHovered_ControllableCharacter += this.onHoveredControllableCharacter;
        MouseRaycaster.Instance.OnHovered_AICharacter += this.onHoveredAICharacter;

        MouseRaycaster.Instance.OnClicked_ControllableCharacter += this.onClickedControllableCharacter;
        MouseRaycaster.Instance.OnClicked_AICharacter += this.onclickedAICharacter;
    }

    public override void ProcessAction()
    {

    }

    protected override void onEndProcessAction()
    {
        MouseRaycaster.Instance.OnHovered_ControllableCharacter -= this.onHoveredControllableCharacter;
        MouseRaycaster.Instance.OnHovered_AICharacter -= this.onHoveredAICharacter;

        MouseRaycaster.Instance.OnClicked_ControllableCharacter -= this.onClickedControllableCharacter;
        MouseRaycaster.Instance.OnClicked_AICharacter -= this.onclickedAICharacter;

        RangedAbilityVisualizer.Instance.SetEnabled(false);
    }

    public override bool CanPerformAction(ControllableCharacter character, object parameters)
    {
        //if (character.EquippedWeapon.GetRemainingAmmoCount(character) < AmmoCost)
        //    return false;

        return true;
    }

    private void invalidTarget()
    {
        RangedAbilityVisualizer.Instance.SetEnabled(false);
    }

    private void onHoveredPosition(Vector3 position)
    {
        if (CanTargetGround == false)
        {
            invalidTarget();
            return;
        }

        //if (canPerformAbilityToPosition(position) == false)
        //{
        //    invalidTarget();
        //    return;
        //}

        Vector3 _characterPosition = m_character.transform.position;
        _characterPosition += Vector3.up * 1.5f;

        RangedAbilityVisualizer.Instance.SetPositions(_characterPosition, position);
        RangedAbilityVisualizer.Instance.SetEnabled(true);
    }

    private void onHoveredControllableCharacter(ControllableCharacter character)
    {
        if (CanTargetControllableCharacters == false)
        {
            invalidTarget();
            return;
        }

        if (character == null ||
            canPerformAbilityToPosition(character.transform.position.ToVector3Int()) == false)
        {
            invalidTarget();
            return;
        }

        Vector3 _characterPosition = m_character.transform.position;
        _characterPosition += Vector3.up * 1.5f;

        Vector3 _mousePosition = character.transform.position + Vector3.up * 1.5f;

        RangedAbilityVisualizer.Instance.SetPositions(_characterPosition, _mousePosition);
        RangedAbilityVisualizer.Instance.SetEnabled(true);
    }

    private void onHoveredAICharacter(AICharacter character)
    {
        if (CanTargetAICharacters == false)
        {
            invalidTarget();
            return;
        }

        if (character == null ||
            canPerformAbilityToPosition(character.transform.position.ToVector3Int()) == false)
        {
            invalidTarget();
            return;
        }

        Vector3 _characterPosition = m_character.transform.position;
        _characterPosition += Vector3.up * 1.5f;

        Vector3 _mousePosition = character.transform.position + Vector3.up * 1.5f;

        RangedAbilityVisualizer.Instance.SetPositions(_characterPosition, _mousePosition);
        RangedAbilityVisualizer.Instance.SetEnabled(true);
    }

    private void finalizeAction()
    {
        for (int i = 0; i < AmmoCost; i++)
            m_weaponInUse?.Use(m_character);

        m_character.SetSelectedAction(null);
    }

    private void onClickedControllableCharacter(ControllableCharacter character)
    {
        if (CanTargetControllableCharacters == false)
            return;

        if (character == null ||
            canPerformAbilityToPosition(character.transform.position.ToVector3Int()) == false)
        {
            invalidTarget();
            return;
        }

        if (HealthEffect != 0)
            character.Health.DealHealthDamage(-HealthEffect);

        if (StunParameters.WillInflict())
            character.AddStatusEffect(StunParameters.GetStatusEffectInstance());

        finalizeAction();
    }

    private void onclickedAICharacter(AICharacter character)
    {
        if (CanTargetAICharacters == false)
            return;

        if (character == null ||
            canPerformAbilityToPosition(character.transform.position.ToVector3Int()) == false)
        {
            invalidTarget();
            return;
        }

        if (HealthEffect != 0)
            character.Health.DealHealthDamage(-HealthEffect);

        if (MakeTargetHostile)
            character.ThreatComponent.SetHostileTowardsCharacter(m_character);

        if (StunParameters.WillInflict())
            character.AddStatusEffect(StunParameters.GetStatusEffectInstance());

        finalizeAction();
    }

    private bool canPerformAbilityToPosition(Vector3 position)
    {
        // implement line of sight check here
        return true;
    }
}
