using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[Serializable]
[CreateAssetMenu(menuName = "Actions/Melee Ability", fileName = "PlayerAction_MeleeAbility")]
public class PlayerAction_MeleeAbility : PlayerActionBase
{
    [Header("Settings")]
    [SerializeField] private int m_range = 1;
    [SerializeField] private bool m_diagonal = false;
    [Space]
    [SerializeField] private bool m_targetControllableCharacters = false;
    [SerializeField] private bool m_targetAICharacters = false;
    [Space]
    [SerializeField, Min(0)] private int m_damage = 0;
    [SerializeField, Min(0)] private int m_pushDistance = 0;
    [Space]
    [SerializeField] private StatusInflictParameters<StunStatus> m_pushStun_Target = new StatusInflictParameters<StunStatus>();
    [SerializeField] private StatusInflictParameters<StunStatus> m_pushStun_Other = new StatusInflictParameters<StunStatus>();

    private List<Vector3Int> m_validPositions = new List<Vector3Int>();

    public override bool CanPerformAction(ControllableCharacter character, object parameters)
    {
        for (int x = -m_range; x <= m_range; x++)
        {
            for (int z = -m_range; z <= m_range; z++)
            {
                if (m_diagonal == false && x != 0 && z != 0)
                    continue;

                //Vector3Int _position = character.MoveComponent.IntPosition + new Vector3Int(x, 0, z);
                //CharacterBase _character = NavigationPathfinder.Instance.GetOccupiedCharacter(_position);

                //if (_character != null)
                //{
                //    if (m_targetControllableCharacters && _character is ControllableCharacter)
                //        return true;

                //    if (m_targetAICharacters && _character is AICharacter)
                //        return true;
                //}
            }
        }

        return false;
    }

    public override void ProcessAction()
    {

    }

    protected override void onBeginProcessAction(object parameters = null)
    {
        m_validPositions.Clear();

        for (int x = -m_range; x <= m_range; x++)
        {
            for (int z = -m_range; z <= m_range; z++)
            {
                if (m_diagonal == false && x != 0 && z != 0) continue;

                Vector3Int _position = m_character.MoveComponent.IntPosition + new Vector3Int(x, 0, z);

                m_validPositions.Add(_position);
            }
        }

        MouseRaycaster.Instance.OnHovered_ControllableCharacter += this.onHoveredControllableCharacter;
        MouseRaycaster.Instance.OnHovered_AICharacter += this.onHoveredAICharacter;

        MouseRaycaster.Instance.OnClicked_ControllableCharacter += this.onClickedControllableCharacter;
        MouseRaycaster.Instance.OnClicked_AICharacter += this.onClickedAICharacter;
    }

    protected override void onEndProcessAction()
    {
        MouseRaycaster.Instance.OnHovered_ControllableCharacter -= this.onHoveredControllableCharacter;
        MouseRaycaster.Instance.OnHovered_AICharacter -= this.onHoveredAICharacter;

        MouseRaycaster.Instance.OnClicked_ControllableCharacter -= this.onClickedControllableCharacter;
        MouseRaycaster.Instance.OnClicked_AICharacter -= this.onClickedAICharacter;
    }

    private void onHoveredControllableCharacter(ControllableCharacter character)
    {

    }

    private void onHoveredAICharacter(AICharacter character)
    {

    }

    private void onClickedControllableCharacter(ControllableCharacter character)
    {
        if (m_targetControllableCharacters == false) return;
        if (character == null) return;
        if (m_validPositions.Contains(character.MoveComponent.IntPosition) == false) return;

        processClickedCharacter(character);
    }

    private void onClickedAICharacter(AICharacter character)
    {
        if (m_targetAICharacters == false) return;
        if (character == null) return;
        if (m_validPositions.Contains(character.MoveComponent.IntPosition) == false) return;

        processClickedCharacter(character);
    }

    private void processClickedCharacter(CharacterBase character)
    {
        if (m_damage > 0) character.Health.DealHealthDamage(m_damage);

        if (m_pushDistance > 0)
        {
            Vector3Int _direction = character.MoveComponent.IntPosition - m_character.MoveComponent.IntPosition;
            _direction.x = Mathf.Clamp(_direction.x, -1, 1);
            _direction.z = Mathf.Clamp(_direction.z, -1, 1);

            PushActionHandler.Instance.OnPushCollision += this.onPushCollision;
            PushActionHandler.Instance.OnPushRoutineCompleted += this.onPushRoutineCompleted;

            PushActionHandler.Instance.PushCharacterToDirection(character, _direction, m_pushDistance);
        }
        else
        {
            m_character.SetSelectedAction(null);
        }
    }

    private void onPushRoutineCompleted()
    {
        PushActionHandler.Instance.OnPushCollision -= this.onPushCollision;
        PushActionHandler.Instance.OnPushRoutineCompleted -= this.onPushRoutineCompleted;

        m_character.SetSelectedAction(null);
    }

    private void onPushCollision(PushCollision collision)
    {
        if (collision.PushedCharacter && m_pushStun_Target.WillInflict())
            collision.PushedCharacter.AddStatusEffect(m_pushStun_Target.GetStatusEffectInstance());

        if (collision.CollidedCharacter && m_pushStun_Other.WillInflict())
            collision.CollidedCharacter.AddStatusEffect(m_pushStun_Other.GetStatusEffectInstance());
    }
}
