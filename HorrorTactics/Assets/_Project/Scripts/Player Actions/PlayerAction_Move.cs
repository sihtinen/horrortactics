using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Actions/Move", fileName = "PlayerAction_Move")]
public class PlayerAction_Move : PlayerActionBase
{
    [Header("Audio Events")]
    [SerializeField] private Sami.Audio.FMODEventAsset m_audioPerformAction = null;

    protected override void onBeginProcessAction(object parameters)
    {
        MouseTargetVisualizer.SetVisualizationType(MouseTargetVisualizer.VisualizationType.Movement);
        MouseRaycaster.Instance.OnHovered_NavMesh += this.onPositionHovered;
        MouseRaycaster.Instance.OnClicked_NavMesh += this.onPositionClicked;
    }

    public override void ProcessAction()
    {
        MouseTargetVisualizer.SetActive(MouseUIHoverManager.IsMouseOverUI == false && MouseRaycaster.Instance.HitNavMeshPoint.HasValue);
    }

    protected override void onEndProcessAction()
    {
        MouseRaycaster.Instance.OnHovered_NavMesh -= this.onPositionHovered;
        MouseRaycaster.Instance.OnClicked_NavMesh -= this.onPositionClicked;
        MouseTargetVisualizer.SetActive(false);
    }

    private void onPositionHovered(Vector3 position)
    {
        MouseTargetVisualizer.SetPosition(position);
    }

    private void onPositionClicked(Vector3 position)
    {
        if (MouseUIHoverManager.IsMouseOverUI)
            return;

        m_character.MoveComponent.SetMoveDestination(position);

        if (m_audioPerformAction != null)
            m_audioPerformAction.PlayAtPosition(position);
    }

    public override bool CanPerformAction(ControllableCharacter character, object parameters)
    {
        return true;
    }
}
