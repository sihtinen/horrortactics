using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Actions/Equip Weapon", fileName = "PlayerAction_EquipWeapon")]
public class PlayerAction_EquipWeapon : PlayerActionBase
{
    private WeaponItem m_weaponAsset = null;

    public override bool CanPerformAction(ControllableCharacter character, object parameters)
    {
        return false;
    }

    protected override void onBeginProcessAction(object parameters)
    {
        m_weaponAsset = parameters as WeaponItem;

        if (m_weaponAsset == null)
        {
            Debug.LogError("PlayerAction_EquipWeapon: input parameters are invalid");
            return;
        }
    }

    protected override void onEndProcessAction()
    {
        m_weaponAsset = null;
    }

    public override void ProcessAction()
    {
        if (m_character.InventoryComponent.IsItemEquipped(m_weaponAsset))
            m_character.InventoryComponent.UnequipItem(m_weaponAsset);
        else
            m_character.InventoryComponent.EquipItem(m_weaponAsset);

        m_character.OnWeaponEquipped?.Invoke();
    }
}
