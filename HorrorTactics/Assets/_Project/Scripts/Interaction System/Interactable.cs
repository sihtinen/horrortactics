using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

using Sirenix.OdinInspector;

public class Interactable : MonoBehaviour
{
    public delegate bool CanInteractEvaluate();
    public CanInteractEvaluate CanInteractEvaluateDelegate = delegate { return true; };

    public bool IsVisibleToPlayer { get { return m_isVisibleToPlayer; } }

    public Action<bool> OnEnabledChanged = null;

    [Header("Runtime Parameters")]
    [SerializeField, ReadOnly] private bool m_isVisibleToPlayer = false;

    [Header("Settings")]
    [Min(0)] public int MinInteractVolumeRadius = 0;
    [Min(0)] public int MaxInteractVolumeRadius = 0;
    public Vector3 TooltipPositionOffset = Vector3.zero;

    [Header("Events")]
    public UnityEvent OnInteract = new UnityEvent();
    public UnityEvent OnEnterInteractionRange = new UnityEvent();
    public UnityEvent OnLeaveInteractionRange = new UnityEvent();

    [Header("Object References")]
    [SerializeField] private PlayerAction_Interact m_interactAction = null;

    private ControllableCharacter m_characterBinding = null;
    private Tuple<PlayerActionBase, object> m_actionWrapper = null;

    private Vector3Int m_intPosition;

    public void SetInteractAction(PlayerAction_Interact interactAction)
    {
        m_interactAction = interactAction;
    }

    private void initializeSelf()
    {
        m_actionWrapper = new Tuple<PlayerActionBase, object>(m_interactAction, this);

        m_intPosition = Vector3Int.RoundToInt(transform.position);
        transform.position = m_intPosition;
    }

    private void Start()
    {
        initializeSelf();

        InteractableCollection.RegisterInteractable(this);
    }

    private void OnDestroy()
    {
        if (Application.isPlaying)
            InteractableCollection.UnregisterInteractable(this);
    }

    private void OnEnable()
    {
        if (Application.isPlaying)
            OnEnabledChanged?.Invoke(true);
    }

    private void OnDisable()
    {
        if (Application.isPlaying)
            OnEnabledChanged?.Invoke(true);
    }

    private void clearBindings()
    {
        if (m_characterBinding == null)
            return;

        m_characterBinding.UnregisterDynamicAction(m_actionWrapper);
        m_characterBinding = null;
    }
}
