using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public static class InteractableCollection
{
    public static Action<Interactable> OnInteractableRegistered = null;

    private static List<Interactable> m_registeredInteractables = new List<Interactable>();

    public static void RegisterInteractable(Interactable interactable)
    {
        m_registeredInteractables.Add(interactable);

        OnInteractableRegistered?.Invoke(interactable);
    }

    public static void UnregisterInteractable(Interactable interactable)
    {
        m_registeredInteractables.Remove(interactable);
    }
}