using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[RequireComponent(typeof(Interactable))]
[RequireComponent(typeof(InventoryComponent))]
public class InteractableInventory : MonoBehaviour
{
    [Header("Interaction Settings")]
    [SerializeField] private bool m_enabledIfEmpty = true;

    [Header("Visualization Settings")]
    [SerializeField] private Vector3Int m_visualVolumeOffset;

    private bool m_isVisible = false;

    private Interactable m_interactable = null;
    private InventoryComponent m_inventoryComponent = null;

    private MeshRenderer[] m_itemVisualizers = null;

    private void Awake()
    {
        m_interactable = GetComponent<Interactable>();
        m_inventoryComponent = GetComponent<InventoryComponent>();
        m_itemVisualizers = GetComponentsInChildren<MeshRenderer>();

        m_interactable.OnInteract.AddListener(this.OpenInventory);
        m_interactable.OnEnabledChanged += this.onInteractableEnabledChanged;
    }

    private void Start()
    {
        if (m_enabledIfEmpty == false &&
            m_inventoryComponent.InventoryData.IsEmpty())
        {
            m_interactable.enabled = false;
        }

        updateVisualizations();
    }

    public void OpenInventory()
    {
        UI_InventoryScreen.Instance.OnClosed += this.onInventoryClosed;
        //UI_InventoryCanvas.Instance.Open(m_inventoryComponent);
    }

    private void onInventoryClosed()
    {
        UI_InventoryScreen.Instance.OnClosed -= this.onInventoryClosed;

        if (m_enabledIfEmpty == false &&
            m_inventoryComponent.InventoryData.IsEmpty())
        {
            m_interactable.enabled = false;
        }
    }

    private void updateVisualizations()
    {
        if (m_itemVisualizers != null && m_itemVisualizers.Length > 0)
        {
            bool _areVisible = m_interactable.enabled && m_isVisible;

            for (int i = 0; i < m_itemVisualizers.Length; i++)
                m_itemVisualizers[i].enabled = _areVisible;
        }
    }

    private void onInteractableEnabledChanged(bool isEnabled)
    {
        updateVisualizations();
    }
}