using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Sirenix.OdinInspector;

public class Door : MonoBehaviour
{
    [Header("Runtime Parameters")]
    [ReadOnly] public bool IsOpened = false;

    [Header("Audio Events")]
    [SerializeField] private Sami.Audio.FMODEventAsset m_audioOpen = null;
    [SerializeField] private Sami.Audio.FMODEventAsset m_audioClose = null;

    private Vector3Int m_intPosition;

    private Interactable m_interactable = null;

    private void Awake()
    {
        m_interactable = GetComponent<Interactable>();
        m_intPosition = transform.position.ToVector3Int();
        transform.position = m_intPosition;
    }

    private void Start()
    {
        SetDoorOpened(IsOpened, playAudio: false);
    }

    public void Interact()
    {
        SetDoorOpened(IsOpened == false);
    }

    public void SetDoorOpened(bool isOpened, bool playAudio = true)
    {
        IsOpened = isOpened;

        if (playAudio == false)
            return;

        if (IsOpened)
        {
            if (m_audioOpen)
                m_audioOpen.PlayAtPosition(transform.position);
        }
        else
        {
            if (m_audioClose)
                m_audioClose.PlayAtPosition(transform.position);
        }
    }
}
