using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

using MEC;

public struct PushCollision
{
    public PushCollision(int i = 0)
    {
        PushedCharacter = null;
        CollidedCharacter = null;
    }

    public CharacterBase PushedCharacter;
    public CharacterBase CollidedCharacter;
}

public class PushActionHandler : SingletonBehaviour<PushActionHandler>
{
    public Action<PushCollision> OnPushCollision = null;
    public Action OnPushRoutineCompleted = null;

    public void PushCharacterToDirection(CharacterBase character, Vector3Int direction, int distance)
    {
        Timing.RunCoroutine(coroutine_pushCharacterToDirection(character, direction, distance));
    }

    private IEnumerator<float> coroutine_pushCharacterToDirection(CharacterBase character, Vector3Int direction, int distance)
    {
        yield return Timing.WaitForOneFrame;

        OnPushRoutineCompleted?.Invoke();
    }
}
