using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "Characters/New Vision Parameters", fileName = "New Vision Parameters")]
public class VisionParameters : ScriptableObject
{
    [Range(0, 32)] public int VisionDistance;
    [Range(0, 32)] public float VisionDistance_Deadzone;
    [Range(0, 180)] public float VisionAngle;

    [Header("Debug")]
    public bool DrawDebugLines = false;
    public float DebugDrawAliveDuration = 5.0f;
}
