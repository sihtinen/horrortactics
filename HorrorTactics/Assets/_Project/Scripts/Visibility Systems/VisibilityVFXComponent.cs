using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Sirenix.OdinInspector;

public class VisibilityVFXComponent : MonoBehaviour
{
    public enum InvisibleType
    {
        None,
        Invisible
    }

    [Header("Runtime Values")]
    [ReadOnly] public bool IsVisible = false;

    [Header("Settings")]
    [SerializeField] private InvisibleType m_invisibleType = InvisibleType.None;

    [Header("Object References")]
    [SerializeField] private Renderer[] m_renderers = null;

    public void SetVisible(bool isVisible)
    {
        IsVisible = isVisible;

        if (IsVisible)
        {
            switch (m_invisibleType)
            {
                case InvisibleType.None:
                    break;

                case InvisibleType.Invisible:

                    for (int i = 0; i < m_renderers.Length; i++)
                        m_renderers[i].enabled = true;

                    break;
            }
        }
        else
        {
            switch (m_invisibleType)
            {
                case InvisibleType.None:
                    break;

                case InvisibleType.Invisible:

                    for (int i = 0; i < m_renderers.Length; i++)
                        m_renderers[i].enabled = false;

                    break;
            }
        }
    }

    public void DisableShadows()
    {
        for (int i = 0; i < m_renderers.Length; i++)
        {
            m_renderers[i].shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        }
    }
}
