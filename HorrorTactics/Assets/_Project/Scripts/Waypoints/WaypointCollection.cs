using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

public class WaypointCollection : MonoBehaviour
{
    [Header("Object References")]
    public List<Waypoint> Waypoints = new List<Waypoint>();

    public Waypoint SelectRandomWaypoint()
    {
        return Waypoints.GetRandomElement();
    }

    [ContextMenu("Create New Waypoint")]
    public void CreateNewWaypoint()
    {
        GameObject _newWaypoint;
        Waypoint _waypointComponent;

        if (Waypoints.Count == 0)
        {
            _newWaypoint = new GameObject("Waypoint_0");
            _newWaypoint.transform.SetParent(transform, false);
            _waypointComponent = _newWaypoint.AddComponent<Waypoint>();
            _waypointComponent.Collection = this;
            Waypoints.Add(_waypointComponent);

            ValidateData();

            return;
        }

        Waypoint _prevWaypoint = Waypoints[Waypoints.Count - 1];
        _newWaypoint = Instantiate(_prevWaypoint.gameObject);
        _newWaypoint.transform.SetParent(transform, true);
        _waypointComponent = _newWaypoint.GetComponent<Waypoint>();
        _waypointComponent.Collection = this;
        Waypoints.Add(_waypointComponent);

        ValidateData();
    }

    [ContextMenu("Validate Positions")]
    public void ValidatePositions()
    {
        for (int i = 0; i < Waypoints.Count; i++)
        {
            Waypoints[i].transform.position = Vector3Int.RoundToInt(Waypoints[i].transform.position);
        }
    }

    [ContextMenu("Validate Data")]
    public void ValidateData()
    {
        for (int i = Waypoints.Count; i --> 0;)
        {
            if (Waypoints[i] == null)
                Waypoints.RemoveAt(i);
        }

        Waypoints = Waypoints.Distinct().ToList();

        Waypoint[] _foundWaypoints = GetComponentsInChildren<Waypoint>();
        for (int i = 0; i < _foundWaypoints.Length; i++)
        {
            if (Waypoints.Contains(_foundWaypoints[i]) == false)
                Waypoints.Add(_foundWaypoints[i]);
        }

        for (int i = 0; i < Waypoints.Count; i++)
        {
            Waypoint _current = Waypoints[i];
            Waypoint _prev = null;

            _current.To.Clear();
            _current.From.Clear();

            _current.gameObject.name = $"Waypoint_{i}";

            if (i > 0)
            {
                _prev = Waypoints[i - 1];
            }

            if (_prev != null)
            {
                _current.From.Add(_prev);
                _prev.To.Add(_current);
            }

            if (i == Waypoints.Count - 1)
            {
                _current.To.Add(Waypoints[0]);
                Waypoints[0].From.Add(_current);
            }
        }
    }

    private void OnValidate()
    {
        if (transform.childCount != Waypoints.Count)
            ValidateData();

        ValidatePositions();
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < Waypoints.Count; i++)
        {
            Waypoint _current = Waypoints[i];

            Gizmos.DrawWireSphere(_current.transform.position, 0.22f);

            if (_current.To.Count > 0)
            {
                for (int ii = 0; ii < _current.To.Count; ii++)
                {
                    Gizmos.DrawLine(_current.To[ii].transform.position, _current.transform.position);

                    Vector3 _diff = _current.To[ii].transform.position - _current.transform.position;
                    Vector3 _diffNormalized = _diff.normalized;
                    Vector3 _left = Vector3.Cross(_diff, Vector3.up).normalized;

                    Vector3 _arrowHeadPos = _current.To[ii].transform.position - _diffNormalized * 0.22f;

                    Gizmos.DrawLine(_arrowHeadPos,
                        _arrowHeadPos - _diffNormalized * 0.6f + _left * 0.4f);
                    Gizmos.DrawLine(_arrowHeadPos,
                        _arrowHeadPos - _diffNormalized * 0.6f - _left * 0.4f);
                }
            }
        }
    }
}
