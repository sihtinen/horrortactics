using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class Waypoint : MonoBehaviour
{
    public WaypointCollection Collection = null;

    public List<Waypoint> From = new List<Waypoint>();
    public List<Waypoint> To = new List<Waypoint>();
}
