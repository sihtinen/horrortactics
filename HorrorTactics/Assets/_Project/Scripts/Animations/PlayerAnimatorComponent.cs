using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class PlayerAnimatorComponent : MonoBehaviour
{
    private Animator m_animator = null;
    private CharacterMoveComponent m_moveComponent = null;

    private static readonly string animState_Idle = "Idle";
    private static readonly string animState_Walk = "Walk";

    private void Awake()
    {
        m_animator = GetComponent<Animator>();
        m_moveComponent = GetComponentInParent<CharacterMoveComponent>();

        m_moveComponent.OnMovementStart += this.onMovementStart;
        m_moveComponent.OnMovementEnd += this.onMovementEnd;
    }

    private void onMovementStart()
    {
        m_animator.CrossFade(animState_Walk, 0.2f, 0);
    }

    private void onMovementEnd()
    {
        m_animator.CrossFade(animState_Idle, 0.2f, 0);
    }
}
