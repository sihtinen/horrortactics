using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions
{
    public static T GetRandomElement<T>(this List<T> list)
    {
        int _randomIndex = Random.Range(0, list.Count);
        return list[_randomIndex];
    }
}
