using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public static class RaycastHitExtensions
{
    public static RaycastHit GetClosest(this RaycastHit[] hits, int hitCount)
    {
        if (hitCount == 1)
            return hits[0];

        RaycastHit _closest = hits[0];

        for (int i = 0; i < hitCount; i++)
        {
            if (hits[i].distance < _closest.distance)
                _closest = hits[i];
        }

        return _closest;
    }
}
