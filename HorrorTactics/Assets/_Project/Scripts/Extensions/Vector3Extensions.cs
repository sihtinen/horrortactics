using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Unity.Mathematics;

public static class Vector3Extensions
{
    public static Vector3Int ToVector3Int(this Vector3 vector)
    {
        return Vector3Int.RoundToInt(vector);
    }

    public static Vector3Int ToVector3Int(this int3 vector)
    {
        return new Vector3Int(vector.x, vector.y, vector.z);
    }

    public static int3 ToInt3(this Vector3Int vector)
    {
        return new int3(vector.x, vector.y, vector.z);
    }
}
