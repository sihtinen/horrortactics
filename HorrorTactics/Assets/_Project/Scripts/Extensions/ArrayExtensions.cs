using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Unity.Mathematics;

public static class ArrayExtensions
{
    public static int2 CoordinatesOf<T>(this T[,] matrix, T value)
    {
        int _width = matrix.GetLength(0);
        int _height = matrix.GetLength(1);

        for (int x = 0; x < _width; ++x)
        {
            for (int y = 0; y < _height; ++y)
            {
                if (matrix[x, y].Equals(value))
                    return new int2(x, y);
            }
        }

        return new int2(-1, -1);
    }
}
