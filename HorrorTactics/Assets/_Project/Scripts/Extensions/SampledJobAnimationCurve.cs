using UnityEngine;

using Unity.Mathematics;
using Unity.Collections;

public struct SampledJobAnimationCurve : System.IDisposable
{
    private NativeArray<float> m_sampleList;

    /// <param name="samples">Must be 2 or higher</param>
    public SampledJobAnimationCurve(AnimationCurve curve, int samples, Allocator allocatorType)
    {
        m_sampleList = new NativeArray<float>(samples, allocatorType);

        float _timeFrom = curve.keys[0].time;
        float _timeTo = curve.keys[curve.keys.Length - 1].time;
        float _timeStep = (_timeTo - _timeFrom) / (samples - 1);

        for (int i = 0; i < samples; i++)
            m_sampleList[i] = curve.Evaluate(_timeFrom + (i * _timeStep));
    }

    public void Dispose()
    {
        if (m_sampleList.IsCreated)
            m_sampleList.Dispose();
    }

    /// <param name="time">Must be from 0 to 1</param>
    public float EvaluateLerp(float time)
    {
        int _length = m_sampleList.Length - 1;
        float _clamp01 = time < 0 ? 0 : (time > 1 ? 1 : time);
        float floatIndex = (_clamp01 * _length);
        int floorIndex = (int)math.floor(floatIndex);

        if (floorIndex == _length)
            return m_sampleList[_length];

        float lowerValue = m_sampleList[floorIndex];
        float higherValue = m_sampleList[floorIndex + 1];

        return math.lerp(lowerValue, higherValue, math.frac(floatIndex));
    }
}
