using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Unity.Collections;
using Unity.Jobs;

public class VisionMeshRaycaster : MonoBehaviour
{
    [SerializeField] private float m_visionDistance = 40;
    [SerializeField, Range(0, 180)] private float m_visionAngleHorizontal = 45;
    [SerializeField, Range(0, 180)] private float m_visionAngleVertical = 45;
    [SerializeField, Min(8)] private int m_resolution = 40;
    [SerializeField] private LayerMask m_hitLayers;
    [SerializeField] private Material m_meshMaterial = null;

    private GameObject m_meshObject = null;
    private MeshFilter m_meshFilter = null;
    private Mesh m_mesh = null;
    private MeshRenderer m_meshRenderer = null;

    private List<Vector3> m_vertices = new List<Vector3>();
    private List<int> m_triangles = new List<int>();

    private void Awake()
    {
        m_meshObject = new GameObject("Vision Mesh Object");
        m_meshObject.transform.SetParent(transform);
        m_meshObject.transform.localPosition = Vector3.zero;
        m_meshObject.transform.localEulerAngles = Vector3.zero;

        m_mesh = new Mesh();
        m_meshFilter = m_meshObject.AddComponent<MeshFilter>();
        m_meshFilter.sharedMesh = m_mesh;

        m_meshRenderer = m_meshObject.AddComponent<MeshRenderer>();
        m_meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        m_meshRenderer.receiveShadows = false;
        m_meshRenderer.sharedMaterial = m_meshMaterial;
    }

    void Start()
    {
        MEC.TimingUtility.SubscribeToUpdate(this.mecUpdate, this);
    }

    private void mecUpdate()
    {
        int _rayCount = m_resolution * m_resolution;

        float _halfAngleHorizontal = m_visionAngleHorizontal / 2;
        float _halfAngleVertical = m_visionAngleVertical / 2;

        var _results = new NativeArray<RaycastHit>(_rayCount, Allocator.TempJob);
        var _commands = new NativeArray<RaycastCommand>(_rayCount, Allocator.TempJob);

        int _x = 0;
        int _y = 0;

        for (int i = 0; i < _rayCount; i++)
        {
            float _horizontalAmount = _x / (float)(m_resolution - 1);
            float _verticalAmount = _y / (float)(m_resolution - 1);

            Vector3 _direction = transform.forward;
            _direction = Quaternion.AngleAxis(Mathf.Lerp(-_halfAngleHorizontal, _halfAngleHorizontal, _horizontalAmount), transform.up) * _direction;
            _direction = Quaternion.AngleAxis(Mathf.Lerp(-_halfAngleVertical, _halfAngleVertical, _verticalAmount), transform.right) * _direction;

            _commands[i] = new RaycastCommand(
                transform.position, 
                _direction, 
                distance: m_visionDistance, 
                layerMask: m_hitLayers, 
                maxHits: 1);

            _x++;

            if (_x == m_resolution)
            {
                _x = 0;
                _y++;
            }
        }

        JobHandle _handle = RaycastCommand.ScheduleBatch(_commands, _results, 1);
        _handle.Complete();

        m_vertices.Clear();
        m_vertices.Add(Vector3.zero);

        for (int i = 0; i < _rayCount; i++)
        {
            var _raycastHit = _results[i];
            var _command = _commands[i];

            Vector3 _vertexPosition = _raycastHit.collider != null ? 
                transform.InverseTransformPoint(_raycastHit.point) : 
                transform.InverseTransformPoint(transform.position + _command.direction * _command.distance);

            m_vertices.Add(_vertexPosition);
        }

        m_triangles.Clear();

        int _arrayLimitUpper = m_resolution - 1;
        int _lastEvaluationIndex = _arrayLimitUpper - 1;

        for (int y = 0; y < m_resolution; y++)
        {
            if (y == _arrayLimitUpper)
                continue;

            for (int x = 0; x < m_resolution; x++)
            {
                if (x == _arrayLimitUpper)
                    continue;

                int _index_lowerLeft = get1DIndex(x, y) + 1;
                int _index_upperLeft = get1DIndex(x, y + 1) + 1;
                int _index_lowerRight = get1DIndex(x + 1, y) + 1;
                int _index_upperRight = get1DIndex(x + 1, y + 1) + 1;

                m_triangles.Add(_index_lowerLeft);
                m_triangles.Add(_index_upperLeft);
                m_triangles.Add(_index_lowerRight);

                m_triangles.Add(_index_upperLeft);
                m_triangles.Add(_index_upperRight);
                m_triangles.Add(_index_lowerRight);

                if (y == 0)
                {
                    m_triangles.Add(0);
                    m_triangles.Add(_index_lowerLeft);
                    m_triangles.Add(_index_lowerRight);
                }
                else if (y == _lastEvaluationIndex)
                {
                    m_triangles.Add(0);
                    m_triangles.Add(_index_upperRight);
                    m_triangles.Add(_index_upperLeft);
                }

                if (x == 0)
                {
                    m_triangles.Add(0);
                    m_triangles.Add(_index_upperLeft);
                    m_triangles.Add(_index_lowerLeft);
                }
                else if (x == _lastEvaluationIndex)
                {
                    m_triangles.Add(0);
                    m_triangles.Add(_index_lowerRight);
                    m_triangles.Add(_index_upperRight);
                }
            }
        }

        _results.Dispose();
        _commands.Dispose();

        if (m_mesh.vertexCount > 0 && m_mesh.vertexCount != m_vertices.Count) 
            m_mesh.Clear();

        m_mesh.SetVertices(m_vertices);
        m_mesh.SetTriangles(m_triangles, 0);
    }

    private int get1DIndex(int x, int y) => (y * m_resolution) + x;
}
