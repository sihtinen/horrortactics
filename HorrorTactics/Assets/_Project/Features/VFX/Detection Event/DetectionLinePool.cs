using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

namespace HorrorTactics.VFX
{
    public class DetectionLinePool : ObjectPool<DetectionLine>
    {
        private void Start()
        {
            if (DetectionEventHub.Instance != null)
                DetectionEventHub.Instance.OnNewDetectionEvent += this.onNewDetectionEvent;
        }

        private void onNewDetectionEvent(DetectionEvent detectionEvent)
        {
            var _newLine = Get();
            _newLine.BindToEvent(detectionEvent);
        }
    }
}