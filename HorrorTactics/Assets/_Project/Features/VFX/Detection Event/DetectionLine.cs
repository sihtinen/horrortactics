using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

namespace HorrorTactics.VFX
{
    public class DetectionLine : MonoBehaviour
    {
        [Header("Object References")]
        [SerializeField] private LineRenderer m_lineRenderer = null;

        private DetectionEvent m_eventBinding = null;

        public void BindToEvent(DetectionEvent detectionEvent)
        {
            m_eventBinding = detectionEvent;
            m_eventBinding.OnDataUpdated += this.updateLine;
            m_eventBinding.OnClearMemory += this.clearAndReturnToPool;

            gameObject.SetActiveOptimized(true);
            WorldOverlayCamera.RegisterUser(this);
        }

        private void clearAndReturnToPool()
        {
            m_eventBinding.OnDataUpdated -= this.updateLine;
            m_eventBinding.OnClearMemory -= this.clearAndReturnToPool;
            m_eventBinding = null;

            gameObject.SetActiveOptimized(false);
            WorldOverlayCamera.UnregisterUser(this);

            DetectionLinePool.ReturnObject(this);
        }

        private void updateLine()
        {
            Vector3 _toTarget = m_eventBinding.Target.transform.position - m_eventBinding.Source.transform.position;
            Vector3 _detectionPos = m_eventBinding.Source.transform.position + _toTarget.normalized * m_eventBinding.DetectionDistance;

            m_lineRenderer.SetPosition(0, m_eventBinding.Source.transform.position);
            m_lineRenderer.SetPosition(1, _detectionPos);

            gameObject.SetActiveOptimized(true);
        }
    }
}