using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using MEC;
using ApplicationCore;

public enum AggressionState
{
    Unaware = 0,
    Alerted = 1,
    Hostile = 2,
    Fleeing = 3,
}

public class ThreatEvaluationComponent : MonoBehaviour
{
    public AggressionState AggressionState { get; private set; } = AggressionState.Unaware;
    public List<AICharacter> CurrentlyVisibleAICharacters { get; private set; } = new List<AICharacter>();
    public List<ControllableCharacter> CurrentlyVisibleControllableCharacters { get; private set; } = new List<ControllableCharacter>();
    public List<CharacterBase> HostileCharacters { get; private set; } = new List<CharacterBase>();
    public List<DetectionEvent> DetectionEvents { get; private set; } = new List<DetectionEvent>();

    public event Action<AggressionState> OnAggressionUpdated = null;

    [SerializeField] private float m_detectionIncreaseSpeed = 0.4f;
    [SerializeField] private float m_detectionDecreaseSpeed = 0.1f;
    [SerializeField] private float m_detectionDecreaseWaitTime = 2.0f;

    private AICharacter m_aiCharacterBinding = null;

    private void Awake()
    {
        m_aiCharacterBinding = GetComponent<AICharacter>();
    }

    private void Start()
    {
        m_aiCharacterBinding.LineOfSightComponent.OnTargetBecameVisible += this.onLOSTargetBecameVisible;
        m_aiCharacterBinding.LineOfSightComponent.OnTargetBecameInvisible += this.onLOSTargetBecameInvisible;

        TimingUtility.SubscribeToUpdate(this.mecUpdate, this);
    }

    private void mecUpdate()
    {
        for (int i = DetectionEvents.Count; i --> 0;)
        {
            var _event = DetectionEvents[i];

            if (_event.Target == null)
            {
                DetectionEvents.RemoveAt(i);
                ClassObjectPool<DetectionEvent>.ReturnToPool(_event);
                continue;
            }

            if (CurrentlyVisibleControllableCharacters.Contains(_event.Target))
            {
                _event.LastVisibleTimeStamp = Time.time;
                _event.DetectionDistance += Timing.DeltaTime * m_detectionIncreaseSpeed;
            }
            else if (Time.time - _event.LastVisibleTimeStamp >= m_detectionDecreaseWaitTime)
                _event.DetectionDistance -= Timing.DeltaTime * m_detectionDecreaseSpeed;

            Vector3 _diffToTarget = _event.Target.transform.position - transform.position;
            _event.DetectionDistance = Mathf.Min(_diffToTarget.magnitude, _event.DetectionDistance);
            _event.Update();

            if (_event.DetectionDistance >= _diffToTarget.magnitude)
                SetHostileTowardsCharacter(_event.Target);
            else if (_event.DetectionDistance <= 0)
            {
                DetectionEvents.RemoveAt(i);
                ClassObjectPool<DetectionEvent>.ReturnToPool(_event);
            }
        }
    }

    private void onLOSTargetBecameVisible(LineOfSightTarget target)
    {
        var _characterBaseComponent = target.transform.root.GetComponent<CharacterBase>();

        if (_characterBaseComponent == null)
            return;

        if (_characterBaseComponent is ControllableCharacter _controllableCharacter)
        {
            CurrentlyVisibleControllableCharacters.Add(_controllableCharacter);

            var _newDetectionEvent = DetectionEventHub.Instance.CreateNewDetectionEvent();
            _newDetectionEvent.Source = this;
            _newDetectionEvent.Target = _controllableCharacter;
            DetectionEvents.Add(_newDetectionEvent);
        }

        if (_characterBaseComponent is AICharacter _aiCharacter)
            CurrentlyVisibleAICharacters.Add(_aiCharacter);

        evaluateAgressionState();
    }

    private void onLOSTargetBecameInvisible(LineOfSightTarget target)
    {
        var _characterBaseComponent = target.transform.root.GetComponent<CharacterBase>();

        if (_characterBaseComponent == null)
            return;

        if (_characterBaseComponent is ControllableCharacter _controllableCharacter)
            CurrentlyVisibleControllableCharacters.Remove(_controllableCharacter);

        if (_characterBaseComponent is AICharacter _aiCharacter)
            CurrentlyVisibleAICharacters.Remove(_aiCharacter);

        evaluateAgressionState();
    }

    public void SetHostileTowardsCharacter(CharacterBase character)
    {
        if (HostileCharacters.Contains(character) == false)
            HostileCharacters.Add(character);

        switch (AggressionState)
        {
            case AggressionState.Unaware:
            case AggressionState.Alerted:
                updateAggressionState(AggressionState.Hostile);
                break;
        }
    }

    private void evaluateAgressionState()
    {
        switch (AggressionState)
        {
            case AggressionState.Unaware:
            case AggressionState.Alerted:

                if (HostileCharacters.Count > 0)
                    updateAggressionState(AggressionState.Hostile);

                break;

            case AggressionState.Hostile:
                break;
            case AggressionState.Fleeing:
                break;
        }
    }

    private void updateAggressionState(AggressionState newState)
    {
        if (AggressionState == newState)
            return;

        AggressionState = newState;
        OnAggressionUpdated?.Invoke(AggressionState);
    }
}
