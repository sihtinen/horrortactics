using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using MEC;

using ApplicationCore;

using Sami.AI;
using Sami.LineOfSight;

public class AICharacter : CharacterBase
{
    public VisibilityVFXComponent VisibilityVFX { get; private set; } = null;
    public ThreatEvaluationComponent ThreatComponent { get; private set; } = null;
    public AIIMapComponent IMapComponent { get; private set; } = null;
    public LineOfSightSource LineOfSightComponent { get; private set; } = null;

    [Header("Object References")]
    public AIState CurrentAIState = null;
    public WaypointCollection Waypoints = null;

    protected override void Awake()
    {
        base.Awake();

        VisibilityVFX = GetComponent<VisibilityVFXComponent>();
        ThreatComponent = GetComponent<ThreatEvaluationComponent>();
        IMapComponent = GetComponent<AIIMapComponent>();
        LineOfSightComponent = GetComponentInChildren<LineOfSightSource>();
    }

    private void Start()
    {
        Health.OnDeath += this.onDeath;
        SetPosition(transform.position.ToVector3Int());

        TimingUtility.SubscribeToUpdate(this.mecUpdate, this);

        if (CurrentAIState != null)
            CurrentAIState.OnStateEnter(this);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        if (CurrentAIState != null)
            CurrentAIState.OnStateExit(this);
    }

    private void mecUpdate()
    {
        if (CurrentAIState == null)
            return;

        AIState _newState = CurrentAIState.ValidateTransitions(this);

        if (_newState != null)
        {
            CurrentAIState.OnStateExit(this);
            CurrentAIState = _newState;
            CurrentAIState.OnStateEnter(this);
        }

        CurrentAIState.UpdateState(this, Timing.DeltaTime);
    }

    private void onDeath()
    {
        gameObject.SetActiveOptimized(false);
    }
}
