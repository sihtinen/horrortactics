using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Sami.IMaps;

public class AIIMapComponent : MonoBehaviour
{
    public IMap LineOfSightMap { get; private set; } = null;
    public IMap ReachablePositionsMap { get; private set; } = null;
    public IMap InvestigateMap { get; private set; } = null;

    [Header("Investigate Map Settings")]
    [SerializeField] private IMapGenerateInfluenceParameters m_investigateGenerationSettings = new IMapGenerateInfluenceParameters();
    [SerializeField, Min(0)] private float m_investigateMapDecayPerUpdate = 0.017f;

    [Header("Debug Settings")]
    [SerializeField] private bool m_drawLineOfSightMap = false;
    [SerializeField] private Color m_lineOfSightMapColor = Color.yellow;
    [Space]
    [SerializeField] private bool m_drawReachablePositionsMap = false;
    [SerializeField] private Color m_reachablePositionsMapColor = Color.yellow;
    [Space]
    [SerializeField] private bool m_drawInvestigateMap = false;
    [SerializeField] private Color m_investigateMapColor = Color.yellow;

    private AICharacter m_aiComponent = null;

    private void Awake()
    {
        m_aiComponent = GetComponent<AICharacter>();
    }

    private void Start()
    {
        LineOfSightMap = IMapPool.Instance.GetCopyOfBaseMap();
        ReachablePositionsMap = IMapPool.Instance.GetCopyOfBaseMap();
        InvestigateMap = IMapPool.Instance.GetCopyOfBaseMap();

        updateMaps();
    }

    private void updateMaps()
    {
        updateReachablePositionsMap();
        updateVisibilityMap();
    }

    private void onTurnStart()
    {
        applyDecayToInvestigateMap();
    }

    private void applyDecayToInvestigateMap()
    {
        if (InvestigateMap != null)
        {
            InvestigateMap.Subtract(m_investigateMapDecayPerUpdate);
            InvestigateMap.GateFilter(
                IMapGateFilter.Less,
                threshold: 0f,
                correctionValue: 0f);
        }
    }

    private void onCharacterEnteredVision(CharacterBase character)
    {
        if (character.Faction == m_aiComponent.Faction)
            return;

        applyDecayToInvestigateMap();

        InvestigateMap.GenerateInfluence(
            character.transform.position.ToVector3Int(),
            m_investigateGenerationSettings);

        InvestigateMap.GateFilter(
            IMapGateFilter.Greater,
            threshold: 1f,
            correctionValue: 1f);
    }

    private void updateVisibilityMap()
    {
        LineOfSightMap.SetToZero();
    }

    private void updateReachablePositionsMap()
    {
        //m_cachedVolumeData.Clear();

        //int _maxReachableDistance = m_aiComponent.MovementParameters.GetMaxMovementDistance(m_aiComponent.RemainingAP);

        //NavigationPathfinder.Instance.GenerateReachableNodes(
        //    m_aiComponent.MoveComponent.IntPosition,
        //    _maxReachableDistance,
        //    ref m_cachedVolumeData);

        //ReachablePositionsMap.SetToZero();

        //int _maxReachableDistanceSqr = _maxReachableDistance * _maxReachableDistance;

        //for (int i = 0; i < m_cachedVolumeData.Count; i++)
        //{
        //    var _reachableNode = m_cachedVolumeData[i];

        //    if (ReachablePositionsMap.NodePositionToListIndexMap.TryGetValue(_reachableNode.IntPosition, out int _listIndex))
        //    {
        //        float _sqrDistance = Vector3.SqrMagnitude(_reachableNode.IntPosition - m_aiComponent.MoveComponent.IntPosition);

        //        var _node = ReachablePositionsMap.NodeList[_listIndex];
        //        _node.NodeValue = 1f - (_sqrDistance / _maxReachableDistanceSqr);
        //        ReachablePositionsMap.NodeList[_listIndex] = _node;
        //    }
        //}

        //ReachablePositionsMap.Normalize();
    }

    private void OnDrawGizmos()
    {
        if (Application.isPlaying == false)
            return;

        if (m_drawLineOfSightMap)
            drawIMapGizmos(LineOfSightMap, m_lineOfSightMapColor);

        if (m_drawReachablePositionsMap)
            drawIMapGizmos(ReachablePositionsMap, m_reachablePositionsMapColor);

        if (m_drawInvestigateMap)
            drawIMapGizmos(InvestigateMap, m_investigateMapColor);
    }

    private void drawIMapGizmos(IMap map, Color color)
    {
        if (map == null)
            return;

        for (int i = 0; i < map.NodeList.Length; i++)
        {
            var _node = map.NodeList[i];

            if (_node.NodeValue > 0)
                _node.DrawGizmos(color * _node.NodeValue);
        }
    }
}
