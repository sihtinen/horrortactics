using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class DetectionEvent : Poolable
{
    public ThreatEvaluationComponent Source = null;
    public ControllableCharacter Target = null;
    public float DetectionDistance = 0;
    public float LastVisibleTimeStamp = 0;

    public event Action OnDataUpdated = null;
    public event Action OnClearMemory = null;

    public void Update()
    {
        OnDataUpdated?.Invoke();
    }

    public override void ClearMemory()
    {
        OnClearMemory?.Invoke();

        Source = null;
        Target = null;
        DetectionDistance = 0;
        LastVisibleTimeStamp = 0;
    }
}
