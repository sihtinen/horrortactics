using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public class DetectionEventHub : SingletonBehaviour<DetectionEventHub>
{
    public event Action<DetectionEvent> OnNewDetectionEvent = null;

    private ClassObjectPool<DetectionEvent> m_detectionEventPool = null;

    protected override void Awake()
    {
        base.Awake();

        m_detectionEventPool = new ClassObjectPool<DetectionEvent>();
        m_detectionEventPool.Initialize(poolSize: 32);
    }

    public DetectionEvent CreateNewDetectionEvent()
    {
        var _newEvent = m_detectionEventPool.GetT();
        OnNewDetectionEvent?.Invoke(_newEvent);
        return _newEvent;
    }
}
