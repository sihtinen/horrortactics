using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Sami.AI
{
    [System.Serializable]
    public abstract class AIStateTransition : ScriptableObject
    {
        public abstract (float, AIState) EvaluateTransition(AICharacter character);
    }

    [System.Serializable]
    public class AIStateTransitionReferenceWrapper
    {
        [Min(0)]
        public float ScoreMultiplier = 1;
        public AIStateTransition Transition = null;
    }
}