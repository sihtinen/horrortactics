using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Sami.AI.States
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "Patrol", menuName = "AI/States/Patrol")]
    public class PatrolState : AIState
    {
        private static Dictionary<int, Waypoint> m_waypointMemory = new Dictionary<int, Waypoint>();
        private static Dictionary<int, float> m_waitTimeMemory = new Dictionary<int, float>();

        public override void OnStateEnter(AICharacter character)
        {
            m_waypointMemory.Add(character.GetInstanceID(), character.Waypoints.Waypoints[0]);
            m_waitTimeMemory.Add(character.GetInstanceID(), 0);

            character.MoveComponent.SetMoveDestination(m_waypointMemory[character.GetInstanceID()].transform.position);
        }

        public override void OnStateExit(AICharacter character)
        {
            m_waypointMemory.Remove(character.GetInstanceID());
            m_waitTimeMemory.Remove(character.GetInstanceID());
        }

        public override void UpdateState(AICharacter character, float deltaTime)
        {
            if (character.MoveComponent.IsMoving == false)
            {
                m_waitTimeMemory[character.GetInstanceID()] -= deltaTime;

                if (m_waitTimeMemory[character.GetInstanceID()] <= 0)
                {
                    m_waitTimeMemory[character.GetInstanceID()] = 3;

                    Waypoint _nextWaypoint = m_waypointMemory[character.GetInstanceID()].To[0];
                    m_waypointMemory[character.GetInstanceID()] = _nextWaypoint;
                    character.MoveComponent.SetMoveDestination(_nextWaypoint.transform.position);
                }
            }
        }
    }
}
