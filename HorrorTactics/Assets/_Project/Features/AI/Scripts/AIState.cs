using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Sami.AI
{
    [System.Serializable]
    public abstract class AIState : ScriptableObject
    {
        [Header("AIState Base Settings")]
        [SerializeField] private List<AIStateTransitionReferenceWrapper> m_transitions = new List<AIStateTransitionReferenceWrapper>();

        public AIState ValidateTransitions(AICharacter character)
        {
            (float, AIState)? _bestTransition = null;

            for (int i = 0; i < m_transitions.Count; i++)
            {
                var _transitionWrapper = m_transitions[i];

                var _currentTransitionResult = _transitionWrapper.Transition.EvaluateTransition(character);
                _currentTransitionResult.Item1 *= _transitionWrapper.ScoreMultiplier;

                if (_currentTransitionResult.Item1 <= 0)
                    continue;

                if (_bestTransition.HasValue == false || _currentTransitionResult.Item1 > _bestTransition.Value.Item1)
                    _bestTransition = _currentTransitionResult;
            }

            if (_bestTransition.HasValue == false)
                return null;

            return _bestTransition.Value.Item2;
        }

        public abstract void OnStateEnter(AICharacter character);
        public abstract void OnStateExit(AICharacter character);
        public abstract void UpdateState(AICharacter character, float deltaTime);
    }
}
