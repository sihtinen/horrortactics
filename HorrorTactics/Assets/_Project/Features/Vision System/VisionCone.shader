﻿Shader "Sihtinen/VisionCone" 
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_DepthBias("Depth Bias", Range(0.0, 0.3)) = 0.01
		_DitherPattern("Dithering Pattern", 2D) = "white" {}
		_DitherScale("Dither Size", Float) = 1
		_DitherStrength("Dither Strength", Float) = 1
		_AlphaClip("Alpha Clip", Float) = 0.5

		[KeywordEnum(SemiVisible, Visible)]
		_AreaType("AreaType", Float) = 0
	}

	Subshader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "true"
			"RenderType" = "Transparent"
		}

		Pass
		{
			Stencil
			{
				Ref 12
				Comp Less
				Pass Replace
			}

			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			Cull Off
			Lighting Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct v2f
			{
				float4 pos : SV_POSITION;
				float3 ray : TEXCOORD1;
				float4 screenUV : TEXCOORD2;
			};

			sampler2D _ViewDepthTexture_Forward;
			sampler2D _ViewDepthTexture_Rotated;
			sampler2D _CameraDepthTexture;
			sampler2D _DitherPattern;

			float4x4 _ViewSpaceMatrix_Forward;
			float4x4 _ViewSpaceMatrix_Rotated;

			half4 _Color;
			half _ViewAngle;
			float _DepthBias;
			half _AreaType;
			half _DitherScale;
			half _DitherStrength;
			half _AlphaClip;
			half _SemiVisibleThreshold;
			half _VerticalSurfaceDotLimit;
			half _TargetHeightOffset_Low;
			half _TargetHeightOffset_High;

			v2f vert(appdata_base v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.ray = UnityObjectToViewPos(v.vertex) * half3(1, 1, -1);
				o.screenUV = ComputeScreenPos(o.pos);
				return o;
			}

			half getAngleAlpha(float2 objectPos)
			{
				half dotLimit = 1.0 - (_ViewAngle / 180);
				float2 objectPosNormalized = normalize(objectPos);
				half forwardDot = dot(half2(0, 1), objectPosNormalized);
				return forwardDot >= dotLimit;
			}

			half getObstacleAlpha(float4 worldPos, sampler2D depthTexture, float4x4 viewSpaceMatrix)
			{
				float4 posViewSpace = mul(viewSpaceMatrix, worldPos);
				float3 projCoords = posViewSpace.xyz / posViewSpace.w;	// ndc : -1 to 1
				projCoords = projCoords * 0.5 + 0.5; // 0 to 1
				
				if (projCoords.x < 0 || projCoords.x > 1 ||
					projCoords.y < 0 || projCoords.y > 1)
					return 0;

				float sampleDepth = 1.0f - SAMPLE_DEPTH_TEXTURE(depthTexture, projCoords.xy);
				float depthDiff = projCoords.z - sampleDepth;

				return depthDiff * 5000 <= _DepthBias;
			}

			half getObstacleAlphaTotal(float4 worldPos)
			{
				half _result_Forward = getObstacleAlpha(worldPos, _ViewDepthTexture_Forward, _ViewSpaceMatrix_Forward);
				half _result_Rotated = getObstacleAlpha(worldPos, _ViewDepthTexture_Rotated, _ViewSpaceMatrix_Rotated);
				return _result_Forward || _result_Rotated;
			}

			half4 frag(v2f i) : COLOR
			{
				float2 _testUV = i.screenUV.xy / i.screenUV.w;

				if (_testUV.x < 0 || _testUV.x > 1)
					discard;
				if (_testUV.y < 0 || _testUV.y > 1)
					discard;

				i.ray = i.ray * (_ProjectionParams.z / i.ray.z);  // farPlane / rayZ

				// 3D point reconstruction from depth texture
				float depth = SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, i.screenUV);
				depth = Linear01Depth(depth);

				float4 viewPos = float4(i.ray * depth, 1);
				float4 worldPos_Ground = mul(unity_CameraToWorld, viewPos);
				float4 worldNormalDDY = normalize(ddy(worldPos_Ground));

				// discard vertical surfaces
				if (dot(worldNormalDDY, half3(0, 1, 0)) > _VerticalSurfaceDotLimit)
					discard;

				// 0 - not visible, 1 - visible
				half isVisibleTop = getObstacleAlphaTotal(worldPos_Ground + half4(0, _TargetHeightOffset_High, 0, 0));
				half isVisibleBottom = getObstacleAlphaTotal(worldPos_Ground + half4(0, _TargetHeightOffset_Low, 0, 0));
				half isVisibleAny = isVisibleTop || isVisibleBottom;

				if (isVisibleAny < 1)
					discard;

				if (_AreaType == 1 && isVisibleBottom < 1)
					discard;

				float3 objectPos = mul(unity_WorldToObject, worldPos_Ground);
				//objectPos.y = 0;

				// Discard hit point if it is outside the box
				float3 clipTest = float3(0.5f, 0.5f, 0.5f) - abs(objectPos.xyz);
				if (clipTest.x < 0 || clipTest.y < 0 || clipTest.z < 0)
					discard;

				float oposDistance = length(objectPos.xz) * 2;

				if (_AreaType == 0 && isVisibleBottom > 0 && oposDistance <= _SemiVisibleThreshold)
					discard;

				if (_AreaType == 1 && oposDistance > _SemiVisibleThreshold)
					discard;

				float ditherValue = 1 - (tex2D(_DitherPattern, worldPos_Ground.xz * _DitherScale).r * _DitherStrength);

				// Alpha calculation
				float2 pos2D = objectPos.xz;
				float alpha = getAngleAlpha(pos2D) * ditherValue;

				half distFromCenter = length(pos2D);
				alpha *= distFromCenter <= 0.5 ? 1 : 0;

				if (alpha < _AlphaClip)
					discard;

				return saturate(half4(_Color.rgb, alpha * _Color.a));
			}

			ENDCG
		}
	}
}