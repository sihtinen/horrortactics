using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Sami.LineOfSight
{
    public class LineOfSightSource : MonoBehaviour
    {
        public static readonly float TargetHeightOffset_Low = 0.2f;
        public static readonly float TargetHeightOffset_High = 1.5f;

        public List<LineOfSightTarget> VisibleTargets { get; private set; } = new List<LineOfSightTarget>();

        public event Action<LineOfSightTarget> OnTargetBecameVisible = null;
        public event Action<LineOfSightTarget> OnTargetBecameInvisible = null;

        [Header("Line Of Sight Settings")]
        [Min(0)]
        public float ViewDistance = 20;
        [Min(0)]
        public float ViewAngle = 45;
        [Range(0, 1)]
        public float SemiVisibleThreshold = 0.7f;
        [Min(0)]
        public float EyeHeight = 2;
        public float CameraAngle_Forward = 5;
        public float CameraAngle_Rotated = 60;
        public LayerMask HitLayers;

        [Header("Object References")]
        [SerializeField] private Camera m_forwardVisionCamera = null;
        [SerializeField] private Camera m_rotatedVisionCamera = null;

        [Header("Debug Settings")]
        [SerializeField] private bool m_drawDebug = false;

        private static RaycastHit[] m_hits = new RaycastHit[10];

        private void Start()
        {
            MEC.TimingUtility.SubscribeToUpdate(this.mecFixedUpdate, this, MEC.Segment.FixedUpdate);
        }

        private void mecFixedUpdate()
        {
            CalculateVisibleTargets();
        }

        public void CalculateVisibleTargets()
        {
            var _allTargets = LineOfSightTargetCollection.AllTargets;

            for (int i = 0; i < _allTargets.Count; i++)
            {
                var _target = _allTargets[i];
                bool _isVisible = evaluateLineOfSight(_target);

                if (_isVisible)
                {
                    if (VisibleTargets.Contains(_target) == false)
                    {
                        VisibleTargets.Add(_target);
                        OnTargetBecameVisible?.Invoke(_target);
                    }
                }
                else
                {
                    if (VisibleTargets.Contains(_target))
                    {
                        VisibleTargets.Remove(_target);
                        OnTargetBecameInvisible?.Invoke(_target);
                    }
                }
            }
        }

        private bool evaluateLineOfSight(LineOfSightTarget target)
        {
            Vector3 _diff = target.transform.position - transform.position;

            if (_diff.magnitude > ViewDistance)
                return false;

            if (isPositionInsideCameraView(target.transform.position) == false)
                return false;

            Vector3 _eyePos = transform.position + new Vector3(0, EyeHeight, 0);

            bool _hit_Low = performLineOfSightRaycast(
                eyePosition: _eyePos,
                targetPosition: target.transform.position + new Vector3(0, TargetHeightOffset_Low, 0),
                target: target,
                viewDistance: ViewDistance * SemiVisibleThreshold);

            bool _hit_High = performLineOfSightRaycast(
                eyePosition: _eyePos,
                targetPosition: target.transform.position + new Vector3(0, TargetHeightOffset_High, 0),
                target: target,
                viewDistance: ViewDistance);

            return _hit_Low || _hit_High;
        }

        private bool performLineOfSightRaycast(Vector3 eyePosition, Vector3 targetPosition, LineOfSightTarget target, float viewDistance)
        {
            Vector3 _diff = targetPosition - eyePosition;

            int _hitCount = Physics.RaycastNonAlloc(
                eyePosition,
                _diff.normalized,
                m_hits,
                _diff.magnitude,
                HitLayers,
                QueryTriggerInteraction.Ignore);

            if (_hitCount == 0)
            {
                if (m_drawDebug)
                    Debug.DrawLine(eyePosition, targetPosition, Color.red, Time.deltaTime);

                return true;
            }

            return false;
        }

        private bool isPositionInsideCameraView(Vector3 position)
        {
            bool _visibleToForwardCam = true;
            bool _visibleToRotatedCam = true;

            m_forwardVisionCamera.transform.localPosition = new Vector3(0, EyeHeight, 0);
            m_forwardVisionCamera.farClipPlane = ViewDistance;
            m_forwardVisionCamera.fieldOfView = ViewAngle;
            m_forwardVisionCamera.transform.localEulerAngles = new Vector3(CameraAngle_Forward, 0, 0);

            Vector3 _viewPortPos = m_forwardVisionCamera.WorldToViewportPoint(position);

            if (_viewPortPos.z < 0 ||
                _viewPortPos.x < 0 || _viewPortPos.x > 1 ||
                _viewPortPos.y < 0 || _viewPortPos.y > 1)
                _visibleToForwardCam = false;

            m_rotatedVisionCamera.transform.localPosition = new Vector3(0, EyeHeight, 0);
            m_rotatedVisionCamera.farClipPlane = ViewDistance;
            m_rotatedVisionCamera.fieldOfView = ViewAngle;
            m_rotatedVisionCamera.transform.localEulerAngles = new Vector3(CameraAngle_Rotated, 0, 0);

            _viewPortPos = m_rotatedVisionCamera.WorldToViewportPoint(position);

            if (_viewPortPos.z < 0 ||
                _viewPortPos.x < 0 || _viewPortPos.x > 1 ||
                _viewPortPos.y < 0 || _viewPortPos.y > 1)
                _visibleToRotatedCam = false;

            return _visibleToForwardCam || _visibleToRotatedCam;
        }
    }
}