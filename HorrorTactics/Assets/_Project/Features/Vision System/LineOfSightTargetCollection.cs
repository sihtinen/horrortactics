using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public static class LineOfSightTargetCollection
{
    public static readonly List<LineOfSightTarget> AllTargets = new List<LineOfSightTarget>();

    public static void RegisterTarget(LineOfSightTarget target)
    {
        AllTargets.Add(target);
    }

    public static void UnregisterTarget(LineOfSightTarget target)
    {
        AllTargets.Remove(target);
    }
}
