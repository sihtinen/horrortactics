﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Sami.LineOfSight
{
    public class VisionConeRenderer : MonoBehaviour
    {
        [Header("Visual Settings")]
        [Range(0, 1)]
        public float SurfaceVerticalDotLimit = 0.45f;

        [Header("Object References")]
        [SerializeField] private LineOfSightSource m_losSource = null;
        [SerializeField] private Camera m_viewCamera_Forward = null;
        [SerializeField] private Camera m_viewCamera_Rotated = null;

        private Material[] m_materialInstances = null;

        private RenderTexture m_rt_Forward = null;
        private RenderTexture m_rt_Rotated = null;

        private MeshRenderer[] m_allLocalRenderers = null;
        private bool[] m_localRendererEnabledStates = null;

        private static readonly int m_shaderID_ViewDepthTexture_Forward = Shader.PropertyToID("_ViewDepthTexture_Forward");
        private static readonly int m_shaderID_ViewDepthTexture_Rotated = Shader.PropertyToID("_ViewDepthTexture_Rotated");
        private static readonly int m_shaderID_ViewSpaceMatrix_Forward = Shader.PropertyToID("_ViewSpaceMatrix_Forward");
        private static readonly int m_shaderID_ViewSpaceMatrix_Rotated = Shader.PropertyToID("_ViewSpaceMatrix_Rotated");
        private static readonly int m_shaderID_ViewAngle = Shader.PropertyToID("_ViewAngle");
        private static readonly int m_shaderID_SemiVisibleThreshold = Shader.PropertyToID("_SemiVisibleThreshold");
        private static readonly int m_shaderID_VerticalSurfaceDotLimit = Shader.PropertyToID("_VerticalSurfaceDotLimit");
        private static readonly int m_shaderID_TargetHeightOffset_Low = Shader.PropertyToID("_TargetHeightOffset_Low");
        private static readonly int m_shaderID_TargetHeightOffset_High = Shader.PropertyToID("_TargetHeightOffset_High");

        private static readonly Vector2Int m_renderResolution = new Vector2Int(400, 200);

        private void Start()
        {
            m_allLocalRenderers = transform.root.GetComponentsInChildren<MeshRenderer>();
            m_localRendererEnabledStates = new bool[m_allLocalRenderers.Length];

            MeshRenderer _renderer = GetComponent<MeshRenderer>();
            m_materialInstances = _renderer.materials;  // This generates a copy of the material
            _renderer.materials = m_materialInstances;

            m_rt_Forward = new RenderTexture(m_renderResolution.x, m_renderResolution.y, 16, RenderTextureFormat.Depth);
            m_rt_Rotated = new RenderTexture(m_renderResolution.x, m_renderResolution.y, 16, RenderTextureFormat.Depth);

            for (int i = 0; i < m_materialInstances.Length; i++)
            {
                m_materialInstances[i].SetTexture(m_shaderID_ViewDepthTexture_Forward, m_rt_Forward);
                m_materialInstances[i].SetTexture(m_shaderID_ViewDepthTexture_Rotated, m_rt_Rotated);
            }

            MEC.TimingUtility.SubscribeToUpdate(this.mecUpdate, this, MEC.Segment.Update);
        }

        private void mecUpdate()
        {
            onPreRender();

            m_viewCamera_Forward.transform.localPosition = new Vector3(0, m_losSource.EyeHeight, 0);
            m_viewCamera_Forward.farClipPlane = m_losSource.ViewDistance;
            m_viewCamera_Forward.fieldOfView = m_losSource.ViewAngle;
            m_viewCamera_Forward.transform.localEulerAngles = new Vector3(m_losSource.CameraAngle_Forward, 0, 0);
            m_viewCamera_Forward.targetTexture = m_rt_Forward;
            m_viewCamera_Forward.Render();

            Matrix4x4 _viewSpaceMatrix_Forward = m_viewCamera_Forward.projectionMatrix * m_viewCamera_Forward.worldToCameraMatrix;

            m_viewCamera_Rotated.transform.localPosition = new Vector3(0, m_losSource.EyeHeight, 0);
            m_viewCamera_Rotated.farClipPlane = m_losSource.ViewDistance;
            m_viewCamera_Rotated.fieldOfView = m_losSource.ViewAngle;
            m_viewCamera_Rotated.transform.localEulerAngles = new Vector3(m_losSource.CameraAngle_Rotated, 0, 0);
            m_viewCamera_Rotated.targetTexture = m_rt_Rotated;
            m_viewCamera_Rotated.Render();

            Matrix4x4 _viewSpaceMatrix_Rotated = m_viewCamera_Rotated.projectionMatrix * m_viewCamera_Rotated.worldToCameraMatrix;

            Vector3 _mainCameraPos = MainCameraComponent.Camera.transform.position;
            float _minLocalHeight = transform.InverseTransformPoint(_mainCameraPos).y - transform.localScale.y / 2;
            transform.localPosition = new Vector3(0, Mathf.Min(_minLocalHeight, 0), 0);
            transform.localScale = new Vector3(m_losSource.ViewDistance * 2, transform.localScale.y, m_losSource.ViewDistance * 2);

            for (int i = 0; i < m_materialInstances.Length; i++)
            {
                var _mat = m_materialInstances[i];

                _mat.SetMatrix(m_shaderID_ViewSpaceMatrix_Forward, _viewSpaceMatrix_Forward);
                _mat.SetMatrix(m_shaderID_ViewSpaceMatrix_Rotated, _viewSpaceMatrix_Rotated);
                _mat.SetFloat(m_shaderID_ViewAngle, m_losSource.ViewAngle);
                _mat.SetFloat(m_shaderID_SemiVisibleThreshold, m_losSource.SemiVisibleThreshold);
                _mat.SetFloat(m_shaderID_VerticalSurfaceDotLimit, SurfaceVerticalDotLimit);
                _mat.SetFloat(m_shaderID_TargetHeightOffset_Low, LineOfSightSource.TargetHeightOffset_Low);
                _mat.SetFloat(m_shaderID_TargetHeightOffset_High, LineOfSightSource.TargetHeightOffset_High);
            }

            onPostRender();
        }

        private void onPreRender()
        {
            for (int i = 0; i < m_allLocalRenderers.Length; i++)
            {
                m_localRendererEnabledStates[i] = m_allLocalRenderers[i].enabled;
                m_allLocalRenderers[i].enabled = false;
            }
        }

        private void onPostRender()
        {
            for (int i = 0; i < m_allLocalRenderers.Length; i++)
                m_allLocalRenderers[i].enabled = m_localRendererEnabledStates[i];
        }

#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            Gizmos.matrix = Matrix4x4.TRS(transform.position, Quaternion.identity, new Vector3(1f, 0f, 1f));
            Gizmos.DrawWireSphere(Vector3.zero, m_losSource.ViewDistance);
            Gizmos.matrix = Matrix4x4.identity;
        }
#endif
    }
}