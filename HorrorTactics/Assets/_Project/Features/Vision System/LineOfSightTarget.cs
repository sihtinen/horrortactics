using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class LineOfSightTarget : MonoBehaviour
{
    private void Start()
    {
        LineOfSightTargetCollection.RegisterTarget(this);
    }

    private void OnDestroy()
    {
        if (Application.isPlaying == false)
            return;

        LineOfSightTargetCollection.UnregisterTarget(this);
    }
}
