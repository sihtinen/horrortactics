using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public class UI_SurvivalTab : UI_TabBase
{
    [Header("Object References")]
    [SerializeField] private RectTransform m_verticalGroupParent = null;
    [SerializeField] private List<UI_EquipmentSlot> m_equipmentSlotUIList = new List<UI_EquipmentSlot>();

    private List<UI_EquipmentItemElement> m_activeEquipmentElements = new List<UI_EquipmentItemElement>();

    public void UpdateUI(InventoryComponent inventoryComponent)
    {
        CharacterInventoryComponent _characterInventory = inventoryComponent as CharacterInventoryComponent;

        if (_characterInventory != null)
        {
            for (int i = 0; i < _characterInventory.EquipmentSlots.Count; i++)
                m_equipmentSlotUIList[i].UpdateUI(_characterInventory.EquipmentSlots[i], inventoryComponent);
        }

        for (int i = 0; i < m_activeEquipmentElements.Count; i++)
            UI_EquipmentItemPool.ReturnObject(m_activeEquipmentElements[i]);

        m_activeEquipmentElements.Clear();

        var _allItems = inventoryComponent.InventoryData.GetAllItems();

        for (int i = 0; i < _allItems.Count; i++)
        {
            var _item = _allItems[i];

            if (_item.Category != ItemCategory.Survival_Equippable)
                continue;

            if (_characterInventory != null && _characterInventory.IsItemEquipped(_item))
                continue;

            if (_item is WeaponItem _weaponItem)
            {
                var _newElement = UI_EquipmentItemPool.Get();
                _newElement.transform.SetParent(m_verticalGroupParent);
                _newElement.BindToItem(_weaponItem, inventoryComponent);
                m_activeEquipmentElements.Add(_newElement);
            }
        }
    }
}