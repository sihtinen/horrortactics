using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class UI_InventoryAudioComponent : MonoBehaviour
{
    [Header("Audio Events")]
    [SerializeField] private Sami.Audio.FMODEventAsset m_audioOpened = null;
    [SerializeField] private Sami.Audio.FMODEventAsset m_audioClosed = null;

    private UI_InventoryScreen m_inventoryScreen = null;

    private void Awake()
    {
        m_inventoryScreen = GetComponent<UI_InventoryScreen>();
        m_inventoryScreen.OnOpened += this.onInventoryOpened;
        m_inventoryScreen.OnClosed += this.onInventoryClosed;
    }

    private void onInventoryOpened()
    {
        if (m_audioOpened)
            m_audioOpened.PlayAtPosition(Vector3.zero);
    }

    private void onInventoryClosed()
    {
        if (m_audioClosed)
            m_audioClosed.PlayAtPosition(Vector3.zero);
    }
}
