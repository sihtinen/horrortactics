using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[System.Serializable]
public class EquipmentSlot
{
    public string SlotDisplayName = "Slot 01 - Heavy";
    public EquipWeightType SlotWeightLimit = EquipWeightType.Heavy;
    public Inventory.InventoryItemWrapper EquippedItemWrapper = null;

    public bool IsEmpty => EquippedItemWrapper == null || EquippedItemWrapper.Item == null;
}
