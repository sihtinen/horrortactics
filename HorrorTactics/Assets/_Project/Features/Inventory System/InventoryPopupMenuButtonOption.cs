using System;

public struct InventoryPopupMenuButtonOption
{
    public string ButtonText;
    public Action<ControllableCharacter> OnClickedCallback;
}
