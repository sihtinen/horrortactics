using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public class UI_InventoryScreen : SingletonBehaviour<UI_InventoryScreen>
{
    public bool IsOpened { get; private set; } = false;

    public event Action OnOpened = null;
    public event Action OnClosed = null;

    [Header("Object References")]
    [SerializeField] private UI_SurvivalTab m_survivalTab = null;
    [SerializeField] private UI_KeyItemsTab m_keyItemsTab = null;
    [SerializeField] private UI_DocumentsTab m_documentsTab = null;

    private Canvas m_canvas = null;
    private InventoryComponent m_inventoryBinding = null;

    protected override void Awake()
    {
        base.Awake();
        m_canvas = GetComponent<Canvas>();
        CloseScreen();
    }

    private void OnDestroy()
    {
        if (Application.isPlaying == false)
            return;

        if (m_inventoryBinding != null)
        {
            m_inventoryBinding.InventoryData.OnInventoryUpdated -= this.onInventoryUpdated;

            if (m_inventoryBinding is CharacterInventoryComponent _characterInventory)
                _characterInventory.OnEquippedItemsUpdated -= this.onInventoryUpdated;
        }
    }

    public void BindToInventory(InventoryComponent inventoryComponent)
    {
        m_inventoryBinding = inventoryComponent;
        m_inventoryBinding.InventoryData.OnInventoryUpdated += this.onInventoryUpdated;

        if (m_inventoryBinding is CharacterInventoryComponent _characterInventory)
            _characterInventory.OnEquippedItemsUpdated += this.onInventoryUpdated;
    }

    private void onInventoryUpdated() => updateUI();

    private void updateUI()
    {
        m_survivalTab.UpdateUI(m_inventoryBinding);
        m_keyItemsTab.UpdateUI(m_inventoryBinding);
        m_documentsTab.UpdateUI(m_inventoryBinding);
    }

    public void OpenScreen()
    {
        updateUI();

        if (ScreenStateManager.Instance != null)
            ScreenStateManager.Instance.SetScreenState(ScreenStateType.Inventory);

        m_canvas.enabled = true;
        IsOpened = true;
        OnOpened?.Invoke();
    }

    public void CloseScreen()
    {
        m_canvas.enabled = false;
        IsOpened = false;
        OnClosed?.Invoke();

        if (ScreenStateManager.Instance != null)
            ScreenStateManager.Instance.SetScreenState(ScreenStateType.Gameplay);
    }
}