using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public class UI_InventoryPopupMenu : SingletonBehaviour<UI_InventoryPopupMenu>
{
    [Header("Object References")]
    [SerializeField] private RectTransform m_rootRectTransform = null;
    [SerializeField] private List<UI_InventoryPopupMenuButton> m_buttons = new List<UI_InventoryPopupMenuButton>();

    private bool m_popupActive = false;
    private bool m_openedThisFrame = false;

    private RectTransform m_rectTransform = null;

    protected override void Awake()
    {
        base.Awake();

        m_rectTransform = GetComponent<RectTransform>();
    }

    private void Start()
    {
        MEC.TimingUtility.SubscribeToUpdate(this.onUpdate, this, layer: 1);
        gameObject.SetActiveOptimized(false);
    }

    private void onUpdate()
    {
        if (m_popupActive == false)
            return;

        if (m_openedThisFrame == false && Input.GetMouseButtonDown(0) && isMouseOverPopupElement() == false)
            SetActive(false, Vector2.zero);

        m_openedThisFrame = false;
    }

    private bool isMouseOverPopupElement()
    {
        Vector2 _localMousePosition = m_rootRectTransform.InverseTransformPoint(Input.mousePosition);
        return m_rootRectTransform.rect.Contains(_localMousePosition);
    }

    public void ClearButtons()
    {
        for (int i = 0; i < m_buttons.Count; i++)
            m_buttons[i].gameObject.SetActiveOptimized(false);
    }

    public void BindToAction(string buttonText, Action onClickedCallback)
    {
        for (int i = 0; i < m_buttons.Count; i++)
        {
            var _button = m_buttons[i];

            if (_button.gameObject.activeSelf)
                continue;

            _button.Bind(buttonText, onClickedCallback);
            return;
        }
    }

    public void SetActive(bool isActive, Vector2 viewPortPos)
    {
        m_rectTransform.anchorMin = viewPortPos;
        m_rectTransform.anchorMax = viewPortPos;
        m_rectTransform.anchoredPosition = Vector2.zero;

        gameObject.SetActiveOptimized(isActive);
        m_popupActive = isActive;

        if (m_popupActive)
            m_openedThisFrame = true;
    }
}
