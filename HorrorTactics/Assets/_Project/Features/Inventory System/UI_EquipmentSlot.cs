using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

public class UI_EquipmentSlot : MonoBehaviour
{
    [Header("Object References")]
    [SerializeField] private GameObject m_slotEmptyIndicator = null;
    [SerializeField] private RectTransform m_contentRect = null;

    private UI_EquipmentItemElement m_itemElement = null;

    public void UpdateUI(EquipmentSlot slot, InventoryComponent inventory)
    {
        if (m_itemElement == null)
        {
            m_itemElement = UI_EquipmentItemPool.Get();
            var _itemRect = m_itemElement.transform as RectTransform;
            _itemRect.SetParent(m_contentRect);
            _itemRect.anchorMin = new Vector2(0, 0);
            _itemRect.anchorMax = new Vector2(1, 1);
            _itemRect.offsetMin = Vector2.zero;
            _itemRect.offsetMax = Vector2.zero;
            _itemRect.localPosition = Vector2.zero;
        }

        m_slotEmptyIndicator.SetActiveOptimized(slot.IsEmpty);

        var _weaponItem = slot.IsEmpty ? null : slot.EquippedItemWrapper?.Item as WeaponItem;
        m_itemElement.BindToItem(_weaponItem, inventory);
    }
}