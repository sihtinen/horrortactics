using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using TMPro;

using ApplicationCore;

public class UI_InventoryPopupMenuButton : MonoBehaviour
{
    [SerializeField] private TMP_Text m_buttonText = null;

    private Action m_onClickedCallback = null;

    public void Bind(string buttonText, Action onClickedCallback)
    {
        m_buttonText.SetText(buttonText);
        m_onClickedCallback = onClickedCallback;
        gameObject.SetActiveOptimized(true);
    }

    public void Button_Clicked()
    {
        UI_InventoryPopupMenu.Instance.SetActive(false, Vector2.zero);
        m_onClickedCallback?.Invoke();
    }
}
