using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class CharacterInventoryComponent : InventoryComponent
{
    public List<EquipmentSlot> EquipmentSlots = new List<EquipmentSlot>();

    public event Action OnEquippedItemsUpdated = null;

    public bool IsItemEquipped(ItemBase item)
    {
        for (int i = 0; i < EquipmentSlots.Count; i++)
        {
            var _slot = EquipmentSlots[i];

            if (_slot.EquippedItemWrapper != null &&
                _slot.EquippedItemWrapper.Item != null &&
                _slot.EquippedItemWrapper.Item.GUID == item.GUID)
                return true;
        }

        return false;
    }

    public void EquipItem(WeaponItem item, EquipmentSlot slot)
    {
        var _itemWrapper = this.InventoryData.GetItemWrapper(item);
        if (_itemWrapper == null)
        {
            Debug.LogError($"{GetType().Name}.EquipItem(): item wrapper for {item.DisplayName} is not found / null!");
            return;
        }

        slot.EquippedItemWrapper = _itemWrapper;
        OnEquippedItemsUpdated?.Invoke();
    }

    public void EquipItem(WeaponItem item)
    {
        var _itemWrapper = this.InventoryData.GetItemWrapper(item);
        if (_itemWrapper == null)
        {
            Debug.LogError($"{GetType().Name}.EquipItem(): item wrapper for {item.DisplayName} is not found / null!");
            return;
        }

        // first check for empty matching slots
        for (int i = 0; i < EquipmentSlots.Count; i++)
        {
            var _slot = EquipmentSlots[i];

            bool _isEmptySlot = _slot.EquippedItemWrapper == null || _slot.EquippedItemWrapper.Item == null;

            if (_isEmptySlot && _slot.SlotWeightLimit == item.EquipWeight)
            {
                _slot.EquippedItemWrapper = _itemWrapper;
                OnEquippedItemsUpdated?.Invoke();
                return;
            }
        }

        // check for lower tier slots
        if (item.EquipWeight == EquipWeightType.Light)
        {
            for (int i = 0; i < EquipmentSlots.Count; i++)
            {
                var _slot = EquipmentSlots[i];

                bool _isEmptySlot = _slot.EquippedItemWrapper == null || _slot.EquippedItemWrapper.Item == null;

                if (_isEmptySlot && _slot.SlotWeightLimit == EquipWeightType.Heavy)
                {
                    _slot.EquippedItemWrapper = _itemWrapper;
                    OnEquippedItemsUpdated?.Invoke();
                    return;
                }
            }
        }

        // finally override first matching slot
        for (int i = 0; i < EquipmentSlots.Count; i++)
        {
            var _slot = EquipmentSlots[i];

            if (_slot.SlotWeightLimit == item.EquipWeight)
            {
                _slot.EquippedItemWrapper = _itemWrapper;
                OnEquippedItemsUpdated?.Invoke();
                return;
            }
        }
    }

    public void UnequipItem(WeaponItem item)
    {
        for (int i = EquipmentSlots.Count; i --> 0;)
        {
            var _slot = EquipmentSlots[i];

            if (_slot.IsEmpty)
                continue;

            if (_slot.EquippedItemWrapper.Item.GUID == item.GUID)
            {
                UnequipItem(_slot.EquippedItemWrapper);
                return;
            }
        }
    }

    public void UnequipItem(Inventory.InventoryItemWrapper itemWrapper)
    {
        for (int i = 0; i < EquipmentSlots.Count; i++)
        {
            var _slot = EquipmentSlots[i];

            if (_slot.EquippedItemWrapper != null && _slot.EquippedItemWrapper == itemWrapper)
            {
                _slot.EquippedItemWrapper = null;
                OnEquippedItemsUpdated?.Invoke();
                return;
            }
        }
    }
}
