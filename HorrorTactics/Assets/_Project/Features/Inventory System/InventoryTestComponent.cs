using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class InventoryTestComponent : MonoBehaviour
{
    [SerializeField] private InventoryComponent m_inventoryComponent = null;

    private void Start()
    {
        UI_InventoryScreen.Instance.BindToInventory(m_inventoryComponent);
        UI_InventoryScreen.Instance.OpenScreen();
    }
}
