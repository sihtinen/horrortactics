using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class InventoryComponent : MonoBehaviour
{
    public Inventory InventoryData { get { return m_inventory; } }

    [Header("Object References")]
    [SerializeField] private Inventory m_inventory = new Inventory();
}
