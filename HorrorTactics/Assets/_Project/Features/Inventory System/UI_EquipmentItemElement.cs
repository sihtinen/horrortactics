using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using TMPro;

using ApplicationCore;

public class UI_EquipmentItemElement : MonoBehaviour
{
    [Header("Object References")]
    [SerializeField] private TMP_Text m_textItemName = null;
    [SerializeField] private TMP_Text m_textDescription = null;
    [SerializeField] private TMP_Text m_textWeightType = null;
    [SerializeField] private Image m_iconImage = null;

    private WeaponItem m_itemBinding = null;
    private InventoryComponent m_inventoryComponent = null;

    public void BindToItem(WeaponItem item, InventoryComponent inventoryComponent)
    {
        m_itemBinding = item;
        m_inventoryComponent = inventoryComponent;

        if (item == null)
        {
            gameObject.SetActiveOptimized(false);
            return;
        }

        m_textItemName.SetText(item.DisplayName);
        m_textDescription.SetText("- implement item asset descriptions -");
        m_textWeightType.SetText("- implement equipment weight types -");
        m_iconImage.overrideSprite = m_itemBinding.UIIcon;

        gameObject.SetActiveOptimized(true);
    }

    public void Button_Click()
    {
        if (m_inventoryComponent is CharacterInventoryComponent _characterInventory)
        {
            UI_InventoryPopupMenu.Instance.ClearButtons();

            if (_characterInventory.IsItemEquipped(m_itemBinding))
            {
                UI_InventoryPopupMenu.Instance.BindToAction(
                    buttonText: $"Unequip",
                    onClickedCallback: () => _characterInventory.UnequipItem(m_itemBinding));

                UI_InventoryPopupMenu.Instance.SetActive(true, Camera.main.ScreenToViewportPoint(Input.mousePosition));

                return;
            }

            for (int i = 0; i < _characterInventory.EquipmentSlots.Count; i++)
            {
                var _slot = _characterInventory.EquipmentSlots[i];

                if (m_itemBinding.EquipWeight == EquipWeightType.Heavy)
                {
                    if (_slot.SlotWeightLimit == EquipWeightType.Heavy)
                    {
                        UI_InventoryPopupMenu.Instance.BindToAction(
                            buttonText: $"Equip: {_slot.SlotDisplayName}",
                            onClickedCallback: () => _characterInventory.EquipItem(m_itemBinding, _slot));
                    }
                }
                else
                {
                    UI_InventoryPopupMenu.Instance.BindToAction(
                        buttonText: $"Equip: {_slot.SlotDisplayName}",
                        onClickedCallback: () => _characterInventory.EquipItem(m_itemBinding, _slot));
                }
            }

            UI_InventoryPopupMenu.Instance.SetActive(true, Camera.main.ScreenToViewportPoint(Input.mousePosition));
        }
    }
}
