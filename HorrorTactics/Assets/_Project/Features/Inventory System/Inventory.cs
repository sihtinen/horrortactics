using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[Serializable]
public class Inventory
{
    [Serializable]
    public class InventoryItemWrapper
    {
        public ItemBase Item = null;
        public int Amount = 0;

        public InventoryItemWrapper(ItemBase item, int amount = 1)
        {
            Item = item;
            Amount = amount;
        }
    }

    public ControllableCharacter CharacterOwner { get; set; } = null;
    public Action OnInventoryUpdated = null;

    public bool CanReceiveItemsFromOtherInventories = true;
    public List<InventoryItemWrapper> ItemWrappers = new List<InventoryItemWrapper>();

    private static List<ItemBase> m_cachedItemList = new List<ItemBase>();

    public void AddItem(ItemBase item, int amount = 1)
    {
        for (int i = 0; i < ItemWrappers.Count; i++)
        {
            if (ItemWrappers[i].Item.GUID == item.GUID)
            {
                ItemWrappers[i].Amount += amount;
                return;
            }
        }

        ItemWrappers.Add(new InventoryItemWrapper(item, amount));
    }

    public void RemoveItem(ItemBase item)
    {
        for (int i = ItemWrappers.Count; i --> 0;)
        {
            if (ItemWrappers[i].Item.GUID == item.GUID)
            {
                ItemWrappers.RemoveAt(i);
                return;
            }
        }
    }

    public bool IsEmpty() => ItemWrappers.Count == 0;

    public List<ItemBase> GetAllItems()
    {
        m_cachedItemList.Clear();

        for (int i = 0; i < ItemWrappers.Count; i++)
            m_cachedItemList.Add(ItemWrappers[i].Item);

        return m_cachedItemList;
    }

    public int GetAmountInInventory(ItemBase targetItem)
    {
        int _result = 0;

        for (int i = 0; i < ItemWrappers.Count; i++)
        {
            if (ItemWrappers[i].Item.GUID == targetItem.GUID)
                return _result++;
        }

        return _result;
    }

    public InventoryItemWrapper GetItemWrapper(ItemBase targetItem)
    {
        for (int i = 0; i < ItemWrappers.Count; i++)
        {
            if (ItemWrappers[i].Item.GUID == targetItem.GUID)
                return ItemWrappers[i];
        }

        return null;
    }
}
