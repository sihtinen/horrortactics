using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using FMOD.Studio;

namespace Sami.Audio
{
    [System.Serializable]
    [CreateAssetMenu(menuName = "Audio/New FMOD Event Asset", fileName = "New FMOD Event")]
    public class FMODEventAsset : ScriptableObject
    {
        public string FMODEventPath { get { return m_eventPath; } }

        [Header("FMOD Event Settings")]
        [SerializeField, FMODUnity.EventRef]
        private string m_eventPath = string.Empty;

        public void PlayOneShotAttached(GameObject attachedToObject)
        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(m_eventPath, attachedToObject);
        }

        public void PlayAtPosition(Vector3 position)
        {
            FMODUnity.RuntimeManager.PlayOneShot(m_eventPath, position);
        }

        public EventInstance CreateInstance()
        {
            return FMODUnity.RuntimeManager.CreateInstance(m_eventPath);
        }
    }
}