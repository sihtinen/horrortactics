using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float Speed = 3;

    private void Update()
    {
        transform.eulerAngles += new Vector3(0, Speed * Time.deltaTime, 0);
    }
}
