using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;
using MEC;

public class DebugSceneLoadComponent : MonoBehaviour
{
    [Header("Debug Settings")]
    [SerializeField] private KeyCode m_loadSceneKey = KeyCode.F1;

    private CoroutineHandle m_coroutine_Update;

    private void Start()
    {
        m_coroutine_Update = TimingUtility.SubscribeToUpdate(mecUpdate, this);
    }

    private void mecUpdate()
    {
        if (Input.GetKeyDown(m_loadSceneKey))
        {
            ApplicationSceneLoader.LoadScene(GameSceneDataAccessPoint.Instance.Data.SceneAssetReference);
        }
    }
}
