﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using TMPro;

using MEC;

namespace ApplicationCore
{
    public class UI_EasyTMPTextBinding : MonoBehaviour
    {
        [Header("Text Format Settings")]
        [SerializeField] private bool m_usePrefix = false;
        [SerializeField] private string m_prefix = string.Empty;
        [SerializeField] private bool m_useSuffix = false;
        [SerializeField] private string m_suffix = string.Empty;

        [Header("Animation Settings")]
        [SerializeField] private bool m_useScaleAnimation = false;
        [SerializeField] private float m_animationLenght = 1.0f;
        [SerializeField] private float m_maxAnimationScaleMultiplier = 1.0f;
        [SerializeField] private AnimationCurve m_animation = null;

        [Header("Gradual Increase Settings (for numeric values)")]
        [SerializeField] private bool m_useGradualIncreaseRoutine = false;
        [SerializeField] private float m_gradualIncreaseLenght = 1.0f;
        [SerializeField] private AnimationCurve m_gradualIncreaseCurve = null;

        private int m_currentValue_Int = 0;
        private float m_currentValue_Float = 0;
        private Vector3 m_startScale = Vector3.zero;

        private RectTransform m_rectTransform = null;
        private TMP_Text m_text = null;

        private CoroutineHandle m_coroutine_animation;
        private CoroutineHandle m_coroutine_gradualIncrease;

        private static StringBuilder m_stringBuilder = null;

        private void Awake()
        {
            m_rectTransform = GetComponent<RectTransform>();
            m_startScale = m_rectTransform.localScale;

            m_text = GetComponent<TMP_Text>();

            if (m_stringBuilder == null)
                m_stringBuilder = new StringBuilder(64);
        }

        public void SetText(float value)
        {
            if (m_useGradualIncreaseRoutine)
            {
                if (m_coroutine_gradualIncrease.IsRunning)
                    Timing.KillCoroutines(m_coroutine_gradualIncrease);

                m_coroutine_gradualIncrease = Timing.RunCoroutine(coroutine_gradualIncrease_Float(value));
            }
            else
                setText_Float(value);

            if (m_useScaleAnimation)
            {
                if (m_coroutine_animation != null) Timing.KillCoroutines(m_coroutine_animation);
                m_coroutine_animation = Timing.RunCoroutine(coroutine_scaleAnimation());
            }
        }

        public void SetText(int value)
        {
            if (m_useGradualIncreaseRoutine)
            {
                if (m_coroutine_gradualIncrease != null) Timing.KillCoroutines(m_coroutine_gradualIncrease);
                m_coroutine_gradualIncrease = Timing.RunCoroutine(coroutine_gradualIncrease_Int(value));
            }
            else
                setText_Int(value);

            if (m_useScaleAnimation)
            {
                if (m_coroutine_animation != null) Timing.KillCoroutines(m_coroutine_animation);
                m_coroutine_animation = Timing.RunCoroutine(coroutine_scaleAnimation());
            }
        }

        private void setText_Int(int value)
        {
            m_stringBuilder.Clear();

            if (m_usePrefix)
                m_stringBuilder.Append(m_prefix);

            m_stringBuilder.Append(value.ToStringMinimalAlloc());

            if (m_useSuffix)
                m_stringBuilder.Append(m_suffix);

            m_text.SetText(m_stringBuilder.ToString());

            m_currentValue_Int = value;
            m_currentValue_Float = value;
        }

        private void setText_Float(float value)
        {
            m_stringBuilder.Clear();

            if (m_usePrefix)
                m_stringBuilder.Append(m_prefix);

            m_stringBuilder.Append(Math.Round(value, 2).ToStringMinimalAlloc());

            if (m_useSuffix)
                m_stringBuilder.Append(m_suffix);

            m_text.SetText(m_stringBuilder.ToString());

            m_currentValue_Int = (int)value;
            m_currentValue_Float = value;
        }

        private IEnumerator<float> coroutine_gradualIncrease_Int(int targetValue)
        {
            float _timer = 0.0f;
            int _startValue = m_currentValue_Int;

            while (_timer < m_gradualIncreaseLenght)
            {
                _timer += Time.unscaledDeltaTime;

                int _currentValue = (int)Mathf.Lerp(_startValue, targetValue, m_gradualIncreaseCurve.Evaluate(_timer / m_gradualIncreaseLenght));
                setText_Int(_currentValue);

                yield return Timing.WaitForOneFrame;
            }

            setText_Int(targetValue);
        }

        private IEnumerator<float> coroutine_gradualIncrease_Float(float targetValue)
        {
            float _timer = 0.0f;
            float _startValue = m_currentValue_Float;

            while (_timer < m_gradualIncreaseLenght)
            {
                _timer += Time.unscaledDeltaTime;

                float _currentValue = Mathf.Lerp(_startValue, targetValue, m_gradualIncreaseCurve.Evaluate(_timer / m_gradualIncreaseLenght));
                setText_Float(_currentValue);

                yield return Timing.WaitForOneFrame;
            }

            setText_Float(targetValue);
        }

        private IEnumerator<float> coroutine_scaleAnimation()
        {
            float _timer = 0.0f;

            while (_timer < m_animationLenght)
            {
                _timer += Time.unscaledDeltaTime;

                Vector3 _newScale = Vector3.Lerp(
                    m_startScale, 
                    m_startScale * m_maxAnimationScaleMultiplier, 
                    m_animation.Evaluate(_timer / m_animationLenght));

                m_rectTransform.localScale = _newScale;

                yield return Timing.WaitForOneFrame;
            }

            m_rectTransform.localScale = m_startScale;
        }
    }
}
