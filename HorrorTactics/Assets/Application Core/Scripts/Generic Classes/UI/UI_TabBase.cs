﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ApplicationCore
{
    public class UI_TabBase : MonoBehaviour
    {
        [Header("Button Settings")]
        [SerializeField] private Button m_tabButton = null;

        [Header("Events")]
        [SerializeField] private UnityEvent m_onTabOpened = null;
        [SerializeField] private UnityEvent m_onTabClosed = null;

        public void InitializeTab()
        {
            onInitializeTab();
        }

        public void UpdateTab(float deltaTime)
        {
            updateTab(deltaTime);
        }

        public void OpenTab()
        {
            onTabOpened();

            if (m_tabButton)
                m_tabButton.interactable = false;

            gameObject.SetActiveOptimized(true);

            m_onTabOpened.Invoke();
        }

        public void CloseTab()
        {
            onTabClosed();

            if (m_tabButton)
                m_tabButton.interactable = true;

            gameObject.SetActiveOptimized(false);

            m_onTabClosed.Invoke();
        }

        protected virtual void onInitializeTab() { }
        protected virtual void updateTab(float deltaTime) { }
        protected virtual void onTabOpened() { }
        protected virtual void onTabClosed() { }
    }
}
