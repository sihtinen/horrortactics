using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using ApplicationCore;

namespace ApplicationCore.UI
{
    [RequireComponent(typeof(RectTransform))]
    public class UI_RectTransformNoiseAnimator : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private List<RotationModifier> m_rotationModifiers = new List<RotationModifier>();
        [SerializeField] private List<PositionModifier> m_positionModifiers = new List<PositionModifier>();

        private RectTransform m_rectTransform = null;

        private Vector2 m_defaultAnchoredPos = Vector2.zero;
        private Vector3 m_defaultEulerAngles = Vector3.zero;

        private float 
            m_randomPosOffsetX, m_randomPosOffsetY,
            m_randomRotOffsetX, m_randomRotOffsetY;

        private void Awake()
        {
            m_rectTransform = transform as RectTransform;

            m_defaultAnchoredPos = m_rectTransform.anchoredPosition;
            m_defaultEulerAngles = m_rectTransform.localEulerAngles;

            m_randomPosOffsetX = Random.Range(0f, 1f);
            m_randomPosOffsetY = Random.Range(0f, 1f);
            m_randomRotOffsetX = Random.Range(0f, 1f);
            m_randomRotOffsetY = Random.Range(0f, 1f);
        }

        private void Start()
        {
            MEC.TimingUtility.SubscribeToUpdate(mecUpdate, this);
        }

        private void mecUpdate()
        {
            Vector3 _localEuler = m_defaultEulerAngles;

            for (int i = 0; i < m_rotationModifiers.Count; i++)
            {
                _localEuler.z += m_rotationModifiers[i].GetRotationOffset(new Vector2(
                    Time.unscaledTime + m_randomRotOffsetX, 
                    Time.unscaledTime + m_randomRotOffsetY));
            }

            m_rectTransform.localEulerAngles = _localEuler;

            Vector2 _anchoredPos = m_defaultAnchoredPos;

            for (int i = 0; i < m_positionModifiers.Count; i++)
            {
                _anchoredPos += m_positionModifiers[i].GetPositionOffset(new Vector2(
                    Time.unscaledTime + m_randomPosOffsetX,
                    Time.unscaledTime + m_randomPosOffsetY));
            }

            m_rectTransform.anchoredPosition = _anchoredPos;
        }

        [System.Serializable]
        public class RotationModifier
        {
            public float Speed = 0;
            public float Amount = 0;

            public float GetRotationOffset(Vector2 samplePosition)
            {
                float _noise = -0.5f + Mathf.PerlinNoise(
                    samplePosition.x * Speed,
                    samplePosition.y * Speed);

                return _noise * Amount;
            }
        }

        [System.Serializable]
        public class PositionModifier
        {
            public float SpeedX = 0;
            public float SpeedY = 0;

            public float AmountX = 0;
            public float AmountY = 0;

            public Vector2 GetPositionOffset(Vector2 samplePosition)
            {
                float _noiseX = -0.5f + Mathf.PerlinNoise(
                    samplePosition.x * SpeedX,
                    samplePosition.y * SpeedX);

                float _noiseY = -0.5f + Mathf.PerlinNoise(
                    samplePosition.x * SpeedY,
                    samplePosition.y * SpeedY);

                return new Vector2(
                    _noiseX * AmountX,
                    _noiseY * AmountY);
            }
        }
    }
}
