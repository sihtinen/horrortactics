﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ApplicationCore
{
    public class UI_TabManager : MonoBehaviour
    {
        [Header("Tab Manager Settings")]
        [SerializeField] private List<UI_TabBase> m_tabs = null;

        private UI_TabBase m_activeTab = null;

        private void Start()
        {
            for (int i = 0; i < m_tabs.Count; i++)
            {
                m_tabs[i].InitializeTab();
                m_tabs[i].CloseTab();
            }

            OpenTab(0);

            MEC.TimingUtility.SubscribeToUpdate(mecUpdate, this);
        }

        private void mecUpdate()
        {
            if (m_activeTab)
                m_activeTab.UpdateTab(Time.deltaTime);
        }

        public void OpenTab(UI_TabBase tab)
        {
            if (tab == m_activeTab)
                return;

            if (m_tabs.Contains(tab))
            {
                if (m_activeTab)
                    m_activeTab.CloseTab();

                m_activeTab = tab;
                m_activeTab.OpenTab();
            }
        }

        public void OpenTab(int tabIndex)
        {
            if (tabIndex < 0 || m_tabs == null || tabIndex >= m_tabs.Count)
                return;

            if (m_activeTab && m_tabs.IndexOf(m_activeTab) == tabIndex)
                return;

            if (m_activeTab)
                m_activeTab.CloseTab();

            m_activeTab = m_tabs[tabIndex];
            m_activeTab.OpenTab();
        }
    }
}
