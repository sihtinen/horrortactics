using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace ApplicationCore.UI
{
    public class UI_PointerClickHandler : MonoBehaviour, IPointerClickHandler
    {
        [Header("Unity Events")]
        public UnityEvent OnMouseClick_Left = new UnityEvent();
        public UnityEvent OnMouseClick_Right = new UnityEvent();
        public UnityEvent OnMouseClick_Middle = new UnityEvent();

        public void OnPointerClick(PointerEventData eventData)
        {
            switch (eventData.button)
            {
                case PointerEventData.InputButton.Left:
                    OnMouseClick_Left.Invoke();
                    break;
                case PointerEventData.InputButton.Right:
                    OnMouseClick_Right.Invoke();
                    break;
                case PointerEventData.InputButton.Middle:
                    OnMouseClick_Middle.Invoke();
                    break;
            }
        }
    }
}
