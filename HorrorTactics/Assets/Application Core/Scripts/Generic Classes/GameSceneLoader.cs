﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AddressableAssets;

using ApplicationCore;

public class GameSceneLoader : MonoBehaviour
{
    [Header("Object References")]
    [SerializeField] private AssetReference m_sceneAsset = null;

    public void LoadScene()
    {
        ApplicationSceneLoader.LoadScene(m_sceneAsset);
    }
}
