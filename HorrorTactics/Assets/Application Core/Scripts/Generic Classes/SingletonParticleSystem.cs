﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ApplicationCore
{
    public abstract class SingletonParticleSystem<T> : SingletonBehaviour<SingletonParticleSystem<T>> where T : MonoBehaviour
    {
        private ParticleSystem m_particles = null;
        protected ParticleSystem.EmitParams EmitParams;

        protected override void Awake()
        {
            base.Awake();

            m_particles = GetComponent<ParticleSystem>();
            EmitParams = new ParticleSystem.EmitParams();
            EmitParams.applyShapeToPosition = true;
        }

        public static void PlayAtPosition(Vector3 position, Vector3 velocity, float spreadAmount = 1.0f)
        {
            Instance.EmitParams.position = position;
            int _particleCount = Instance.applyNewEmitParameters();

            for (int i = 0; i < _particleCount; i++)
            {
                Instance.EmitParams.velocity = 
                    velocity * Random.Range(1.0f, 1.5f) + 
                    Vector3.right * Random.Range(-1f, 1f) * spreadAmount + 
                    Vector3.up * Random.Range(-1f, 1f) * spreadAmount;

                Instance.m_particles.Emit(Instance.EmitParams, 1);
            }
        }

        protected abstract int applyNewEmitParameters();
    }
}
