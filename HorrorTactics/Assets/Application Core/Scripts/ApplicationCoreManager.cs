﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;

using MEC;

namespace ApplicationCore
{
    public class ApplicationCoreManager : MonoBehaviour
    {
        public static bool IsInitialized { get { return m_instance != null && m_instance.m_isInitializing == false; } }
        public static bool IsGamePaused { get { return m_instance.m_isPaused; } }
        public static bool IsGameLoading { get { return m_instance.m_isLoading; } }
        public static bool IsGamePausedOrLoading 
        { 
            get 
            {
                if (m_instance == null)
                    return true;

                return IsGamePaused || IsGameLoading; 
            } 
        }

        public static Action OnSceneLoadStarted = null;
        public static Action OnActiveSceneUnloaded = null;
        public static Action OnSceneLoadCompleted = null;

        [Header("Scene Settings")]
        [SerializeField] private AssetReference m_defaultGameScene = null;

        [Header("Object References")]
        [SerializeField] private ApplicationSceneLoader m_sceneLoader = null;
        [SerializeField] private ApplicationSettingsManager m_settingsManager = null;
        [SerializeField] private ApplicationGameTimeManager m_gameTimeManager = null;
        [SerializeField] private SaveSystem.ApplicationSaveManager m_saveManager = null;

        [Header("Runtime Values")]
        [SerializeField] private bool m_isInitializing = true;
        [SerializeField] private bool m_isPaused = true;
        [SerializeField] private bool m_isLoading = true;

        [Header("Unity Events")]
        public UnityEvent OnCoreInitialized = null;

        private CoroutineHandle m_coroutine_Update;
        private CoroutineHandle m_coroutine_FixedUpdate;
        private CoroutineHandle m_coroutine_ExitGame;

        private static ApplicationCoreManager m_instance = null;

        public void Initialize(Scene activeScene, AssetReference sceneToLoad)
        {
            m_instance = this;

            OnSceneLoadStarted += delegate
            {
                m_isLoading = true;
            };
            OnSceneLoadCompleted += delegate
            {
                m_isLoading = false;
                m_isPaused = false;
            };

            m_instance.preInitializationSettings();
            m_sceneLoader.Initialize(activeScene);

            m_settingsManager.Initialize();
            m_gameTimeManager.Initialize();
            m_saveManager.Initialize();

            Debug.Log("ApplicationCoreManager: initialization completed - starting the application");

            m_isInitializing = false;

            OnCoreInitialized?.Invoke();

            if (sceneToLoad == null)
                sceneToLoad = m_defaultGameScene;

            ApplicationSceneLoader.LoadScene(sceneToLoad);

            m_coroutine_Update = TimingUtility.SubscribeToUpdate(
                callback: mecUpdate, 
                sourceMonoBehaviour: this, 
                segment: Segment.Update,
                layer: 1);

            m_coroutine_FixedUpdate = TimingUtility.SubscribeToUpdate(
                callback: mecFixedUpdate, 
                sourceMonoBehaviour: this, 
                segment: Segment.FixedUpdate,
                layer: 1);
        }

        public static void SetGamePaused(bool isPaused)
        {
            m_instance.m_isPaused = isPaused;
        }

        public static void ExitGame()
        {
            if (m_instance.m_coroutine_ExitGame.IsRunning == false)
                m_instance.m_coroutine_ExitGame = Timing.RunCoroutine(m_instance.coroutine_ExitGame(), layer: 1);
        }

        private void mecUpdate()
        {
            if (m_isLoading)
                return;

            m_gameTimeManager.ManualUpdate();
        }

        private void mecFixedUpdate()
        {
            if (m_isLoading || m_isPaused)
            {
                if (Physics.autoSimulation)
                    Physics.autoSimulation = false;

                return;
            }

            if (Physics.autoSimulation == false)
                Physics.autoSimulation = true;
        }

        private IEnumerator<float> coroutine_ExitGame()
        {
            yield return Timing.WaitUntilDone(UI_BlackScreenCanvas.Coroutine_FadeIn(0.25f));

#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
            Application.OpenURL(webplayerQuitURL);
#else
            Application.Quit();
#endif
        }

        private void preInitializationSettings()
        {
            if (Debug.isDebugBuild == false)
                Debug.unityLogger.logEnabled = false;
        }
    }
}