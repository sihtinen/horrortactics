﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using MEC;

namespace ApplicationCore
{
    public class UI_BlackScreenCanvas : MonoBehaviour
    {
        [Header("Animation Curves")]
        [SerializeField] private AnimationCurve m_curve_FadeIn = null;
        [SerializeField] private AnimationCurve m_curve_FadeOut = null;

        [Header("Object References")]
        [SerializeField] private Canvas m_canvas = null;
        [SerializeField] private CanvasGroup m_canvasGroup = null;

        private static UI_BlackScreenCanvas m_instance = null;
        private CoroutineHandle m_currentCoroutine;

        public void Initialize()
        {
            m_instance = this;
        }

        public static void FadeIn(float time)
        {
            if (m_instance.m_currentCoroutine.IsRunning)
                Timing.KillCoroutines(m_instance.m_currentCoroutine);

            m_instance.m_currentCoroutine = Timing.RunCoroutine(Coroutine_FadeIn(time));
        }

        public static void FadeOut(float time)
        {
            if (m_instance.m_currentCoroutine.IsRunning)
                Timing.KillCoroutines(m_instance.m_currentCoroutine);

            m_instance.m_currentCoroutine = Timing.RunCoroutine(Coroutine_FadeOut(time));
        }

        public static IEnumerator<float> Coroutine_FadeIn(float time)
        {
            m_instance.m_canvasGroup.alpha = 0.0f;
            m_instance.m_canvas.enabled = true;

            float _timer = 0.0f;
            while (_timer < time)
            {
                _timer += Time.unscaledDeltaTime;
                m_instance.m_canvasGroup.alpha = m_instance.m_curve_FadeIn.Evaluate((_timer / time));
                yield return Timing.WaitForOneFrame;
            }
        }

        public static IEnumerator<float> Coroutine_FadeOut(float time)
        {
            m_instance.m_canvasGroup.alpha = 1.0f;
            m_instance.m_canvas.enabled = true;

            float _timer = 0.0f;
            while (_timer < time)
            {
                _timer += Time.unscaledDeltaTime;
                m_instance.m_canvasGroup.alpha = m_instance.m_curve_FadeOut.Evaluate(_timer / time).OneMinus();
                yield return Timing.WaitForOneFrame;
            }

            m_instance.m_canvas.enabled = false;
        }
    }
}