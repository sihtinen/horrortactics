﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Rendering;

namespace ApplicationCore
{
    public enum FramerateTarget
    {
        FPS_30,
        FPS_60,
        FPS_144
    }

    public enum GraphicsQualityPreset
    {
        Quality,
        Performance,
        Desktop
    }

    public class ApplicationSettingsManager : MonoBehaviour
    {
        public static bool PostProcessingEnabled = true;
        public static bool AAEnabled = true;
        public static GraphicsQualityPreset CurrentPreset { get { return m_instance.m_currentPreset; } }

        private static ApplicationSettingsManager m_instance = null;

        private GraphicsQualityPreset m_currentPreset = GraphicsQualityPreset.Desktop;

        public void Initialize()
        {
            m_instance = this;

            QualitySettings.maxQueuedFrames = 1;
            ApplyQualityPreset(m_currentPreset);

            ApplicationCoreManager.OnSceneLoadCompleted += this.onSceneLoadCompleted;
        }

        private void onSceneLoadCompleted()
        {
            ApplyQualityPreset(m_currentPreset);
        }

        public static void ApplyQualityPreset(GraphicsQualityPreset preset)
        {
            m_instance.m_currentPreset = preset;

            switch (m_instance.m_currentPreset)
            {
                case GraphicsQualityPreset.Quality:

                    SetFramerateTarget(FramerateTarget.FPS_30);

                    PostProcessingEnabled = true;
                    AAEnabled = true;

                    break;

                case GraphicsQualityPreset.Performance:

                    SetFramerateTarget(FramerateTarget.FPS_60);

                    PostProcessingEnabled = false;
                    AAEnabled = false;

                    break;

                case GraphicsQualityPreset.Desktop:

                    SetFramerateTarget(FramerateTarget.FPS_144);

                    PostProcessingEnabled = true;
                    AAEnabled = true;

                    break;
            }
        }

        public static void SetFramerateTarget(FramerateTarget target)
        {
            QualitySettings.vSyncCount = 0;

            switch (target)
            {
                case FramerateTarget.FPS_30:
                    Application.targetFrameRate = 30;
                    break;
                case FramerateTarget.FPS_60:
                    Application.targetFrameRate = 60;
                    break;
                case FramerateTarget.FPS_144:
                    Application.targetFrameRate = 144;
                    break;
            }
        }

        public static bool ShadowsEnabled()
        {
            switch (m_instance.m_currentPreset)
            {
                case GraphicsQualityPreset.Performance:
                    return false;

                default: 
                    return true;
            }
        }
    }
}