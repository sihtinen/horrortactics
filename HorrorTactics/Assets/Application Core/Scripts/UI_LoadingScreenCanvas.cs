﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Unity.Mathematics;

using TMPro;

namespace ApplicationCore
{
    public class UI_LoadingScreenCanvas : MonoBehaviour
    {
        private static UI_LoadingScreenCanvas m_instance = null;

        [Header("Object Reference")]
        [SerializeField] private Slider m_loadingSlider = null;
        [SerializeField] private TMP_Text m_textLoadingPercentage = null;

        private void Awake()
        {
            m_instance = this;
        }

        public void SetActive(bool active)
        {
            if (active == false) SetLoadingAmount(0.0f);

            if (gameObject.activeSelf != active)
                gameObject.SetActive(active);
        }

        public static void SetLoadingAmountStatic(float amount)
        {
            m_instance.SetLoadingAmount(amount);
        }

        public void SetLoadingAmount(float amount)
        {
            m_loadingSlider.SetValueWithoutNotify(amount);
            m_textLoadingPercentage.SetText($"{math.round(amount * 100)}%");
        }
    }
}
