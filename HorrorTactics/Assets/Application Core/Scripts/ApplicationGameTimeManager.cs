﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ApplicationCore
{
    public class ApplicationGameTimeManager : MonoBehaviour
    {
        private static ApplicationGameTimeManager m_instance = null;

        private bool m_hitStopActive = false;

        private int m_hitStopPriority = 0;

        private float m_hitStopRemaining = 0.0f;
        private float m_maximumHitStopTime = 0.0f;
        private float m_minimumTimeScale = 0.0f;

        public void Initialize()
        {
            m_instance = this;

            Time.fixedDeltaTime = 1.0f / 45;
            Time.maximumDeltaTime = 1.0f / 20;
            Time.maximumParticleDeltaTime = 1.0f / 20;
        }

        public void ManualUpdate()
        {
            if (ApplicationCoreManager.IsGamePaused)
            {
                Time.timeScale = 0.0f;
                return;
            }

            float _timeScale = 1.0f;

            if (m_hitStopActive)
            {
                m_hitStopRemaining -= Time.unscaledDeltaTime;

                if (m_hitStopRemaining <= 0.0f)
                {
                    _timeScale = 1.0f;
                    m_hitStopActive = false;
                }
                else
                {
                    float _normalized = m_hitStopRemaining / m_maximumHitStopTime;

                    _timeScale = Mathf.Lerp(
                        1.0f,
                        m_minimumTimeScale,
                        Mathf.Pow(_normalized, 3));
                }
            }

            if (Time.timeScale != _timeScale) Time.timeScale = _timeScale;
        }

        public static void HitStop(float time, float minimumTimeScale = 0.05f, int priority = 0)
        {
            if (m_instance.m_hitStopActive && priority < m_instance.m_hitStopPriority)
            {
                return;
            }

            m_instance.m_hitStopActive = true;
            m_instance.m_hitStopRemaining = time;
            m_instance.m_maximumHitStopTime = time;
            m_instance.m_minimumTimeScale = minimumTimeScale;
            m_instance.m_hitStopPriority = priority;
        }
    }
}

