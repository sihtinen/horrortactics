﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace ApplicationCore
{
    public class UI_VirtualButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
    {
        [Header("Input Events")]
        [SerializeField] private UnityEvent m_onPressed = null;
        [SerializeField] private UnityEvent m_onReleased = null;

        private bool m_pressed = false;

        public void OnPointerDown(PointerEventData eventData)
        {
            m_pressed = true;
            m_onPressed.Invoke();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            m_pressed = false;
            m_onReleased.Invoke();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (m_pressed)
            {
                OnPointerUp(null);
            }
        }
    }
}
