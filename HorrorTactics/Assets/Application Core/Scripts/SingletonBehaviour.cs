using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ApplicationCore
{
    public abstract class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        [Header("Singleton Settings")]
        [SerializeField] private bool m_isPersistentObject = false;

        public static T Instance { get; private set; } = null;

        protected virtual void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this as T;

            if (m_isPersistentObject)
            {
                transform.SetParent(null);
                DontDestroyOnLoad(gameObject);
            }
        }
    }
}