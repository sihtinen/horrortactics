﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;

using MEC;

namespace ApplicationCore
{
    public class ApplicationSceneLoader : MonoBehaviour
    {
        public static bool IsLoading()
        {
            return m_instance.m_currentCoroutine != null;
        }

        [Header("Object References")]
        [SerializeField] private UI_BlackScreenCanvas m_blackScreenCanvas = null;
        [SerializeField] private UI_LoadingScreenCanvas m_loadScreenCanvas = null;

        [Header("Debug")]
        [SerializeField] private bool m_logDebugMessages = false;

        private static ApplicationSceneLoader m_instance = null;

        private CoroutineHandle m_currentCoroutine;
        private Scene m_currentActiveScene;

        public void Initialize(Scene activeScene)
        {
            m_instance = this;
            m_currentActiveScene = activeScene;

            m_blackScreenCanvas.Initialize();
            m_loadScreenCanvas.SetActive(false);
        }

        public static void LoadScene(AssetReference sceneAssetReference)
        {
            if (m_instance.m_currentCoroutine.IsRunning)
            {
                if (m_instance.m_logDebugMessages)
                    Debug.LogWarning("ApplicationSceneLoader.LoadScene(): coroutine already running");

                return;
            }

            m_instance.m_currentCoroutine = 
                Timing.RunCoroutine(m_instance.coroutine_LoadScene(sceneAssetReference), layer: 1);
        }

        private IEnumerator<float> coroutine_LoadScene(AssetReference sceneAssetReference)
        {
            ApplicationCoreManager.OnSceneLoadStarted?.Invoke();

            yield return Timing.WaitUntilDone(UI_BlackScreenCanvas.Coroutine_FadeIn(0.25f));
            m_loadScreenCanvas.SetActive(true);
            yield return Timing.WaitUntilDone(UI_BlackScreenCanvas.Coroutine_FadeOut(0.25f));

            string _previousSceneName = m_currentActiveScene.name;

            AsyncOperation _asyncHandle = SceneManager.UnloadSceneAsync(m_currentActiveScene);
            while (_asyncHandle.isDone == false)
            {
                m_loadScreenCanvas.SetLoadingAmount(_asyncHandle.progress * 0.3f);
                yield return Timing.WaitForOneFrame;
            }

            if (m_logDebugMessages)
                Debug.Log($"ApplicationSceneLoader: scene {_previousSceneName} unloaded");

            yield return Timing.WaitForOneFrame;

            //int _numOfCoroutinesKilled = Timing.KillCoroutines(layer: 0);

            //if (m_logDebugMessages)
            //    Debug.Log($"Killed {_numOfCoroutinesKilled} MEC coroutines on layer 0");

            ApplicationCoreManager.OnActiveSceneUnloaded?.Invoke();

            AsyncOperationHandle<SceneInstance> _handle = sceneAssetReference.LoadSceneAsync(
                LoadSceneMode.Additive,
                activateOnLoad: false);

            while (_handle.IsDone == false)
            {
                m_loadScreenCanvas.SetLoadingAmount(0.3f + _handle.PercentComplete * 0.3f);
                yield return Timing.WaitForOneFrame;
            }

            m_currentActiveScene = _handle.Result.Scene;

            var _activationRoutine = _handle.Result.ActivateAsync();
            while (_activationRoutine.isDone == false)
                yield return Timing.WaitForOneFrame;

            SceneManager.SetActiveScene(m_currentActiveScene);

            yield return Timing.WaitUntilDone(UI_BlackScreenCanvas.Coroutine_FadeIn(0.25f));
            m_loadScreenCanvas.SetActive(false);
            yield return Timing.WaitUntilDone(UI_BlackScreenCanvas.Coroutine_FadeOut(0.25f));

            if (m_logDebugMessages)
                Debug.Log($"ApplicationSceneLoader: scene {_handle.Result.Scene.name} loaded");

            ApplicationCoreManager.OnSceneLoadCompleted?.Invoke();
        }
    }
}
