using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ApplicationCore
{
    public static class GameObjectExtensions
    {
        public static void SetActiveOptimized(this GameObject go, bool isActive)
        {
            if (go.activeSelf != isActive)
                go.SetActive(isActive);
        }
    }
}