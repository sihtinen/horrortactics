﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ApplicationCore
{
    public static class StringExtensions
    {
        private static Dictionary<int, string> m_allocatedIntStrings = null;
        private static Dictionary<double, string> m_allocatedDoubleStrings = null;

        private const int m_initialRange = 20000;

        public static string ToStringMinimalAlloc(this int value)
        {
            if (m_allocatedIntStrings == null)
            {
                m_allocatedIntStrings = new Dictionary<int, string>();

                for (int i = -m_initialRange; i < m_initialRange; i++)
                {
                    m_allocatedIntStrings.Add(i, i.ToString());
                }
            }

            bool _success = m_allocatedIntStrings.TryGetValue(value, out string _stringResult);

            if (_success)
            {
                return _stringResult;
            }
            else
            {
                string _toString = value.ToString();
                m_allocatedIntStrings.Add(value, _toString);
                return _toString;
            }
        }

        public static string ToStringMinimalAlloc(this double value)
        {
            if (m_allocatedDoubleStrings == null)
            {
                m_allocatedDoubleStrings = new Dictionary<double, string>();

                for (int i = -m_initialRange; i < m_initialRange; i++)
                {
                    m_allocatedDoubleStrings.Add(i, i.ToString());
                }
            }

            if (m_allocatedDoubleStrings.ContainsKey(value))
            {
                return m_allocatedDoubleStrings[value];
            }
            else
            {
                string _toString = value.ToString();
                m_allocatedDoubleStrings.Add(value, _toString);
                return _toString;
            }
        }
    }
}