﻿using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;

namespace ApplicationCore
{
    public static class SerializationUtilities
    {
        public static byte[] SerializeToByteArray(this object obj, bool compress = false)
        {
            if (obj == null)
            {
                return null;
            }

            BinaryFormatter _binaryFormatter = new BinaryFormatter();

            using (MemoryStream _memStream = new MemoryStream())
            {
                _binaryFormatter.Serialize(_memStream, obj);

                if (compress)
                {
                    return _memStream.ToArray().Compress();
                }
                else
                {
                    return _memStream.ToArray();
                }
            }
        }

        public static T DeserializeFromByteArray<T>(this byte[] byteArray, bool isCompressed = false) where T : class
        {
            if (byteArray == null)
            {
                return null;
            }

            if (isCompressed)
            {
                byteArray = byteArray.Decompress();
            }

            using (MemoryStream _memStream = new MemoryStream())
            {
                BinaryFormatter _binaryFormatter = new BinaryFormatter();
                _memStream.Write(byteArray, 0, byteArray.Length);
                _memStream.Seek(0, SeekOrigin.Begin);
                T obj = (T)_binaryFormatter.Deserialize(_memStream);

                return obj;
            }
        }

        public static byte[] Compress(this byte[] data)
        {
            MemoryStream output = new MemoryStream();
            using (DeflateStream dstream = new DeflateStream(output, CompressionLevel.Optimal))
            {
                dstream.Write(data, 0, data.Length);
            }

            return output.ToArray();
        }

        public static byte[] Decompress(this byte[] data)
        {
            MemoryStream input = new MemoryStream(data);
            MemoryStream output = new MemoryStream();

            using (DeflateStream dstream = new DeflateStream(input, CompressionMode.Decompress))
            {
                dstream.CopyTo(output);
            }

            return output.ToArray();
        }
    }
}
