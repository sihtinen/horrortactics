using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace ApplicationCore
{
    public static class ImageExtensions
    {
        public static void SetAlpha(this Image image, float alpha)
        {
            var _color = image.color;
            if (_color.a != alpha)
            {
                _color.a = alpha;
                image.color = _color;
            }
        }
    }
}