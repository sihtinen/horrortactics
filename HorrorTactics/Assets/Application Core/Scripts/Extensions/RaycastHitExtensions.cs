﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ApplicationCore
{
    public static class RaycastHitExtensions
    {
        public struct RaycastHitResultWrapper
        {
            public bool HitFound;
            public RaycastHit RaycastHit;
        }

        public static RaycastHitResultWrapper GetClosestHit(this RaycastHit[] array, int hitCount, GameObject[] ignoreObjects = null)
        {
            RaycastHitResultWrapper _result = new RaycastHitResultWrapper()
            {
                HitFound = false,
            };

            if (hitCount == 1)
            {
                if (ignoreObjects != null && ignoreObjects.Length > 0)
                {
                    bool _hitIgnoredObject = false;

                    for (int i = 0; i < ignoreObjects.Length; i++)
                    {
                        if (array[0].collider.gameObject == ignoreObjects[i])
                        {
                            _hitIgnoredObject = true;
                        }
                    }

                    if (_hitIgnoredObject == false)
                    {
                        _result.HitFound = true;
                        _result.RaycastHit = array[0];
                    }
                }
                else
                {
                    _result.HitFound = true;
                    _result.RaycastHit = array[0];
                }
            }
            else
            {
                float _closestDistance = float.MaxValue;
                RaycastHit _closest = new RaycastHit();

                for (int i = 0; i < hitCount; i++)
                {
                    if (array[i].distance < _closestDistance)
                    {
                        if (ignoreObjects != null && ignoreObjects.Length > 0 && array[i].collider.gameObject)
                        {
                            bool _hitIgnoredObject = false;

                            for (int ii = 0; ii < ignoreObjects.Length; ii++)
                            {
                                if (array[i].collider.gameObject == ignoreObjects[ii])
                                {
                                    _hitIgnoredObject = true;
                                }
                            }

                            if (_hitIgnoredObject == false)
                            {
                                _result.HitFound = true;
                                _closest = array[i];
                                _closestDistance = _closest.distance;
                            }
                        }
                        else
                        {
                            _result.HitFound = true;
                            _closest = array[i];
                            _closestDistance = _closest.distance;
                        }
                    }
                }

                _result.RaycastHit = _closest;
            }

            return _result;
        }
    }
}
