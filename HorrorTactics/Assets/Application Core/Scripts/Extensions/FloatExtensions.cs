﻿public static class FloatExtensions
{
    public static float OneMinus(this float value)
    {
        return 1.0f - value;
    }
}
