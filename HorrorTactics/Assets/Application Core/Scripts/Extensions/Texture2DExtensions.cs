using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace ApplicationCore
{
    public static class Texture2DExtensions
    {
        //public static Vector2 SizeToParent(this RawImage image, float padding = 0)
        //{
        //    float _w = 0, _h = 0;
        //    RectTransform _parent = image.transform.parent.GetComponent<RectTransform>();
        //    RectTransform _imageTransform = image.GetComponent<RectTransform>();

        //    if (image.texture != null)
        //    {
        //        if (_parent == false)
        //        {
        //            return _imageTransform.sizeDelta;
        //        }

        //        padding = 1 - padding;

        //        float _ratio = image.texture.width / (float)image.texture.height;
        //        Rect _bounds = new Rect(0, 0, _parent.rect.width, _parent.rect.height);

        //        if (Mathf.RoundToInt(_imageTransform.eulerAngles.z) % 180 == 90)
        //        {
        //            _bounds.size = new Vector2(_bounds.height, _bounds.width);
        //        }

        //        _h = _bounds.height * padding;
        //        _w = _h * _ratio;

        //        if (_w > _bounds.width * padding)
        //        {
        //            _w = _bounds.width * padding;
        //            _h = _w / _ratio;
        //        }
        //    }

        //    _imageTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _w);
        //    _imageTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, _h);

        //    return _imageTransform.sizeDelta;
        //}

        public static Vector2 SizeToParent(this RawImage image, float padding = 0)
        {
            var parent = image.transform.parent.GetComponentInParent<RectTransform>();
            var imageTransform = image.GetComponent<RectTransform>();
            if (!parent) { return imageTransform.sizeDelta; } //if we don't have a parent, just return our current width;
            padding = 1 - padding;
            float w = 0, h = 0;
            float ratio = image.texture.width / (float)image.texture.height;
            var bounds = new Rect(0, 0, parent.rect.width, parent.rect.height);
            if (Mathf.RoundToInt(imageTransform.eulerAngles.z) % 180 == 90)
            {
                //Invert the bounds if the image is rotated
                bounds.size = new Vector2(bounds.height, bounds.width);
            }
            //Size by height first
            h = bounds.height * padding;
            w = h * ratio;
            if (w > bounds.width * padding)
            { //If it doesn't fit, fallback to width;
                w = bounds.width * padding;
                h = w / ratio;
            }
            imageTransform.sizeDelta = new Vector2(w, h);
            return imageTransform.sizeDelta;
        }
    }
}