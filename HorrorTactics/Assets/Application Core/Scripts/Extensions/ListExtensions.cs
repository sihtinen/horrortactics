﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ApplicationCore
{
    public static class ListExtensions
    {
        public static T GetRandomElement<T>(this IList<T> list)
        {
            if (list == null || list.Count == 0)
            {
                Debug.LogError("ListExtensions.GetRandomElement: list is null or empty");
                return default;
            }

            return list[Random.Range(0, list.Count)];
        }
    }
}