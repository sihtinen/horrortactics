﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;

using MEC;

namespace ApplicationCore
{
    public class SceneInitializer : MonoBehaviour
    {
        [Header("Asset References")]
        [SerializeField] private AssetReference m_sceneToLoad = null;
        [SerializeField] private AssetReference m_applicationCoreScene = null;

        private ApplicationCoreManager m_applicationCoreManager = null;

        private void Start()
        {
            if (ApplicationCoreManager.IsInitialized)
                return;

            Timing.RunCoroutine(coroutine_initializeApplicationCore(), layer: 1);
        }

        private IEnumerator<float> coroutine_initializeApplicationCore()
        {
            AsyncOperationHandle<SceneInstance> _handle = m_applicationCoreScene.LoadSceneAsync(LoadSceneMode.Additive, true);

            while (_handle.IsDone == false)
                yield return Timing.WaitForOneFrame;

            GameObject[] _gos = _handle.Result.Scene.GetRootGameObjects();
            if (_gos != null && _gos.Length > 0)
            {
                for (int i = 0; i < _gos.Length; i++)
                {
                    if (_gos[i].TryGetComponent(out m_applicationCoreManager))
                    {
                        m_applicationCoreManager.Initialize(gameObject.scene, m_sceneToLoad);
                        break;
                    }
                }
            }
        }
    }
}
