using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ApplicationCore.SaveSystem
{
    public class SaveTestComponent : MonoBehaviour
    {
        [System.Serializable]
        public class SaveableObject
        {
            public float Float;
            public char[] CharArray = null;
            public SaveableObject NestedSaveable = null;

            public override string ToString()
            {
                string _result = "Saveable Object: Float: " + Float + ", CharArray: " + new string(CharArray);

                if (NestedSaveable != null)
                    _result += ", nested: " + NestedSaveable.ToString();

                return _result;
            }
        }

        public enum TestMode
        {
            Save,
            Load,
            Delete
        }

        [Header("Settings")]
        public TestMode Mode = TestMode.Save;
        public string SaveFileName = "SaveData_Test";

        private void Start()
        {
            SaveableObject _saveableObject;

            switch (Mode)
            {
                case TestMode.Save:

                    ApplicationSaveManager.Data.RegisterVariable("Int 01", 234);
                    ApplicationSaveManager.Data.RegisterVariable("Double 01", 532.55);
                    ApplicationSaveManager.Data.RegisterVariable("String 01", "justabunchofwords");
                    ApplicationSaveManager.Data.RegisterVariable("Bool 01", true);

                    _saveableObject = new SaveableObject();
                    _saveableObject.Float = 3.33333f;
                    _saveableObject.CharArray = new char[] { '3', 'a', ':' };
                    _saveableObject.NestedSaveable = new SaveableObject();
                    _saveableObject.NestedSaveable.Float = -7.2f;
                    _saveableObject.NestedSaveable.CharArray = new char[] { 'N', 'E', 'S', 'T' };

                    ApplicationSaveManager.Data.RegisterVariable("Object 01", _saveableObject);

                    ApplicationSaveManager.SaveToPersistentData(SaveFileName);

                    break;

                case TestMode.Load:

                    ApplicationSaveManager.LoadFromPersistentData(SaveFileName);

                    _saveableObject = new SaveableObject();
                    ApplicationSaveManager.Data.ReadObject("Object 01", _saveableObject);

                    Debug.Log(ApplicationSaveManager.Data.ToString());

                    break;

                case TestMode.Delete:

                    ApplicationSaveManager.ClearSaveFile(SaveFileName);

                    break;
            }
        }
    }
}
