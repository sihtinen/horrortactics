using System.IO;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ApplicationCore.SaveSystem
{
    public class ApplicationSaveManager : MonoBehaviour
    {
        public static SaveData Data
        {
            get
            {
                if (m_instance.m_currentSaveData == null)
                {
                    m_instance.m_currentSaveData = new SaveData(m_instance.m_saveFileVersion);
                }

                return m_instance.m_currentSaveData;
            }
        }

        public enum SaveFileFormat
        {
            Binary,
            Json
        }

        [Header("Settings")]
        [SerializeField] private double m_saveFileVersion = 1.0;
        [SerializeField] private SaveFileFormat m_fileFormat = SaveFileFormat.Binary;

        [Header("Debug")]
        [SerializeField] private bool m_printDebugMessages = false;

        private static ApplicationSaveManager m_instance = null;

        private SaveData m_currentSaveData = null;

        private static readonly string m_systemIODataPath = "/CoreSaveData/";
        private static readonly string m_fileType_bytes = ".bytes";
        private static readonly string m_fileType_json = ".json";

        public void Initialize()
        {
            m_instance = this;
            m_currentSaveData = new SaveData(m_saveFileVersion);
        }

        public static void SaveToPersistentData(string fileName)
        {
            string _filePath = Application.persistentDataPath + m_systemIODataPath;

            if (Directory.Exists(_filePath) == false)
                Directory.CreateDirectory(_filePath);

            string _fileStreamPath = null;
            string _saveDate;

            switch (m_instance.m_fileFormat)
            {
                case SaveFileFormat.Binary:
                    _saveDate = SaveBackupUtility.GetSaveDateAndDeleteOldBackup(_filePath, fileName, m_fileType_bytes);
                    _fileStreamPath = _filePath + fileName + _saveDate + m_fileType_bytes;
                    break;
                case SaveFileFormat.Json:
                    _saveDate = SaveBackupUtility.GetSaveDateAndDeleteOldBackup(_filePath, fileName, m_fileType_json);
                    _fileStreamPath = _filePath + fileName + _saveDate + m_fileType_json;
                    break;
            }

            switch (m_instance.m_fileFormat)
            {
                case SaveFileFormat.Binary:
                    SaveDataCompressed _compressedSaveFile = new SaveDataCompressed(Data.SerializeToByteArray(true));
                    BinaryFileOperations.Write(_fileStreamPath, _compressedSaveFile);
                    break;
                case SaveFileFormat.Json:
                    JsonFileOperations.Write(_fileStreamPath, Data);
                    break;
            }
        }

        public static bool LoadFromPersistentData(string fileName)
        {
            string _filePath = Application.persistentDataPath + m_systemIODataPath;

            if (Directory.Exists(_filePath) == false)
                Directory.CreateDirectory(_filePath);

            string _fileType = null;

            switch (m_instance.m_fileFormat)
            {
                case SaveFileFormat.Binary:
                    _fileType = m_fileType_bytes;
                    break;
                case SaveFileFormat.Json:
                    _fileType = m_fileType_json;
                    break;
            }

            SaveBackupUtility.BackupLoadResult _backupResult = SaveBackupUtility.GetNewestSaveFile(_filePath, fileName, _fileType);
            bool _loadOperationSuccess = false;

            while (_loadOperationSuccess == false && _backupResult.FileFound == true)
            {
                switch (m_instance.m_fileFormat)
                {
                    case SaveFileFormat.Binary:

                        try
                        {
                            SaveDataCompressed _compressed = BinaryFileOperations.ReadBinary<SaveDataCompressed>(_backupResult.FilePath);
                            m_instance.m_currentSaveData = _compressed.CompressedBytes.DeserializeFromByteArray<SaveData>(true);
                            _loadOperationSuccess = true;
                        }
                        catch (System.Exception ex)
                        {
                            Debug.LogError(ex);
                        }

                        break;
                    case SaveFileFormat.Json:
                        _loadOperationSuccess = JsonFileOperations.ReadJsonFromFile(_backupResult.FilePath, m_instance.m_currentSaveData);
                        break;
                }

                if (_loadOperationSuccess)
                {
                    if (m_instance.m_printDebugMessages)
                        Debug.Log($"SaveManager: file '{fileName}{_fileType}' loaded");

                    return true;
                }
                else
                {
                    if (m_instance.m_printDebugMessages)
                        Debug.LogError($"SaveManager: file corrupted, deleting file:\n{_backupResult.FilePath}");

                    if (File.Exists(_backupResult.FilePath))
                        File.Delete(_backupResult.FilePath);

                    _backupResult = SaveBackupUtility.GetNewestSaveFile(_filePath, fileName, _fileType);
                }
            }

            if (m_instance.m_printDebugMessages)
                Debug.Log($"SaveManager: no valid save file found for {fileName}{_fileType}");

            return false;
        }

        public static void ClearSaveFile(string fileName)
        {
            m_instance.m_currentSaveData = new SaveData(m_instance.m_saveFileVersion);
            SaveToPersistentData(fileName);
        }
    }
}