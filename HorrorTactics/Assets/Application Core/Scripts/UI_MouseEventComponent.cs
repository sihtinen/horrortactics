using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;

public class UI_MouseEventComponent : MonoBehaviour,
    IPointerEnterHandler,
    IPointerExitHandler,

    IPointerClickHandler,

    IBeginDragHandler, 
    IDragHandler, 
    IEndDragHandler
{
    public bool IsDragging 
    {
        get { return m_dragging; }
    }

    public Vector2 DragCurrentPosition
    {
        get { return m_dragStartPosition + m_dragOffset; }
    }

    public event Action OnPointerEnter = null;
    public event Action OnPointerExit = null;

    public Action OnPointerClicked = null;

    public event Action OnBeginDrag = null;
    public event Action<GameObject> OnEndDrag = null;

    private Vector2 m_dragStartPosition = Vector2.zero;
    private Vector2 m_dragOffset = Vector2.zero;

    private bool m_dragging = false;

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
        OnPointerEnter?.Invoke();
    }

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
    {
        OnPointerExit?.Invoke();
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        if (m_dragging) return;

        OnPointerClicked?.Invoke();
    }

    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
    {
        m_dragStartPosition = eventData.position;
        m_dragOffset = Vector2.zero;

        m_dragging = true;

        OnBeginDrag?.Invoke();
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        m_dragOffset += eventData.delta;
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData)
    {
        m_dragging = false;

        OnEndDrag?.Invoke(eventData.pointerCurrentRaycast.gameObject);
    }
}
