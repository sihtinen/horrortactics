﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ApplicationCore
{
    public class ObjectPool<T> : SingletonBehaviour<ObjectPool<T>> where T : MonoBehaviour
    {
        [Header("Object Pool Settings")]
        public T Prefab = null;
        public int InitialSize = 128;
        [SerializeField] private bool m_setParentSelf = false;

        protected List<T> m_freeList = null;
        protected List<T> m_usedList = null;

        protected override void Awake()
        {
            base.Awake();

            if (Instance != this)
            {
                return;
            }

            m_freeList = new List<T>();
            m_usedList = new List<T>();

            instantiateNewObjects();
        }

        private void instantiateNewObjects()
        {
            for (int i = 0; i < InitialSize; i++)
            {
                T _pooledObject = Instantiate(Prefab, transform);
                _pooledObject.gameObject.SetActive(false);

                if (m_setParentSelf)
                    _pooledObject.transform.SetParent(transform);
                else
                    _pooledObject.transform.SetParent(null);

                m_freeList.Add(_pooledObject);
            }
        }

        public static T Get()
        {
            int _numFree = Instance.m_freeList.Count;
            if (_numFree == 0)
            {
                Instance.instantiateNewObjects();
                _numFree = Instance.m_freeList.Count;
            }

            T _pooledObject = Instance.m_freeList[_numFree - 1];
            Instance.m_freeList.RemoveAt(_numFree - 1);
            Instance.m_usedList.Add(_pooledObject);

            return _pooledObject;
        }

        public static void ReturnObject(T pooledObject)
        {
            if (Instance == null || pooledObject == null)
            {
                Debug.LogWarning("ObjectPool.ReturnObject(): instance is null / pooled object is null");
                return;
            }

            pooledObject.gameObject.SetActiveOptimized(false);

            Instance.m_usedList.Remove(pooledObject);
            Instance.m_freeList.Add(pooledObject);
        }

        public static void ResetPool()
        {
            if (Instance.m_usedList.Count > 0)
            {
                for (int i = Instance.m_usedList.Count; i-- > 0;)
                {
                    ReturnObject(Instance.m_usedList[i]);
                }
            }
        }
    }
}