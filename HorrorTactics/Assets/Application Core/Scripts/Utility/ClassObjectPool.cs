using System.Collections;
using System.Collections.Generic;
using System;

public abstract class Poolable
{
    public abstract void ClearMemory();
}

public class ClassObjectPool<T> where T : Poolable, new()
{
    private Stack<T> m_free = new Stack<T>();
    private List<T> m_used = new List<T>();

    private static ClassObjectPool<T> m_instance = null;

    public void Initialize(int poolSize = 128)
    {
        m_instance = this;

        for (int i = 0; i < poolSize; i++)
        {
            T _newInstance = Activator.CreateInstance<T>();
            m_free.Push(_newInstance);
        }
    }

    public T GetT()
    {
        return Get();
    }

    public static T Get()
    {
        if (m_instance.m_free.Count == 0)
        {
            for (int i = 0; i < 16; i++)
            {
                T _newInstance = Activator.CreateInstance<T>();
                m_instance.m_free.Push(_newInstance);
            }
        }

        T _result = m_instance.m_free.Pop();
        m_instance.m_used.Add(_result);
        return _result;
    }

    public static void Reset()
    {
        for (int i = 0; i < m_instance.m_used.Count; i++)
        {
            m_instance.m_used[i].ClearMemory();
            m_instance.m_free.Push(m_instance.m_used[i]);
        }

        m_instance.m_used.Clear();
    }

    public static void ReturnToPool(T instance)
    {
        instance.ClearMemory();
        m_instance.m_used.Remove(instance);
        m_instance.m_free.Push(instance);
    }
}
