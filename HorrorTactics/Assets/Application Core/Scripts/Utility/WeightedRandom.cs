using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ApplicationCore
{
    internal class WeightedInstance<T>
    {
        public WeightedInstance(T instance, int weight)
        {
            Instance = instance;
            Weight = weight;
        }

        public T Instance;
        public int Weight;
        public int StartIndex;
    }

    public class WeightedRandomSelector<T>
    {
        private List<WeightedInstance<T>> m_options = new List<WeightedInstance<T>>();

        public void AddOption(T instance, int weight)
        {
            m_options.Add(new WeightedInstance<T>(instance, weight));
        }

        public T SelectRandom()
        {
            if (m_options.Count == 0) return default(T);

            int _total = 0;

            for (int i = 0; i < m_options.Count; i++)
            {
                m_options[i].StartIndex = _total;
                _total += m_options[i].Weight;
            }

            int _random = Random.Range(0, _total);

            for (int i = 0; i < m_options.Count; i++)
            {
                if (_random >= m_options[i].StartIndex) continue;

                return m_options[i].Instance;
            }

            return m_options[m_options.Count - 1].Instance;
        }
    }
}
