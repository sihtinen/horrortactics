﻿using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ApplicationCore
{
    [Serializable]
    public struct MinMax
    {
        [SerializeField] private float m_min;
        [SerializeField] private float m_max;

        public float Min
        {
            get
            {
                return this.m_min;
            }
            set
            {
                this.m_min = value;
            }
        }

        public float Max
        {
            get
            {
                return this.m_max;
            }
            set
            {
                this.m_max = value;
            }
        }

        public bool IsInRange(float value)
        {
            return value >= this.m_min && value <= this.m_max;
        }

        public float RandomValue
        {
            get
            {
                return UnityEngine.Random.Range(this.m_min, this.m_max);
            }
        }

        public MinMax(float min, float max)
        {
            this.m_min = min;
            this.m_max = max;
        }

        public float Clamp(float value)
        {
            return Mathf.Clamp(value, this.m_min, this.m_max);
        }
    }

    public class MinMaxSliderAttribute : PropertyAttribute
    {
        public readonly float Min;
        public readonly float Max;

        public MinMaxSliderAttribute(float min, float max)
        {
            Min = min;
            Max = max;
        }
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(MinMax))]
    [CustomPropertyDrawer(typeof(MinMaxSliderAttribute))]
    public class MinMaxDrawer : PropertyDrawer
    {

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (property.serializedObject.isEditingMultipleObjects) return 0f;
            return base.GetPropertyHeight(property, label) + 16f;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.serializedObject.isEditingMultipleObjects) return;

            SerializedProperty _minProperty = property.FindPropertyRelative("m_min");
            SerializedProperty _maxProperty = property.FindPropertyRelative("m_max");
            MinMaxSliderAttribute _minmax = attribute as MinMaxSliderAttribute ?? new MinMaxSliderAttribute(0, 1);
            position.height -= 16f;

            label = EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            float _min = _minProperty.floatValue;
            float _max = _maxProperty.floatValue;

            Rect _left = new Rect(position.x, position.y, position.width / 2 - 11f, position.height);
            Rect _right = new Rect(position.x + position.width - _left.width, position.y, _left.width, position.height);
            Rect _mid = new Rect(_left.xMax, position.y, 22, position.height);

            _min = Mathf.Clamp(EditorGUI.FloatField(_left, _min), _minmax.Min, _max);
            EditorGUI.LabelField(_mid, " to ");
            _max = Mathf.Clamp(EditorGUI.FloatField(_right, _max), _min, _minmax.Max);

            position.y += 16f;
            EditorGUI.MinMaxSlider(position, GUIContent.none, ref _min, ref _max, _minmax.Min, _minmax.Max);

            _minProperty.floatValue = _min;
            _maxProperty.floatValue = _max;
            EditorGUI.EndProperty();
        }
    }
#endif
}