﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ApplicationCore
{
    public static class UIUtility
    {
        private static List<RaycastResult> m_cachedResults = new List<RaycastResult>();

        /// <summary>
        /// Cast a ray to test if Input.mousePosition is over any UI object in EventSystem.current. This is a replacement
        /// for IsPointerOverGameObject() which does not work on Android in 4.6.0f3
        /// </summary>
        public static bool IsPointerOverUIObject()
        {
            // Referencing this code for GraphicRaycaster https://gist.github.com/stramit/ead7ca1f432f3c0f181f
            // the ray cast appears to require only eventData.position.
            PointerEventData _eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            _eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            m_cachedResults.Clear();
            EventSystem.current.RaycastAll(_eventDataCurrentPosition, m_cachedResults);
            return m_cachedResults.Count > 0;
        }

        /// <summary>
        /// Cast a ray to test if screenPosition is over any UI object in canvas. This is a replacement
        /// for IsPointerOverGameObject() which does not work on Android in 4.6.0f3
        /// </summary>
        public static bool IsPointerOverUIObject(Canvas[] canvases, Vector2 screenPosition)
        {
            // Referencing this code for GraphicRaycaster https://gist.github.com/stramit/ead7ca1f432f3c0f181f
            // the ray cast appears to require only eventData.position.
            PointerEventData _eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            _eventDataCurrentPosition.position = screenPosition;

            int _results = 0;

            for (int i = 0; i < canvases.Length; i++)
            {
                if (canvases[i].gameObject.TryGetComponent(out GraphicRaycaster raycaster))
                {
                    m_cachedResults.Clear();
                    raycaster.Raycast(_eventDataCurrentPosition, m_cachedResults);
                    _results += m_cachedResults.Count;
                }
            }

            return _results > 0;
        }

        public static bool IsInputOverUIObject(Vector2 inputPosition, GameObject targetObject)
        {
            // Referencing this code for GraphicRaycaster https://gist.github.com/stramit/ead7ca1f432f3c0f181f
            // the ray cast appears to require only eventData.position.
            PointerEventData _eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            _eventDataCurrentPosition.position = inputPosition;

            m_cachedResults.Clear();
            EventSystem.current.RaycastAll(_eventDataCurrentPosition, m_cachedResults);

            if (m_cachedResults.Count == 0) return false;

            for (int i = 0; i < m_cachedResults.Count; i++)
            {
                if (m_cachedResults[i].gameObject == targetObject) return true;
            }

            return false;
        }
    }
}