﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace ApplicationCore
{
    public static class Vector3Utility
    {
        public static Vector3 Random(float scale)
        {
            return new Vector3(
                UnityEngine.Random.Range(-scale, scale),
                UnityEngine.Random.Range(-scale, scale),
                UnityEngine.Random.Range(-scale, scale));
        }

        public static Vector3 Random(float scaleX, float scaleY, float scaleZ)
        {
            return new Vector3(
                UnityEngine.Random.Range(-scaleX, scaleX),
                UnityEngine.Random.Range(-scaleY, scaleY),
                UnityEngine.Random.Range(-scaleZ, scaleZ));
        }

        public static Vector3 CalculateCenter(this Vector3[] array)
        {
            Vector3 _result = Vector3.zero;

            for (int i = 0; i < array.Length; i++)
            {
                _result += array[i];
            }

            return _result / array.Length;
        }
    }
}
