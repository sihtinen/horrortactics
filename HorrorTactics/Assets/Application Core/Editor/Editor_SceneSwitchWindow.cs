﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEditor;
using UnityEditor.SceneManagement;

public class Editor_SceneSwitchWindow : EditorWindow
{
	public enum ScenesSource
	{
		Assets,
		BuildSettings
	}

	private Vector2 m_scrollPosition;
	private ScenesSource m_scenesSource = ScenesSource.Assets;
	private OpenSceneMode m_openSceneMode = OpenSceneMode.Single;

	public string[] FilterKeywords = new string[]{ "Plugins", "Editor" };

	[MenuItem("Tools/Scene Switcher")]
	public static void Init()
	{
		var window = GetWindow<Editor_SceneSwitchWindow>("Scene Switch");
		window.Show();
	}

	protected virtual void OnGUI()
	{
		List<EditorBuildSettingsScene> buildScenes = new List<EditorBuildSettingsScene>(EditorBuildSettings.scenes);
		this.m_scenesSource = (ScenesSource)EditorGUILayout.EnumPopup("Scenes Source", this.m_scenesSource);
		this.m_openSceneMode = (OpenSceneMode)EditorGUILayout.EnumPopup("Open Scene Mode", this.m_openSceneMode);

		ScriptableObject _target = this;
		SerializedObject _so = new SerializedObject(_target);
		SerializedProperty stringsProperty = _so.FindProperty("FilterKeywords");
		GUIContent _label = new GUIContent() { text = "Hide Scenes By File Path Keywords" };
		EditorGUILayout.PropertyField(stringsProperty, _label, true);
		_so.ApplyModifiedProperties();

		GUILayout.Label("Scenes", EditorStyles.boldLabel);
		string[] guids = AssetDatabase.FindAssets("t:Scene");
		this.m_scrollPosition = EditorGUILayout.BeginScrollView(this.m_scrollPosition);

		EditorGUILayout.BeginVertical();

		for (int i = 0; i < guids.Length; i++)
		{
			string path = AssetDatabase.GUIDToAssetPath(guids[i]);

			if (FilterKeywords != null && FilterKeywords.Length > 0)
            {
				bool _ignore = false;

                for (int ii = 0; ii < FilterKeywords.Length; ii++)
                {
					if (FilterKeywords[ii] != string.Empty && path.Contains(FilterKeywords[ii]))
					{
						_ignore = true;
					}
				}

				if (_ignore) continue;
            }

			SceneAsset sceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(path);

			EditorBuildSettingsScene buildScene = buildScenes.Find((editorBuildScene) =>
			{
				return editorBuildScene.path == path;
			});

			Scene scene = SceneManager.GetSceneByPath(path);
			bool isOpen = scene.IsValid() && scene.isLoaded;
			GUI.enabled = !isOpen;

			if (this.m_scenesSource == ScenesSource.Assets)
			{
				if (GUILayout.Button(sceneAsset.name))
				{
					Open(path);
				}
			}
			else
			{
				if (buildScene != null)
				{
					if (GUILayout.Button(sceneAsset.name))
					{
						Open(path);
					}
				}
			}

			GUI.enabled = true;
		}

		//if (GUILayout.Button("Create New Scene"))
		//{
		//	Scene newScene = EditorSceneManager.NewScene(NewSceneSetup.DefaultGameObjects, NewSceneMode.Single);
		//	EditorSceneManager.SaveScene(newScene);
		//}

		EditorGUILayout.EndVertical();
		EditorGUILayout.EndScrollView();
	}

	public virtual void Open(string path)
	{
		if (EditorSceneManager.EnsureUntitledSceneHasBeenSaved("You don't have saved the Untitled Scene, Do you want to leave?"))
		{
			EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
			EditorSceneManager.OpenScene(path, this.m_openSceneMode);
		}
	}

}