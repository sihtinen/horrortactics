﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using UnityEditor;


public class Editor_PersistentDataPathButton : EditorWindow
{
	[MenuItem("Tools/Open Persistent Data Path")]
	public static void Execute()
	{
		Application.OpenURL($"file://{Application.persistentDataPath}");
	}
}
