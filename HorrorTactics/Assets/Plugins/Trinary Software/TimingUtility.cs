using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace MEC
{
    public static class TimingUtility
    {
        public static CoroutineHandle SubscribeToUpdate(
            Action callback, 
            MonoBehaviour sourceMonoBehaviour, 
            Segment segment = Segment.Update, 
            int layer = 0)
        {
            return Timing.RunCoroutine(
                coroutine:  coroutine_update(callback, sourceMonoBehaviour),
                segment:    segment,
                layer:      layer);
        }

        private static IEnumerator<float> coroutine_update(Action callback, MonoBehaviour sourceMonoBehaviour)
        {
            yield return Timing.WaitForOneFrame;

            while (sourceMonoBehaviour != null && sourceMonoBehaviour.gameObject != null)
            {
                if (sourceMonoBehaviour.gameObject.activeInHierarchy && sourceMonoBehaviour.enabled)
                    callback();

                yield return Timing.WaitForOneFrame;
            }
        }
    }
}